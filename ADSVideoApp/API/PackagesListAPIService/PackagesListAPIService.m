//
//  PackagesListAPIService.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/17/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//


#define APIService_protected
#import "PackagesListAPIService.h"
#import "Organization.h"

@interface PackagesListAPIService ()

@property (strong, nonatomic) NSString * code;

@end

@implementation PackagesListAPIService

+ (instancetype)getService {
	return [[PackagesListAPIService alloc] init];
}

- (instancetype)init {
	self = [super init];
	if (self) {
		self.code = nil;
	}
	return self;
}

- (instancetype)initWithCode:(NSString *)code {
	self = [super init];
	if (self) {
		self.code = code;
	}
	return self;
}

- (NSDictionary *)getParams {
	NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithObject:appIdAPIString forKey:appIdAPIKey];
	if (self.code) {
		[dict setObject:self.code forKey:appCodeAPIKey];
	}
	
	return dict;
}


- (void)sendRequestWithCompletion:(CompletionBlock)completion {
	WELF_MACRO
	[self.httpManager POST:@"packages/getAppPackages"
				parameters:[self getParams]
				  progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
					  
					  NSInteger statusCode = ((NSHTTPURLResponse *)task.response).statusCode;
					  if (statusCode == HTTP_OK) {
						[welf processResponce:responseObject withCompletion:completion];
					  } else {
						NSError * error = [Utils errorWithLocalizedDescription:[NSString stringWithFormat:@"Failed to connect to server"] code:statusCode domainClass:[self class]];
						  [welf processResponce:error withCompletion:completion];
					  }
				  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
					  NSLog(@"%@", task.response);
					  NSLog(@"%@", task.originalRequest);
					  NSLog(@"%@", task.currentRequest);
					  
					  [welf processResponce:error withCompletion:completion];
				  }];
	
}


- (void)handleSuccess:(NSDictionary *)responceDictionary withCompletion:(CompletionBlock)completion {

	NSMutableArray <Organization *> * organizations = [NSMutableArray array];
	
	if ([responceDictionary containsValueForKey:orgsAPIKey]) {
		NSArray * orgDictionaries = responceDictionary[orgsAPIKey];
		for (NSDictionary * dict in orgDictionaries) {
			[organizations addObject:[[Organization alloc] initWithDict:dict]];
		}
	}
	
	id item = nil;
	
	if (self.code) {
		NSArray <Package *> * packages = organizations ? organizations.count > 0 ? organizations[0].packages ? : nil : nil : nil;
		item = packages.count > 0 ? packages : nil;
	} else {
		item = organizations ? organizations : nil;
	}
	
	if (item) {
		if (completion) {
			completion(CompletionStatusSucces, item, nil);
		}
	} else {
		NSError * error = nil;
		if (self.code) {
			error = [Utils errorWithLocalizedDescription:@"Package not found" code:404 domainClass:[self class]];
		}
//		else {
//			error = [Utils errorWithLocalizedDescription:@"There are no content packages on this device. To download content, enter/scan a code provided by the content publisher or browse the list of public content."
//													code:1488
//											 domainClass:[self class]];
//		}
		
		if (completion) {
			completion(CompletionStatusError, nil, error);
		}
	}
}

@end
