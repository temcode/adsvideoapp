//
//  PackagesListAPIService.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/17/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "APIService.h"

@interface PackagesListAPIService : APIService
- (instancetype)initWithCode:(NSString *)code;
@end
