//
//  SarzinAPIService.h
//  Sarzin
//
//  Created by Artem Selivanov on 12/21/16.
//  Copyright © 2016 Sarzin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

#define NoInternetConnectionErrorCode 90000

#define ecInvalidEmail      90100
#define ecInvalidPhone      90110
#define ecEmptyFields		90111
#define ecInvalidPassword   90101
#define ecServerResponce    90102
#define ecInvalidUID        90103
#define ecBadResponce        90104
#define ecUnauthorizedResponce        90105

@protocol SarzinAPIServiceDelegate <NSObject>

- (void)progressDialogWasTapped;

@end

@interface APIService : NSObject

@property (weak, nonatomic) id<SarzinAPIServiceDelegate> delegate;
@property (nonatomic, assign) BOOL needShowProgressHUD;

+ (instancetype)getService;
- (BOOL)callServiceForController:(UIViewController *)callerController withCompletion:(CompletionBlock)completion;

@end

#ifdef APIService_protected

extern NSInteger const HTTP_OK;
extern NSInteger const HTTP_CREATED;
extern NSInteger const HTTP_UNAUTHORIZED;

@interface APIService ()

@property (nonatomic, weak) UIViewController *callerController;
@property (nonatomic, strong) dispatch_queue_t bgQueue;
@property (nonatomic, strong, readwrite) AFHTTPSessionManager* httpManager;

@property (nonatomic, assign) BOOL needHideProgressHUDAfterMainResponce;
@property (nonatomic, assign) BOOL needToObserveProgressHUDState;
@property (assign, nonatomic) BOOL isSyncModeEnabled;

@property (nonatomic, strong) NSString * progressStatus;

- (void)preRequest;
- (void)preResponce;
- (void)postResponce;
- (void)processResponce:(id)responceObject withCompletion:(CompletionBlock)completion;
- (void)noInternetConnection:(CompletionBlock)completion;
- (AFHTTPSessionManager *)initializationAFHTTPManager;

- (BOOL)validateParameters:(NSError **)error;
- (NSDictionary *)getParams;
- (NSString *)getMainEndPoint;
- (void)sendRequestWithCompletion:(CompletionBlock)completion;
- (void)didTapOnProgressHud:(NSNotificationCenter *)notification;
- (void)handleSuccess:(NSDictionary *)responceDictionary withCompletion:(CompletionBlock)completion;
- (void)handleFaluireWithError:(NSError *)error completion:(CompletionBlock)completion;

- (void)showProgressHUD;
- (void)hideProgressHUD;

@end
#endif
