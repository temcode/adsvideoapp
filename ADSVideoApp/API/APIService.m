//
//  SarzinAPIService.m
//  Sarzin
//
//  Created by Artem Selivanov on 12/21/16.
//  Copyright © 2016 Sarzin. All rights reserved.
//

#define APIService_protected
#import "APIService.h"

NSInteger const HTTP_OK = 200;
NSInteger const HTTP_CREATED = 201;
NSInteger const HTTP_UNAUTHORIZED = 401;


@interface APIService ()

@property (strong, nonatomic) APIService *selfReference;

@property (strong, nonatomic) dispatch_group_t completionGroup;

@end

@implementation APIService

#pragma mark - Life Cycle -

- (void)dealloc {
    
    NSLog(@"%@ - DEALLOC", NSStringFromClass([self class]));
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    self.bgQueue = nil;
    self.httpManager = nil;
    self.completionGroup = nil;
}

#pragma mark - Public Methods -

+ (instancetype)getService {
    return [[APIService alloc] init];
}

- (instancetype)init {
    
    self = [super init];
    
    if (self) {
        self.callerController = nil;
        self.needShowProgressHUD = YES;
        self.needToObserveProgressHUDState = NO;
        self.isSyncModeEnabled = NO;
        return self;
    }
    
    return nil;
}

- (BOOL)callServiceForController:(UIViewController *)callerController withCompletion:(CompletionBlock)completion {
	
	NSLog(@"%@ - CALLED", NSStringFromClass([self class]));
	
    if (callerController)
        self.callerController = callerController;
	
    NSError *error = nil;
    if ([AFNetworkReachabilityManager sharedManager].networkReachabilityStatus == AFNetworkReachabilityStatusNotReachable) {
        [self noInternetConnection:completion];
    } else if (![self validateParameters:&error]) {
        
        if (completion)
            completion(CompletionStatusError, nil, error);
        
    } else {
        
        if (self.isSyncModeEnabled) {
            self.completionGroup = dispatch_group_create();
            dispatch_group_enter(self.completionGroup);
        }
    
        [self preRequest];
        WELF_MACRO
        dispatch_async(self.bgQueue, ^{
            [welf sendRequestWithCompletion:completion];
        });
        if (self.isSyncModeEnabled) {
             dispatch_group_wait(self.completionGroup, DISPATCH_TIME_FOREVER);
        }
       
        return YES;
    }
    
    return NO;
}

#pragma mark - Private -




#pragma mark - Protected -

- (void)noInternetConnection:(CompletionBlock)completion {
    NSError * error = [Utils errorWithLocalizedDescription:localized(@"FailedToConnectToServer")
                                            code:NoInternetConnectionErrorCode
                                     domainClass:[self class]];
    if (completion)
		completion(CompletionStatusError, nil, error);
}

- (AFHTTPSessionManager *)initializationAFHTTPManager {
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:mainURL
									sessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
	
//	manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
//	manager.requestSerializer = [AFJSONRequestSerializer serializer];
	
	[manager.responseSerializer setAcceptableContentTypes:[NSSet setWithArray:@[@"text/html", @"application/json"]]];
	
	return manager;
}

#pragma mark * Getters

- (AFHTTPSessionManager *)httpManager {
    
    if (!_httpManager) {
        _httpManager = [self initializationAFHTTPManager];
        
        [_httpManager.requestSerializer setTimeoutInterval:15.f];
        [_httpManager setCompletionQueue:self.bgQueue];
    }
    
    return _httpManager;
}

- (dispatch_queue_t)bgQueue {
    if (!_bgQueue) {
        NSString *queueLabelString = [NSString stringWithFormat:@"com.ios.Sarzin.api.service.%@.bgQueue", NSStringFromClass([self class])];
        const char *queueLabel = [queueLabelString UTF8String];
        _bgQueue = dispatch_queue_create(queueLabel , NULL);
    }
    
    return _bgQueue;
}

#pragma mark * Methods

- (void)showProgressHUD {
    NSString *progressStatus = [self progressStatus];
    if (progressStatus) {
        [SVProgressHUD showWithStatus:[self progressStatus]];
    } else {
        [SVProgressHUD show];
    }
    
    if (self.callerController) {
        [defaultNotificationCenter addObserver:self
                                      selector:@selector(didTapOnProgressHud:)
                                          name:SVProgressHUDDidTouchDownInsideNotification
                                        object:nil];
    }
}

- (void)hideProgressHUD {
    if ([SVProgressHUD isVisible])
        [SVProgressHUD popActivity];
    
    if (self.callerController) {
        [defaultNotificationCenter removeObserver:self.callerController
                                             name:SVProgressHUDDidTouchDownInsideNotification
                                           object:nil];
    }
}

- (void)preRequest {
    NSLog(@"%@: PreRequest", NSStringFromClass([self class]));
    
    self.selfReference = self;
    
    if (self.needShowProgressHUD) {
        WELF_MACRO
        performBlockOnMainThread(^{
            [welf showProgressHUD];
        });
    }
}

- (void)preResponce {
     NSLog(@"%@: PreResponce", NSStringFromClass([self class]));

    if (self.needShowProgressHUD) {
        WELF_MACRO
        performBlockOnMainThread(^{
            [welf hideProgressHUD];
        });
    }
}

- (void)postResponce {
    NSLog(@"%@: PostResponce", NSStringFromClass([self class]));
    
    if (self.isSyncModeEnabled) {
        dispatch_group_leave(self.completionGroup);
    }
    
    self.selfReference = nil;
}

- (void)processResponce:(id)responceObject withCompletion:(CompletionBlock)completion {
    
    [self preResponce];
    
    if ([responceObject isKindOfClass:[NSError class]]) {
        NSLog(@"%@: Failed %@", NSStringFromClass([self class]), [(NSError *)responceObject localizedDescription]);
        [self handleFaluireWithError:(NSError *)responceObject completion:completion];
    } else {
        if (responceObject) {
			NSLog(@"%@: SUCCESS", NSStringFromClass([self class]));
            NSDictionary * responceDictionary = (NSDictionary *)responceObject;
            [self handleSuccess:responceDictionary withCompletion:completion];
        } else {
            NSLog(@"%@: Failed to parse responce object - %@", NSStringFromClass([self class]), responceObject);
			if (completion) {
				completion(CompletionStatusSucces, nil, nil);
			}
        }
    }
    
    [self postResponce];
}


#pragma mark * Abstract

- (BOOL)validateParameters:(NSError **)error {
    return YES;
}

- (NSString *)progressStatus {
    return localized(@"Loading");
}

- (NSDictionary *)getParams {
    return nil;
}

- (NSString *)getMainEndPoint {
    return nil;
}

- (void)sendRequestWithCompletion:(CompletionBlock)completion {}

- (void)didTapOnProgressHud:(NSNotificationCenter *)notification {}

- (void)handleSuccess:(NSDictionary *)responceObject withCompletion:(CompletionBlock)completion {
    if (completion) {
        completion(CompletionStatusSucces, responceObject, nil);
    }
}

- (void)handleFaluireWithError:(NSError *)error completion:(CompletionBlock)completion {
    if (completion) {
        completion(CompletionStatusError, nil, error);
    }
}

@end
