//
//  SarzinPresentationAnimationController.h
//  Sarzin
//
//  Created by Artem Selivanov on 1/25/17.
//  Copyright © 2017 Sarzin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DialogPresentationAnimationController : NSObject <UIViewControllerAnimatedTransitioning>

@property (assign, nonatomic) BOOL isPresenting;

@end
