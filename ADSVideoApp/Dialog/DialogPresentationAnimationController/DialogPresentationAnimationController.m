//
//  SarzinPresentationAnimationController.m
//  Sarzin
//
//  Created by Artem Selivanov on 1/25/17.
//  Copyright © 2017 Sarzin. All rights reserved.
//

#import "DialogPresentationAnimationController.h"

@implementation DialogPresentationAnimationController

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext {
	
	return self.isPresenting ? 0.1f : 0.1f;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext {
	
	UIViewController * fromVc = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
	UIViewController * toVc = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
	UIView * fromView = fromVc.view;
	UIView * toView = toVc.view;
	UIView *containerView = [transitionContext containerView];
	
	UIViewController * animatingVc = self.isPresenting ? toVc : fromVc;
	UIView * animatingView = animatingVc.view;
	
	
	if (self.isPresenting) {
		[containerView addSubview:toView];
		toView.layer.opacity = 0.2f;
		toView.layer.transform = CATransform3DMakeScale(1.5f, 1.5f, 1.0f);
	} else {
	
	}
	
	CGRect finalFrameForVc = [transitionContext finalFrameForViewController:animatingVc];
	CGRect initalFrameForVc = finalFrameForVc;
	
	CGRect initialFrame = self.isPresenting ? initalFrameForVc : finalFrameForVc;
	CGRect finalFrame = self.isPresenting ? finalFrameForVc : initalFrameForVc;
	
	animatingView.frame = initialFrame;
	animatingView.layer.cornerRadius = 5.f;
	
	UIViewAnimationOptions option = self.isPresenting ? UIViewAnimationOptionCurveEaseIn : UIViewAnimationOptionCurveEaseOut;
	
	[UIView animateWithDuration:[self transitionDuration:transitionContext]
						  delay:0
						options:option
					 animations:^{
						 
						 animatingView.frame = finalFrame;
						 
						 if (self.isPresenting) {
							 toView.layer.opacity = 1.0f;
							 toView.layer.transform = CATransform3DMakeScale(1.f, 1.f, 1.f);
						 } else {
							 fromView.layer.opacity = 0.0f;
						 }
					 } completion:^(BOOL finished) {
						 
						 if (!self.isPresenting) {
							 [fromView removeFromSuperview];
						 }
						 
						 [transitionContext completeTransition:YES];
					 }];
}


@end
