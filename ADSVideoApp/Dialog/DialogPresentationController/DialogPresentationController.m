//
//  SarzinPresentationController.m
//  Sarzin
//
//  Created by Artem Selivanov on 1/25/17.
//  Copyright © 2017 Sarzin. All rights reserved.
//

#import "DialogPresentationController.h"

@interface DialogPresentationController ()

@property (nonatomic, readonly) UIView *dimmingView;
@property (strong, nonatomic) UITapGestureRecognizer * dismissGesture;
@property (assign, nonatomic) CGFloat keyboardHeight;
@end

@implementation DialogPresentationController

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	if ([[self.dimmingView gestureRecognizers] containsObject:self.dismissGesture]) {
		[self.dimmingView removeGestureRecognizer:self.dismissGesture];
	}
	self.dismissGesture = nil;
}

- (UIView *)dimmingView {
	static UIView *instance = nil;
	if (instance == nil) {
		instance = [[UIView alloc] initWithFrame:self.containerView.bounds];
		instance.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
	}
	if (![[instance gestureRecognizers] containsObject:self.dismissGesture] || !self.dismissGesture) {
		self.dismissGesture = self.dismissGesture ? : [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss:)];
		[instance addGestureRecognizer:self.dismissGesture];
	}
	return instance;
}

- (void)dismiss:(UITapGestureRecognizer *)recognizer {
	if (self.protocol && [self.protocol respondsToSelector:@selector(shouldDismissPresentedControllerByOutsideTap)]) {
		if ([self.protocol shouldDismissPresentedControllerByOutsideTap]) {
			[self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
		}
	}
}


- (void)keyboardWillBeShown:(NSNotification *)notification {
	
	NSDictionary* keyboardInfo = [notification userInfo];
	CGRect keyboardFrame = [keyboardInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
	self.keyboardHeight = keyboardFrame.size.height;
	[self resizeDialog];
}

- (void)keyboardWasShown:(NSNotification *)notification {
	
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
	self.keyboardHeight = 0;
	[self resizeDialog];
}

- (void)keyboardWasHidden:(NSNotification *)notification {
	
}


- (void)presentationTransitionWillBegin {
	
	UIView *presentedView = self.presentedViewController.view;

	presentedView.layer.shadowColor = [[UIColor blackColor] CGColor];
	presentedView.layer.shadowOffset = CGSizeMake(0, 10);
	presentedView.layer.shadowRadius = 10;
	presentedView.layer.shadowOpacity = 0.5;
	presentedView.layer.cornerRadius = 5.f;
	
	self.dimmingView.frame = self.containerView.bounds;
	self.dimmingView.alpha = 0;
	[self.containerView addSubview:self.dimmingView];
	
	[self.presentedViewController.transitionCoordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
		self.dimmingView.alpha = 1;
	} completion:nil];
}

- (void)presentationTransitionDidEnd:(BOOL)completed {
	if (!completed) {
		[self.dimmingView removeFromSuperview];
	} else {
		self.keyboardHeight = 0.f;
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(keyboardWillBeShown:)
													 name:UIKeyboardWillShowNotification
												   object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(keyboardWasShown:)
													 name:UIKeyboardDidShowNotification
												   object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(keyboardWillBeHidden:)
													 name:UIKeyboardWillHideNotification
												   object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(keyboardWasHidden:)
													 name:UIKeyboardDidHideNotification
												   object:nil];
	}
}

- (void)dismissalTransitionWillBegin {
	
	if ([self.protocol respondsToSelector:@selector(presentedControllerWillBeDismissed)]) {
		[self.protocol presentedControllerWillBeDismissed];
	}
	
	[self.presentedViewController.transitionCoordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
		self.dimmingView.alpha = 0;
	} completion:nil];
}

- (void)dismissalTransitionDidEnd:(BOOL)completed {
	if (completed) {
		[self.dimmingView removeFromSuperview];
	}
}

- (void)resizeDialog {
	CGRect r = [self frameOfPresentedViewInContainerView];
	[UIView beginAnimations:@"dialogResizeAnimation" context:nil];
	self.presentedViewController.view.frame = r;
	[UIView commitAnimations];
}

- (CGRect)frameOfPresentedViewInContainerView {
	
	CGSize maxSize = maxPresentationSize;
	maxSize = CGSizeMake(maxSize.width, maxSize.height - self.keyboardHeight);
	CGSize neededSize = [self.protocol sizeForPresentatingView];
	
	CGFloat width = maxSize.width <= neededSize.width ? maxSize.width : neededSize.width;
	CGFloat height = maxSize.height <= neededSize.height ? maxSize.height : neededSize.height;
	
	CGRect frame = CGRectZero;
	
	if (self.keyboardHeight > 0) {
		frame = CGRectMake((self.containerView.frame.size.width - width) / 2,
							  (self.containerView.frame.size.height - self.keyboardHeight - height) / 2,
							  width,
							  height);
		
	} else {
		frame = CGRectMake((self.containerView.frame.size.width - width) / 2,
							  (self.containerView.frame.size.height - height) / 2,
							  width,
							  height);
	}
	
	return frame;
	
}

- (void)containerViewWillLayoutSubviews {
	self.dimmingView.frame = self.containerView.bounds;
	self.presentedView.frame = [self frameOfPresentedViewInContainerView];
}

@end
