//
//  SarzinPresentationController.h
//  Sarzin
//
//  Created by Artem Selivanov on 1/25/17.
//  Copyright © 2017 Sarzin. All rights reserved.
//

#import <UIKit/UIKit.h>

#define dialogMargin 25.f

#define maxPresentationSize CGSizeMake(SCREEN_SIZE.width - (dialogMargin * 2.f), SCREEN_SIZE.height - (dialogMargin * 2.f))

@protocol DialogPresentationControllerProtocol <NSObject>

- (CGSize)sizeForPresentatingView;

- (void)presentedControllerWillBeDismissed;
- (BOOL)shouldDismissPresentedControllerByOutsideTap;

@end

@interface DialogPresentationController : UIPresentationController

@property (weak, nonatomic) id<DialogPresentationControllerProtocol> protocol;

- (CGRect)frameOfPresentedViewInContainerView;
- (void)resizeDialog;

@end
