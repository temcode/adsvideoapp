//
//  SarzinDialogNavigationController.m
//  Sarzin
//
//  Created by Artem Selivanov on 1/27/17.
//  Copyright © 2017 Sarzin. All rights reserved.
//

#import "DialogNavigationController.h"

#import "DialogViewController.h"
#import "DialogPresentationController.h"
#import "DialogPresentationAnimationController.h"

@interface DialogNavigationController () <UIViewControllerTransitioningDelegate>
@property (assign, nonatomic) CGFloat keyboardHeight;
@end

@implementation DialogNavigationController

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	if (self) {
		if ([self respondsToSelector:@selector(setTransitioningDelegate:)]) {
			self.modalPresentationStyle = UIModalPresentationCustom;
			self.transitioningDelegate = self;
		}
	}
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.view.layer.cornerRadius = 5.f;

	self.navigationBar.layer.cornerRadius = 5.f;
	
	[self setNeedsStatusBarAppearanceUpdate];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWillBeShown:)
												 name:UIKeyboardWillShowNotification
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWasShown:)
												 name:UIKeyboardDidShowNotification
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWillBeHidden:)
												 name:UIKeyboardWillHideNotification
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWasHidden:)
												 name:UIKeyboardDidHideNotification
											   object:nil];
	
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(didChangeOrientation:)
												 name:UIApplicationDidChangeStatusBarOrientationNotification
											   object:nil];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
	return UIStatusBarStyleLightContent;
}

- (void)didChangeOrientation:(NSNotification *)notificaiton {

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Keyboard Notifications -

- (void)keyboardWillBeShown:(NSNotification *)notification {
	NSDictionary * dict = notification.userInfo;
	
	CGFloat duration = [dict[UIKeyboardAnimationDurationUserInfoKey] floatValue];
	CGFloat keyboardHeight = [dict[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
	self.keyboardHeight = keyboardHeight;
	CGRect finalRect = CGRectZero;
	CGFloat freeHeight = maxPresentationSize.height - keyboardHeight + dialogMargin;
	
	if (freeHeight > self.view.frame.size.height) {
		finalRect = CGRectMake((SCREEN_WIDTH - (dialogWidth)) / 2.f,
							   (freeHeight - self.view.frame.size.height) / 2.f,
							   dialogWidth,
							   self.view.frame.size.height);
	} else if (freeHeight == self.view.frame.size.height) {
		finalRect = CGRectMake((SCREEN_WIDTH - (dialogWidth)) / 2.f,
							   dialogMargin,
							   dialogWidth,
							   freeHeight);
	} else {
		finalRect = CGRectMake((SCREEN_WIDTH - (dialogWidth)) / 2.f,
							   0,
							   dialogWidth,
							   self.view.frame.size.height);
	}
	
	[UIView animateWithDuration:duration animations:^{
		self.view.frame = finalRect;
		[self.view layoutIfNeeded];
	}];
}

- (void)keyboardWasShown:(NSNotification *)notification {
	
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
	NSDictionary * dict = notification.userInfo;
	
	CGFloat duration = [dict[UIKeyboardAnimationDurationUserInfoKey] floatValue];
	CGFloat keyboardHeight = [dict[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
	self.keyboardHeight = keyboardHeight;
	CGRect finalRect = CGRectZero;
	CGFloat freeHeight = maxPresentationSize.height;
	CGFloat neededHeight = [(DialogPresentationController *)self.presentationController frameOfPresentedViewInContainerView].size.height;
	if (freeHeight > neededHeight) {
		finalRect = CGRectMake((SCREEN_WIDTH - (dialogWidth)) / 2.f,
							   (freeHeight - neededHeight) / 2.f,
							   dialogWidth,
							   neededHeight);
	} else if (freeHeight == neededHeight) {
		finalRect = CGRectMake((SCREEN_WIDTH - (dialogWidth)) / 2.f,
							   dialogMargin,
							   dialogWidth,
							   freeHeight);
	} else {
		finalRect = CGRectMake((SCREEN_WIDTH - (dialogWidth)) / 2.f,
							   dialogMargin,
							   dialogWidth,
							   freeHeight);
	}
	
	[UIView animateWithDuration:duration animations:^{
		self.view.frame = finalRect;
		[self.view layoutIfNeeded];
	}];

}

- (void)keyboardWasHidden:(NSNotification *)notification {
	
}






#pragma mark - UIViewControllerTransitioningDelegate

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
																   presentingController:(UIViewController *)presenting
																	   sourceController:(UIViewController *)source {
	
	DialogPresentationAnimationController *presentation = [[DialogPresentationAnimationController alloc] init];
	presentation.isPresenting = YES;
	return presentation;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
	DialogPresentationAnimationController *dismiss = [[DialogPresentationAnimationController alloc] init];
	dismiss.isPresenting = NO;
	return dismiss;
}

- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented
													  presentingViewController:(UIViewController *)presenting
														  sourceViewController:(UIViewController *)source {
	
	DialogPresentationController *controller = [[DialogPresentationController alloc] initWithPresentedViewController:presented
																							presentingViewController:presenting];
	DialogNavigationController * sdnc = (DialogNavigationController *)presented;
	DialogViewController * snvc = (DialogViewController *)[sdnc viewControllers].firstObject;
	controller.protocol = (id<DialogPresentationControllerProtocol>)snvc;
	snvc.pController = controller;
	return controller;
}

@end
