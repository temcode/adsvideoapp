//
//  SarzinDialogViewController.m
//  Sarzin
//
//  Created by Artem Selivanov on 1/27/17.
//  Copyright © 2017 Sarzin. All rights reserved.
//

#import "DialogViewController.h"
#import "DialogNavigationController.h"
#import "DialogPresentationController.h"
#import "DialogPresentationAnimationController.h"

#import "DialogAction.h"

#define maxAllowedSize (self.shouldHideNavBar ? maxPresentationSize : CGSizeMake(maxPresentationSize.width, maxPresentationSize.height - self.navigationController.navigationBar.frame.size.height))

@interface DialogViewController () <DialogPresentationControllerProtocol, UIViewControllerTransitioningDelegate>
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIView *backButtonView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIView *separator;

@property (weak, nonatomic) IBOutlet UIView *buttonsView;

@property (strong, nonatomic) NSLayoutConstraint *containerTop;
@property (strong, nonatomic) NSLayoutConstraint *containerBot;
@property (strong, nonatomic) NSLayoutConstraint *containerLead;
@property (strong, nonatomic) NSLayoutConstraint *containerTrall;

@property (strong, nonatomic) NSMutableArray <DialogAction *> * actions;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonsViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonsViewBotContraint;
@property (weak, nonatomic) IBOutlet UIView * buttonsScrollContentView;
@property (weak, nonatomic) IBOutlet UIView *buttonsViewSeparator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonsSepartorViewTopConstraint;
@property (weak, nonatomic) IBOutlet UIView *containerSuperView;

@property (assign, nonatomic) BOOL shouldHideNavBar;
@end

@implementation DialogViewController

- (void)dealloc {
	self.dialogTitle = nil;
	
	if ([self.backButtonView gestureRecognizers].count > 0) {
		for (UIGestureRecognizer *item in [self.backButtonView gestureRecognizers]) {
			[self.backButtonView removeGestureRecognizer:item];
		}
	}
	self.dialogContainerController = nil;

	if (self.actions) {
		[self.actions removeAllObjects];
		self.actions = nil;
	}
	
	self.containerBot = nil;
	self.containerTop = nil;
	self.containerLead = nil;
	self.containerTrall = nil;
	
	[DialogAction resetDefaults];
}

+ (DialogViewController *)dialogWithTitle:(NSString *)title containerController:(UIViewController *)containerController {
	DialogNavigationController *dialogNav = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"DialogNavigationController"];

	DialogViewController * dialog = (DialogViewController *)[dialogNav viewControllers].firstObject;
	
	dialog.dialogTitle = title;
	dialog.dialogContainerController = containerController;
	dialog.view.frame = CGRectMake(0, 0, dialogWidth, SCREEN_HEIGHT);
	return dialog;
}

- (void)presentWith:(UIViewController *)presentingController {

	[presentingController presentViewController:self.navigationController animated:YES completion:nil];
}

- (void)viewDidLoad {
	[super viewDidLoad];
	
	[self setNeedsStatusBarAppearanceUpdate];
	
	self.view.layer.cornerRadius = 5.f;
	self.view.clipsToBounds = YES;
	
	self.navigationController.navigationBar.barTintColor = [Colors shared].primary;
	
	UITapGestureRecognizer *backGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onBack:)];
	[self.backButtonView addGestureRecognizer:backGesture];
	
	self.titleLabel.text = self.dialogTitle;
	
	if (self.dialogContainerController && [self.dialogContainerController conformsToProtocol:@protocol(DialogContainerProtocol)]) {
		self.dialogProtocol = (id<DialogContainerProtocol>)self.dialogContainerController;
		
		if ([self.dialogProtocol respondsToSelector:@selector(configureDialogViewControllerElements:backButton:titleLabel:separator:)]) {
			[self.dialogProtocol configureDialogViewControllerElements:self
															backButton:self.backButton
															titleLabel:self.titleLabel
															 separator:self.separator];
		}
		
		if ([self.dialogProtocol respondsToSelector:@selector(shouldHideNavBar)]) {
			self.shouldHideNavBar = [self.dialogProtocol shouldHideNavBar];
		}
		
		if ([self.dialogProtocol respondsToSelector:@selector(dialogActionsForDialog:)]) {
			self.actions = [NSMutableArray arrayWithArray:[self.dialogProtocol dialogActionsForDialog:self]];
		}
		
		if (self.actions.count > 0) {
			[self setupActionButtons];
		} else {
			self.buttonsViewHeightConstraint.constant = 0.f;
		}
		
		[self displayContentController:self.dialogContainerController];
	}
}

- (UIStatusBarStyle)preferredStatusBarStyle {
	return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	if ([self sizeForPresentatingView].height > maxAllowedSize.height) {
		if ([self buttonsViewHeight] >= maxAllowedSize.height / 2.f) {
			if ([self containerNeededSize].height < maxAllowedSize.height / 2.f) {
				self.buttonsViewHeightConstraint.constant = maxAllowedSize.height - [self containerNeededSize].height;
			} else {
				self.buttonsViewHeightConstraint.constant = maxAllowedSize.height / 2.f;
			}
			[self.view layoutIfNeeded];
		}
	}

	if (self.shouldHideNavBar && self.shouldHideNavBar != self.navigationController.navigationBar.hidden) {
		[self.navigationController setNavigationBarHidden:self.shouldHideNavBar animated:NO];
	}
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	if ([self sizeForPresentatingView].height > maxAllowedSize.height) {
		if ([self buttonsViewHeight] >= maxAllowedSize.height / 2.f) {
			if ([self containerNeededSize].height < maxAllowedSize.height / 2.f) {
				self.buttonsViewHeightConstraint.constant = maxAllowedSize.height - [self containerNeededSize].height;
			} else {
				self.buttonsViewHeightConstraint.constant = maxAllowedSize.height / 2.f;
			}
			[self.view layoutIfNeeded];
		}
	}
}

- (void)resize {
	[self.pController resizeDialog];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (CGRect)frameOfPresentedViewInContainerView {

	return [self.pController frameOfPresentedViewInContainerView];
}

- (void)addAction:(DialogAction * _Nonnull)action {
	if (!self.actions) {
		self.actions = [NSMutableArray array];
	}
	
	if (action) {
		[self.actions addObject:action];
	}
}

- (void)setupActionButtons {
	CGFloat height = 0.f;
	CGFloat margin = [DialogAction buttonViewMargin] - ([DialogAction betweenButtonsMargin] / 2.f);
	
	if (self.actions.count == 1) {
		DialogAction * action = self.actions[0];
		action.parentDialog = self;
		UIView * button = [action actionButtonView];
		[self.buttonsScrollContentView addSubview:button];
		NSDictionary *views = NSDictionaryOfVariableBindings(button);
		
		NSString * constraintFormat = [NSString stringWithFormat:@"H:|-%f-[button]-%f-|", margin, margin];
		
		[self.buttonsScrollContentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:constraintFormat
																				  options:0
																				  metrics:nil
																					views:views]];
		
		
		constraintFormat = [NSString stringWithFormat:@"V:|-%f-[button]-%f-|", margin, margin];
		[self.buttonsScrollContentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:constraintFormat
																							  options:0
																							  metrics:nil
																								views:views]];
		

		height = [DialogAction dialogButtonHeight] + [DialogAction betweenButtonsMargin] + (margin * 2.f);
	} else if (self.actions.count == 2) {
		
		DialogAction * action0 = self.actions[0];
		DialogAction * action1 = self.actions[1];
		action0.parentDialog = self;
		action1.parentDialog = self;
		UIView * button0 = [action0 actionButtonView];
		UIView * button1 = [action1 actionButtonView];
		
		[self.buttonsScrollContentView addSubview:button0];
		[self.buttonsScrollContentView addSubview:button1];
		NSDictionary *views = NSDictionaryOfVariableBindings(button0, button1);
		
		
		
		NSString * constraintFormat = [NSString stringWithFormat:@"H:|-%f-[button0]-0-[button1]-%f-|", margin, margin];
		
		[self.buttonsScrollContentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:constraintFormat
																							  options:0
																							  metrics:nil
																								views:views]];
		
		[self.buttonsScrollContentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[button0]-0-|"
																							  options:0
																							  metrics:nil
																								views:views]];
		[self.buttonsScrollContentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[button1]-0-|"
																							  options:0
																							  metrics:nil
																								views:views]];
		
		[self.buttonsScrollContentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[button0(==button1)]"
																							  options:0
																							  metrics:nil
																								views:views]];
		
		action1.horizontalSeparator.hidden = NO;
		
		height = [DialogAction dialogButtonHeight] + [DialogAction betweenButtonsMargin] + (margin * 2.f);
		
	} else if (self.actions.count > 2) {
		for (int i = 0; i <= self.actions.count - 1; i++) {
			DialogAction * currentAction = self.actions[i];
			DialogAction * previousAction = nil;
			if (i > 0)
				previousAction = self.actions[i - 1];
			
			currentAction.parentDialog = self;
			
			UIView * currentButton = [currentAction actionButtonView];
			UIView * previousButton = nil;
			if (previousAction)
				previousButton = [previousAction actionButtonView];
			
			[self.buttonsScrollContentView addSubview:currentButton];
			
			NSDictionary *views = nil;
			if (previousAction)
				views = NSDictionaryOfVariableBindings(previousButton, currentButton);
			else
				views = NSDictionaryOfVariableBindings(currentButton);
			
			UIView * item = previousButton ? previousButton : self.buttonsScrollContentView;
			NSLayoutAttribute attr = previousButton ? NSLayoutAttributeBottom : NSLayoutAttributeTop;
			CGFloat constant = previousButton ? 0.f : margin;
			
			[[NSLayoutConstraint constraintWithItem:currentButton
										  attribute:NSLayoutAttributeTop
										  relatedBy:NSLayoutRelationEqual
											 toItem:item
										  attribute:attr
										 multiplier:1.0
										   constant:constant] setActive:YES];
			
			
			NSString * constraintFormat = [NSString stringWithFormat:@"H:|-%f-[currentButton]-%f-|", margin, margin];
			[self.buttonsScrollContentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:constraintFormat
																								  options:0
																								  metrics:nil
																									views:views]];
			
			
			
			
			if (i == self.actions.count - 1) {
				[[NSLayoutConstraint constraintWithItem:self.buttonsScrollContentView
											  attribute:NSLayoutAttributeBottom
											  relatedBy:NSLayoutRelationEqual
												 toItem:currentButton
											  attribute:NSLayoutAttributeBottom
											 multiplier:1.0f
											   constant:margin] setActive:YES];
			}
			
			currentAction.verticalSeparator.hidden = !previousButton;
			
			height += [DialogAction dialogButtonHeight] + [DialogAction betweenButtonsMargin] + ((i == 0 || (i == self.actions.count - 1)) ? margin : 0.f);
		}
	}
	
	self.buttonsViewHeightConstraint.constant = height + self.buttonsViewSeparator.frame.size.height;
	self.buttonsViewBotContraint.constant = 0.f;
	
}

- (CGSize)containerNeededSize {
	CGSize s = CGSizeZero;
	if (self.dialogProtocol && [self.dialogProtocol respondsToSelector:@selector(neededCotentSize)]) {
		s = [self.dialogProtocol neededCotentSize];
	}
	return s;
}

- (CGFloat)buttonsViewHeight {
	return self.buttonsViewHeightConstraint.constant;
}

#pragma mark - Navigation

//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//}

- (IBAction)onBack:(id)sender {
	[self.navigationController dismissViewControllerAnimated:YES
												  completion:nil];
}


- (void) hideContentController: (UIViewController*) content {
	[content willMoveToParentViewController:nil];
	[self.view removeConstraints:@[self.containerTop,
								   self.containerBot,
								   self.containerTrall,
								   self.containerLead]];
	self.containerTop = nil;
	self.containerBot = nil;
	self.containerTrall = nil;
	self.containerLead = nil;
	[content.view removeFromSuperview];
	[content removeFromParentViewController];
}

- (void) displayContentController: (UIViewController*) content {
	[self addChildViewController:content];
	content.view.translatesAutoresizingMaskIntoConstraints = NO;
	[self.containerSuperView addSubview:content.view];
	
	self.containerTop = [NSLayoutConstraint constraintWithItem:content.view
											attribute:NSLayoutAttributeTop
											relatedBy:NSLayoutRelationEqual
											   toItem:self.containerSuperView
											attribute:NSLayoutAttributeTop
										   multiplier:1.0
											 constant:0];
	
	self.containerBot = [NSLayoutConstraint constraintWithItem:content.view
											attribute:NSLayoutAttributeBottom
											relatedBy:NSLayoutRelationEqual
											   toItem:self.containerSuperView
											attribute:NSLayoutAttributeBottom
										   multiplier:1.0
											 constant:0];
	
	self.containerLead = [NSLayoutConstraint constraintWithItem:content.view
											attribute:NSLayoutAttributeLeading
											relatedBy:NSLayoutRelationEqual
											   toItem:self.containerSuperView
											attribute:NSLayoutAttributeLeading
										   multiplier:1.0
											 constant:0];
	
	self.containerTrall = [NSLayoutConstraint constraintWithItem:content.view
											attribute:NSLayoutAttributeTrailing
											relatedBy:NSLayoutRelationEqual
											   toItem:self.containerSuperView
											attribute:NSLayoutAttributeTrailing
										   multiplier:1.0
											 constant:0];
	
	
	[self.view addConstraints:@[self.containerTop,
								self.containerBot,
								self.containerLead,
								self.containerTrall]];
	
	[content didMoveToParentViewController:self];
}

#pragma mark - PresentationControllerProtocol -

- (CGSize)sizeForPresentatingView {

	CGSize s = [self containerNeededSize];
	
	s = CGSizeMake(s.width, s.height + [self buttonsViewHeight] + (!self.shouldHideNavBar ? self.navigationController.navigationBar.frame.size.height : 0.f));
	
	return s;
}

- (BOOL)shouldDismissPresentedControllerByOutsideTap {
//	if (self.actions.count == 0)
//		return NO;
	
	if (self.dialogProtocol && [self.dialogProtocol respondsToSelector:@selector(shouldDismissPresentedControllerByOutsideTap)]) {
		return [self.dialogProtocol shouldDismissPresentedControllerByOutsideTap];
	}
	
	return YES;
}


- (void)presentedControllerWillBeDismissed {
	if (self.dialogProtocol && [self.dialogProtocol respondsToSelector:@selector(presentedControllerWillBeDismissed)]) {
		[self.dialogProtocol presentedControllerWillBeDismissed];
	}
}
@end
