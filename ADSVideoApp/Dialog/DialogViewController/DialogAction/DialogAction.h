//
//  DialogAction.h
//  Sarzin
//
//  Created by Artem Selivanov on 1/28/17.
//  Copyright © 2017 Sarzin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DialogViewController.h"

@class DialogAction;
@class DialogViewController;

typedef void (^DialogActionHandler)(DialogAction * _Nonnull action);

@interface DialogAction : NSObject

@property (weak, nonatomic) DialogViewController * _Nullable parentDialog;


@property (strong, nonatomic) NSString * _Nullable titleText;

@property (strong, nonatomic) UIView * _Nullable actionButtonView;
@property (strong, nonatomic) UIView * _Nullable verticalSeparator;
@property (strong, nonatomic) UIView * _Nullable horizontalSeparator;

+ (void)resetDefaults;

+ (DialogAction * _Nullable)dialogActionWithTitle:(NSString * _Nullable)title
										   style:(UIAlertActionStyle)style
										 handler:(DialogActionHandler _Nullable)handler;

+ (DialogAction * _Nullable)dialogActionWithTitle:(NSString * _Nullable)title
										   style:(UIAlertActionStyle)style
					   dismissDialogAfterHandler:(BOOL)dismiss
										 handler:(DialogActionHandler _Nullable)handler;
- (void)performHandler;

#pragma mark - Getters -

+ (UIColor * _Nonnull)defaultButtonTitleColor;
+ (UIColor * _Nonnull)defaultButtonHighlightedTitleColor;
+ (UIColor * _Nonnull)defaultButtonBackgroundColor;
+ (UIColor * _Nonnull)defaultButtonHighlightedBackgroundColor;

+ (UIColor * _Nonnull)cancelButtonTitleColor;
+ (UIColor * _Nonnull)cancelButtonHighlightedTitleColor;
+ (UIColor * _Nonnull)cancelButtonBackgroundColor;
+ (UIColor * _Nonnull)cancelButtonHighlightedBackgroundColor;

+ (UIColor * _Nonnull)destructiveButtonTitleColor;
+ (UIColor * _Nonnull)destructiveButtonHighlightedTitleColor;
+ (UIColor * _Nonnull)destructiveButtonBackgroundColor;
+ (UIColor * _Nonnull)destructiveButtonHighlightedBackgroundColor;

+ (CGFloat)buttonsCornerRadius;
+ (CGFloat)buttonViewMargin;
+ (CGFloat)betweenButtonsMargin;
+ (CGFloat)dialogButtonHeight;

#pragma mark - Setters -

+ (void)setButtonViewMargin:(CGFloat)value;
+ (void)setBeetweenButtons:(CGFloat)value;
+ (void)setDialogButtonHeight:(CGFloat)value;

+ (void)setButtonsTitleFontSize:(CGFloat)value;
+ (void)setButtonCornerRadius:(CGFloat)value;
	
+ (void)setDefaultButtonTitleColor:(UIColor * _Nonnull)color;
+ (void)setDefaultButtonHighlightedTitleColor:(UIColor * _Nonnull)color;
+ (void)setDefaultButtonBackgroundColor:(UIColor * _Nonnull)color;
+ (void)setDefaultButtonHighlightedBackgroundColor:(UIColor * _Nonnull)color;

+ (void)setCancelButtonTitleColor:(UIColor * _Nonnull)color;
+ (void)setCancelButtonHighlightedTitleColor:(UIColor * _Nonnull)color;
+ (void)setCancelButtonBackgroundColor:(UIColor * _Nonnull)color;
+ (void)setCancelButtonHighlightedBackgroundColor:(UIColor * _Nonnull)color;

+ (void)setDestructiveButtonTitleColor:(UIColor * _Nonnull)color;
+ (void)setDestructiveButtonHighlightedTitleColor:(UIColor * _Nonnull)color;
+ (void)setDestructiveButtonBackgroundColor:(UIColor * _Nonnull)color;
+ (void)setDestructiveButtonHighlightedBackgroundColor:(UIColor * _Nonnull)color;



@end
