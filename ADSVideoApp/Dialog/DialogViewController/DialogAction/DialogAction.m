//
//  DialogAction.m
//  Sarzin
//
//  Created by Artem Selivanov on 1/28/17.
//  Copyright © 2017 Sarzin. All rights reserved.
//

#import "DialogAction.h"

//typedef void (^CompletionBlock)(CompletionStatus status, id _Nullable resultObj, NSError * _Nullable error);

@interface DialogAction ()

@property (copy, nonatomic) DialogActionHandler handler;
@property (assign, nonatomic) UIAlertActionStyle style;
@property (strong, nonatomic) NSString *title;
@property (assign, nonatomic) BOOL dismissDialogAfterHandler;

@property (strong, nonatomic) UIButton * _Nullable actionButton;

@end

static UIColor * defaultButtonTitleColor;
static UIColor * defaultButtonHighlightedTitleColor;
static UIColor * defaultButtonBackgroundColor;
static UIColor * defaultButtonHighlightedBackgroundColor;

static UIColor * cancelButtonTitleColor;
static UIColor * cancelButtonHighlightedTitleColor;
static UIColor * cancelButtonBackgroundColor;
static UIColor * cancelButtonHighlightedBackgroundColor;

static UIColor * destructiveButtonTitleColor;
static UIColor * destructiveButtonHighlightedTitleColor;
static UIColor * destructiveButtonBackgroundColor;
static UIColor * destructiveButtonHighlightedBackgroundColor;

static CGFloat buttonsCornerRadius = 10.f;
static CGFloat buttonsTitleFontSize = 14.f;
static CGFloat buttonViewMargin = 10.f;
static CGFloat betweenButtonsMargin = 5.f;
static CGFloat dialogButtonHeight = 35.f;

@implementation DialogAction

- (void)dealloc {
	self.actionButton = nil;
	self.title = nil;
	self.handler = nil;
	self.actionButtonView = nil;
	
	if (self.horizontalSeparator) {
		[self.horizontalSeparator removeFromSuperview];
		self.horizontalSeparator = nil;
	}
	if (self.verticalSeparator) {
		[self.verticalSeparator removeFromSuperview];
		self.verticalSeparator = nil;
	}
}

+ (void)resetDefaults {
	
	defaultButtonTitleColor = nil;
	defaultButtonHighlightedTitleColor = nil;
	defaultButtonBackgroundColor = nil;
	defaultButtonHighlightedBackgroundColor = nil;
	
	cancelButtonTitleColor = nil;
	cancelButtonHighlightedTitleColor = nil;
	cancelButtonBackgroundColor = nil;
	cancelButtonHighlightedBackgroundColor = nil;
	
	destructiveButtonTitleColor = nil;
	destructiveButtonHighlightedTitleColor = nil;
	destructiveButtonBackgroundColor = nil;
	destructiveButtonHighlightedBackgroundColor = nil;
	
	
	buttonsTitleFontSize = 14.f;
	buttonViewMargin = 10.f;
	betweenButtonsMargin = 5.f;
	dialogButtonHeight = 35.f;
	buttonsCornerRadius = 10.f;
}

+ (DialogAction *)dialogActionWithTitle:(NSString * _Nullable)title style:(UIAlertActionStyle)style handler:(DialogActionHandler)handler {
	return [DialogAction dialogActionWithTitle:title style:style dismissDialogAfterHandler:YES handler:handler];
}

+ (DialogAction * _Nullable)dialogActionWithTitle:(NSString * _Nullable)title style:(UIAlertActionStyle)style dismissDialogAfterHandler:(BOOL)dismiss handler:(DialogActionHandler)handler {
	if (title) {
		DialogAction * action = [[DialogAction alloc] init];
		action.handler = handler;
		action.style = style;
		action.title = title;
		action.dismissDialogAfterHandler = dismiss;
		
		return action;
	}
	return nil;
}

- (void)setHandler:(DialogActionHandler)handler {
	_handler = handler;
}


- (void)performHandler {
	if (self.handler) {
		self.handler(self);
	}
	if (self.dismissDialogAfterHandler) {
		if (self.parentDialog) {
			[self.parentDialog.presentingViewController dismissViewControllerAnimated:YES completion:nil];
		}
	}
}

- (UIButton *)actionButton {
	if (!_actionButton) {
		_actionButton = [[UIButton alloc] init];
		[_actionButton setTranslatesAutoresizingMaskIntoConstraints:NO];
		_actionButton.clipsToBounds = NO;
		
		UIFont *font = [UIFont boldSystemFontOfSize:buttonsTitleFontSize];
		[_actionButton.titleLabel setFont:font];
	
		
		[_actionButton setTitle:self.title forState:UIControlStateNormal];
		[_actionButton setTitle:self.title forState:UIControlStateHighlighted];
		_actionButton.titleLabel.textAlignment = NSTextAlignmentNatural;
		_actionButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
		_actionButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;

//		_actionButton.showsTouchWhenHighlighted = YES;
		_actionButton.reversesTitleShadowWhenHighlighted = YES;
		
//		[_actionButton addTarget:self action:@selector(onButtonHighlight:) forControlEvents:UIControlEventTouchDown | UIControlEventTouchDragExit | UIControlEventTouchDragOutside | UIControlEventTouchCancel | UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
		
		[_actionButton addTarget:self action:@selector(performHandler) forControlEvents:UIControlEventTouchUpInside];
		
		
		[_actionButton setTitleColor:[DialogAction defaultButtonTitleColor] forState:UIControlStateNormal];
		[_actionButton setTitleColor:[DialogAction defaultButtonHighlightedTitleColor] forState:UIControlStateHighlighted];
		
		_actionButton.layer.cornerRadius = buttonsCornerRadius;
		
		switch (self.style) {
			case UIAlertActionStyleCancel: {
				[_actionButton setTitleColor:[DialogAction cancelButtonTitleColor] forState:UIControlStateNormal];
				[_actionButton setTitleColor:[DialogAction cancelButtonHighlightedTitleColor] forState:UIControlStateHighlighted];
				[_actionButton setBackgroundColor:[DialogAction cancelButtonBackgroundColor]];
				break;
			}
			case UIAlertActionStyleDefault: {
				[_actionButton setTitleColor:[DialogAction defaultButtonTitleColor] forState:UIControlStateNormal];
				[_actionButton setTitleColor:[DialogAction defaultButtonHighlightedTitleColor] forState:UIControlStateHighlighted];
				[_actionButton setBackgroundColor:[DialogAction defaultButtonBackgroundColor]];
				break;
			}
			case UIAlertActionStyleDestructive: {
				[_actionButton setTitleColor:[DialogAction destructiveButtonTitleColor] forState:UIControlStateNormal];
				[_actionButton setTitleColor:[DialogAction destructiveButtonHighlightedTitleColor] forState:UIControlStateHighlighted];
				[_actionButton setBackgroundColor:[DialogAction destructiveButtonBackgroundColor]];
				break;
			}
			default:
				break;
		}
		
		
		[[NSLayoutConstraint constraintWithItem:_actionButton
									 attribute:NSLayoutAttributeHeight
									 relatedBy:NSLayoutRelationEqual
										toItem:nil
									 attribute:NSLayoutAttributeNotAnAttribute
									multiplier:1.0
									  constant:dialogButtonHeight] setActive:YES];
	}
	
	return _actionButton;
}

- (UIView *)actionButtonView {
	if (!_actionButtonView) {
		
		_actionButtonView = [[UIView alloc] init];
		_actionButtonView.translatesAutoresizingMaskIntoConstraints = NO;
		
		UIButton * button = self.actionButton;
		
		[_actionButtonView addSubview:button];
		
		NSDictionary *views = NSDictionaryOfVariableBindings(_actionButtonView, button);
		CGFloat margin = [DialogAction betweenButtonsMargin] / 2.f;
		
		NSString * constraintFormat = [NSString stringWithFormat:@"H:|-%f-[button]-%f-|", margin, margin];
		
		[_actionButtonView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:constraintFormat
																							  options:0
																							  metrics:nil
																								views:views]];
		constraintFormat = [NSString stringWithFormat:@"V:|-%f-[button]-%f-|", margin, margin];
		[_actionButtonView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:constraintFormat
																				  options:0
																				  metrics:nil
																					views:views]];
		
		self.verticalSeparator = [[UIView alloc] init];
		self.verticalSeparator.translatesAutoresizingMaskIntoConstraints = NO;
		self.verticalSeparator.hidden = YES;
		self.verticalSeparator.backgroundColor = [UIColor whiteColor];
		[_actionButtonView addSubview:self.verticalSeparator];
		
		NSLayoutConstraint * vSepHeight = [NSLayoutConstraint constraintWithItem:self.verticalSeparator
																	   attribute:NSLayoutAttributeHeight
																	   relatedBy:NSLayoutRelationEqual
																		  toItem:nil
																	   attribute:NSLayoutAttributeNotAnAttribute
																	  multiplier:1.f
																		constant:1.f];
		[vSepHeight setActive:YES];
		
		NSLayoutConstraint * vSepWidth = [NSLayoutConstraint constraintWithItem:self.verticalSeparator
																	  attribute:NSLayoutAttributeWidth
																	  relatedBy:NSLayoutRelationEqual
																		 toItem:_actionButtonView
																	  attribute:NSLayoutAttributeWidth
																	 multiplier:1.f
																	   constant:-30.f];
		
		NSLayoutConstraint * vSepCenterX = [NSLayoutConstraint constraintWithItem:self.verticalSeparator
																		attribute:NSLayoutAttributeCenterX
																		relatedBy:NSLayoutRelationEqual
																		   toItem:_actionButtonView
																		attribute:NSLayoutAttributeCenterX
																	   multiplier:1.f
																		 constant:0.f];
		
		NSLayoutConstraint * vSepTop = [NSLayoutConstraint constraintWithItem:self.verticalSeparator
																	attribute:NSLayoutAttributeTop
																	relatedBy:NSLayoutRelationEqual
																	   toItem:_actionButtonView
																	attribute:NSLayoutAttributeTop
																   multiplier:1.f
																	 constant:-0.5f];
		[NSLayoutConstraint activateConstraints:@[vSepWidth, vSepCenterX, vSepTop]];
		
		self.horizontalSeparator = [[UIView alloc] init];
		self.horizontalSeparator.translatesAutoresizingMaskIntoConstraints = NO;
		self.horizontalSeparator.hidden = YES;
		self.horizontalSeparator.backgroundColor = [UIColor whiteColor];
		[_actionButtonView addSubview:self.horizontalSeparator];
		
		NSLayoutConstraint *hSepWidth  = [NSLayoutConstraint constraintWithItem:self.horizontalSeparator
																	  attribute:NSLayoutAttributeWidth
																	  relatedBy:NSLayoutRelationEqual
																		 toItem:nil
																	  attribute:NSLayoutAttributeNotAnAttribute
																	 multiplier:1.f
																	   constant:1.f];
		[hSepWidth setActive:YES];
		
		NSLayoutConstraint * hSepHeight = [NSLayoutConstraint constraintWithItem:self.horizontalSeparator
																	   attribute:NSLayoutAttributeHeight
																	   relatedBy:NSLayoutRelationEqual
																		  toItem:_actionButtonView
																	   attribute:NSLayoutAttributeHeight
																	  multiplier:1.f
																		constant:-20.f];
		
		NSLayoutConstraint * hSepCenterY = [NSLayoutConstraint constraintWithItem:self.horizontalSeparator
																		attribute:NSLayoutAttributeCenterYWithinMargins
																		relatedBy:NSLayoutRelationEqual
																		   toItem:_actionButtonView
																		attribute:NSLayoutAttributeCenterYWithinMargins
																	   multiplier:1.f
																		 constant:0.f];
		
		NSLayoutConstraint * hSepLead = [NSLayoutConstraint constraintWithItem:self.horizontalSeparator
																	 attribute:NSLayoutAttributeLeading
																	 relatedBy:NSLayoutRelationEqual
																		toItem:_actionButtonView
																	 attribute:NSLayoutAttributeLeading
																	multiplier:1.f
																	  constant:-0.5f];
		
		[NSLayoutConstraint activateConstraints:@[hSepHeight, hSepCenterY, hSepLead]];
	}
	
	return _actionButtonView;
}

- (void)onButtonHighlight:(id)sender {
	
	switch (self.style) {
		case UIAlertActionStyleCancel: {
			[self.actionButton setBackgroundColor:self.actionButton.highlighted ? [DialogAction cancelButtonHighlightedBackgroundColor] : [DialogAction cancelButtonBackgroundColor]];
			break;
			}
		case UIAlertActionStyleDefault: {
			[self.actionButton setBackgroundColor:self.actionButton.highlighted ? [DialogAction defaultButtonHighlightedBackgroundColor] : [DialogAction defaultButtonBackgroundColor]];
				break;
		}
		case UIAlertActionStyleDestructive: {
			[self.actionButton setBackgroundColor:self.actionButton.highlighted ? [DialogAction destructiveButtonHighlightedBackgroundColor] : [DialogAction destructiveButtonBackgroundColor]];
			break;
		}
		default:
			break;
	}
}

#pragma mark - Getters -

- (NSString *)titleText {
	return _actionButton.titleLabel.text;
}

+ (UIColor *)defaultButtonTitleColor {
	return defaultButtonTitleColor ? : [UIColor whiteColor];
}

+ (UIColor *)defaultButtonHighlightedTitleColor {
	return defaultButtonHighlightedTitleColor ? : [UIColor lightGrayColor];
}

+ (UIColor *)defaultButtonBackgroundColor {
	return defaultButtonBackgroundColor ? : [UIColor darkGrayColor];
}

+ (UIColor *)defaultButtonHighlightedBackgroundColor {
	return defaultButtonHighlightedBackgroundColor ? : [UIColor clearColor];
}


+ (UIColor *)cancelButtonTitleColor {
	return cancelButtonTitleColor ? : [DialogAction defaultButtonTitleColor];
}

+ (UIColor *)cancelButtonHighlightedTitleColor {
	return cancelButtonHighlightedTitleColor ? : [DialogAction defaultButtonHighlightedTitleColor];
}

+ (UIColor *)cancelButtonBackgroundColor {
	return cancelButtonBackgroundColor ? : [DialogAction defaultButtonBackgroundColor];
}

+ (UIColor *)cancelButtonHighlightedBackgroundColor {
	return cancelButtonHighlightedBackgroundColor ? : [DialogAction defaultButtonHighlightedBackgroundColor];
}


+ (UIColor *)destructiveButtonTitleColor {
	return destructiveButtonTitleColor ? : [DialogAction defaultButtonTitleColor];
}

+ (UIColor *)destructiveButtonHighlightedTitleColor {
	return destructiveButtonHighlightedTitleColor ? : [DialogAction defaultButtonHighlightedTitleColor];
}

+ (UIColor *)destructiveButtonBackgroundColor {
	return destructiveButtonBackgroundColor ? : [DialogAction defaultButtonBackgroundColor];
}

+ (UIColor *)destructiveButtonHighlightedBackgroundColor {
	return destructiveButtonHighlightedBackgroundColor ? : [DialogAction defaultButtonHighlightedBackgroundColor];
}

+ (CGFloat)buttonsCornerRadius {
	return buttonsCornerRadius;
}

+ (CGFloat)buttonViewMargin {
	return buttonViewMargin;
}

+ (CGFloat)betweenButtonsMargin {
	return betweenButtonsMargin;
}

+ (CGFloat)dialogButtonHeight {
	return dialogButtonHeight;
}


#pragma mark - Setters -

- (void)setTitleText:(NSString *)titleText {
	[_actionButton setTitle:titleText forState:UIControlStateNormal];
	[_actionButton setTitle:titleText forState:UIControlStateHighlighted];
}

+ (void)setButtonViewMargin:(CGFloat)value {
	buttonViewMargin = value;
}

+ (void)setBeetweenButtons:(CGFloat)value {
	betweenButtonsMargin = value;
}

+ (void)setDialogButtonHeight:(CGFloat)value {
	dialogButtonHeight = value;
}


+ (void)setButtonsTitleFontSize:(CGFloat)value {
	buttonsTitleFontSize = value;
}

+ (void)setButtonCornerRadius:(CGFloat)value {
	buttonsCornerRadius = value;
}



+ (void)setDefaultButtonTitleColor:(UIColor *)color {
	defaultButtonTitleColor = color;
}

+ (void)setDefaultButtonHighlightedTitleColor:(UIColor *)color {
	defaultButtonHighlightedTitleColor = color;
}

+ (void)setDefaultButtonBackgroundColor:(UIColor *)color {
	defaultButtonBackgroundColor = color;
}

+ (void)setDefaultButtonHighlightedBackgroundColor:(UIColor *)color {
	defaultButtonHighlightedBackgroundColor = color;
}




+ (void)setCancelButtonTitleColor:(UIColor *)color {
	cancelButtonTitleColor = color;
}

+ (void)setCancelButtonHighlightedTitleColor:(UIColor *)color {
	cancelButtonHighlightedTitleColor = color;
}

+ (void)setCancelButtonBackgroundColor:(UIColor *)color {
	cancelButtonBackgroundColor = color;
}

+ (void)setCancelButtonHighlightedBackgroundColor:(UIColor *)color {
	cancelButtonHighlightedBackgroundColor = color;
}




+ (void)setDestructiveButtonTitleColor:(UIColor *)color {
	destructiveButtonTitleColor = color;
}

+ (void)setDestructiveButtonHighlightedTitleColor:(UIColor *)color {
	destructiveButtonHighlightedTitleColor = color;
}

+ (void)setDestructiveButtonBackgroundColor:(UIColor *)color {
	destructiveButtonBackgroundColor = color;
}

+ (void)setDestructiveButtonHighlightedBackgroundColor:(UIColor *)color {
	destructiveButtonHighlightedBackgroundColor = color;
}


@end
