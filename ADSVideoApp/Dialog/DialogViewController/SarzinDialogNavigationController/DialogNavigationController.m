//
//  SarzinDialogNavigationController.m
//  Sarzin
//
//  Created by Artem Selivanov on 1/27/17.
//  Copyright © 2017 Sarzin. All rights reserved.
//

#import "DialogNavigationController.h"

#import "DialogViewController.h"
#import "DialogPresentationController.h"
#import "DialogPresentationAnimationController.h"

@interface DialogNavigationController () <UIViewControllerTransitioningDelegate>

@end

@implementation DialogNavigationController

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	if (self) {
		if ([self respondsToSelector:@selector(setTransitioningDelegate:)]) {
			self.modalPresentationStyle = UIModalPresentationCustom;
			self.transitioningDelegate = self;
		}
	}
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UIViewControllerTransitioningDelegate

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
																   presentingController:(UIViewController *)presenting
																	   sourceController:(UIViewController *)source {
	
	DialogPresentationAnimationController *presentation = [[DialogPresentationAnimationController alloc] init];
	presentation.isPresenting = YES;
	return presentation;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
	DialogPresentationAnimationController *dismiss = [[DialogPresentationAnimationController alloc] init];
	dismiss.isPresenting = NO;
	return dismiss;
}

- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented
													  presentingViewController:(UIViewController *)presenting
														  sourceViewController:(UIViewController *)source {
	
	DialogPresentationController *controller = [[DialogPresentationController alloc] initWithPresentedViewController:presented
																							presentingViewController:presenting];
	DialogNavigationController * sdnc = (DialogNavigationController *)presented;
	DialogViewController * snvc = (DialogViewController *)[sdnc viewControllers].firstObject;
	controller.protocol = (id<DialogPresentationControllerProtocol>)snvc;
	return controller;
}

@end
