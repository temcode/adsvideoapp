//
//  SarzinDialogNavigationController.h
//  Sarzin
//
//  Created by Artem Selivanov on 1/27/17.
//  Copyright © 2017 Sarzin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DialogNavigationController : UINavigationController

@end
