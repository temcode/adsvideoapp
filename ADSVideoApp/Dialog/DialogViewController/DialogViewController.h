//
//  SarzinDialogViewController.h
//  Sarzin
//
//  Created by Artem Selivanov on 1/27/17.
//  Copyright © 2017 Sarzin. All rights reserved.
//

#import "DialogAction.h"
#import "DialogPresentationController.h"

#define dialogWidth IS_IPAD ? (IS_LANDSCAPE ?  (SCREEN_WIDTH / 2.f) : (SCREEN_WIDTH * 2.f / 3.f)) : (IS_LANDSCAPE ? (SCREEN_WIDTH * 2.f / 3.f) : (SCREEN_WIDTH - (dialogMargin * 2.f)))


@class DialogAction;
@class DialogViewController;

@protocol DialogContainerProtocol <NSObject>

- (CGSize)neededCotentSize;
- (BOOL)shouldDismissPresentedControllerByOutsideTap;
- (void)presentedControllerWillBeDismissed;
- (BOOL)shouldHideNavBar;
- (NSArray <DialogAction *> *)dialogActionsForDialog:(DialogViewController *)dialog;

@optional

- (void)configureDialogViewControllerElements:(DialogViewController *)dialogVC
								   backButton:(UIButton *)button
								   titleLabel:(UILabel *)label
									separator:(UIView *)separator;

@end


@interface DialogViewController : UIViewController

@property (weak, nonatomic) id<DialogContainerProtocol> dialogProtocol;

@property (weak, nonatomic) DialogPresentationController * pController;
@property (strong, nonatomic) UIViewController * dialogContainerController;
@property (strong, nonatomic) NSString * dialogTitle;

- (void)resize;

+ (DialogViewController *)dialogWithTitle:(NSString *)title containerController:(UIViewController *)containerController;
- (void)addAction:(DialogAction *)action;

- (void)presentWith:(UIViewController *)presentingController;

- (CGRect)frameOfPresentedViewInContainerView;

- (CGSize)sizeForPresentatingView;
@end
