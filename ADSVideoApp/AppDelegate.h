//
//  AppDelegate.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/3/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DownloadStore;
@class HWIFileDownloader;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow * _Nonnull window;

@property (nonnull, nonatomic, strong, readonly) DownloadStore *downloadStore;
@property (nonnull, nonatomic, strong, readonly) HWIFileDownloader *fileDownloader;

@property (assign, nonatomic) BOOL appInBackground;
@property (assign, nonatomic) BOOL appIsActive;

@end

