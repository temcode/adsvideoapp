//
//  VideoViewController.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/4/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Video.h"

@interface VideoViewController : UIViewController
@property (strong, nonatomic) Video * video;
@property (strong, nonatomic) VideoPoint * point;
@end
