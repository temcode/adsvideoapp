//
//  ViewController.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/3/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//



#import "SinglePackageViewController.h"
#import "Colors.h"
#import "ChooseViewController.h"

#define chooseVideosSegue @"VideosSegueId"
#define chooseDocumentSegue @"DocumentsSegueId"

@interface SinglePackageViewController () 
@property (weak, nonatomic) IBOutlet UILabel *appInstractionsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet UIButton *documentsButton;
@property (weak, nonatomic) IBOutlet UIButton *videosButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (weak, nonatomic) IBOutlet UIView *scrollContentsView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *height;
@property (weak, nonatomic) IBOutlet UIImageView *mainMenuImage;

@property (weak, nonatomic) IBOutlet UIButton *supportButton;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *appInstractionTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *supportButtonTop;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *documentsButtonTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *videosButtonTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *mainMenuImageTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *logoTop;
@end

@implementation SinglePackageViewController

- (void)dealloc {
	self.appInstractionTop = nil;
	self.supportButtonTop = nil;
	self.documentsButtonTop = nil;
	self.videosButtonTop = nil;
	self.logoTop = nil;
	self.mainMenuImageTop = nil;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	
	self.navigationController.navigationBar.barTintColor = [Colors shared].primary;

	if ([PackageSession sharedSession].error) {
		self.backButton.hidden = YES;
		self.scrollContentsView.hidden = YES;
	} else {
	
		[self.backButton setTitle:[PackageSession sharedSession].appTitle
						 forState:UIControlStateNormal];
		
		UIImage * logoImage = [UIImage imageWithContentsOfFile:logoPath];
		if (logoImage) {
			self.logo.image = logoImage;
			[self.logoTop setActive:YES];
			self.logo.hidden = NO;
		} else {
			[self.logoTop setActive:NO];
			self.logo.hidden = YES;
		}
		
		UIImage * mainImage = [UIImage imageWithContentsOfFile:splashPath];
		if (logoImage) {
			self.mainMenuImage.image = mainImage;
			[self.mainMenuImageTop setActive:YES];
			self.mainMenuImage.hidden = NO;
		} else {
			[self.mainMenuImageTop setActive:NO];
			self.mainMenuImage.hidden = YES;
		}
		
		if ([PackageSession sharedSession].appInstructions) {
			self.appInstractionsLabel.text = [PackageSession sharedSession].appInstructions;
			self.appInstractionsLabel.textColor = [Colors shared].secondary;
			[self.appInstractionTop setActive:YES];
			self.appInstractionsLabel.hidden = NO;
		} else {
			self.appInstractionsLabel.hidden = YES;
			[self.appInstractionTop setActive:NO];
		}
		
		
		if ([PackageSession sharedSession].videos && [PackageSession sharedSession].videos.count > 0) {
			
			[self.videosButton setTitle:[PackageSession sharedSession].videoActionTitle
							   forState:UIControlStateNormal];
			
			[self.videosButton setBackgroundColor:[Colors shared].secondary];
			
			[self.videosButtonTop setActive:YES];
			self.videosButton.hidden = NO;
			self.videosButton.layer.cornerRadius = 3.f;
		} else {
			[self.videosButtonTop setActive:NO];
			self.videosButton.hidden = YES;
		}
		
		if ([PackageSession sharedSession].documents && [PackageSession sharedSession].documents.count > 0) {
			
			[self.documentsButton setTitle:[PackageSession sharedSession].documentActionTitle
								  forState:UIControlStateNormal];
			
			[self.documentsButton setBackgroundColor:[Colors shared].secondary];
			
			[self.documentsButtonTop setActive:YES];
			self.documentsButton.hidden = NO;
			self.documentsButton.layer.cornerRadius = 3.f;
		} else {
			[self.documentsButtonTop setActive:NO];
			self.documentsButton.hidden = YES;
		}
		
		
		if ([PackageSession sharedSession].links && [PackageSession sharedSession].links.count > 0) {
			
			if ([PackageSession sharedSession].linkActionTitle) {
				[self.supportButton setTitle:[PackageSession sharedSession].linkActionTitle
									forState:UIControlStateNormal];
			} else {
				[self.supportButton setTitle:localized(@"ContactSupport")
									forState:UIControlStateNormal];
			}
			
			[self.supportButton setBackgroundColor:[Colors shared].secondary];
			self.supportButton.layer.cornerRadius = 3.f;
		} else {
			[self.supportButtonTop setActive:NO];
			self.supportButton.hidden = YES;
		}
	}
}


- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	if ([PackageSession sharedSession].error) {
		UIAlertController * alert = [UIAlertController alertControllerWithTitle:localized(@"Oops")
																		message:localized(@"FailedToParsePackageContent")
																 preferredStyle:UIAlertControllerStyleAlert];
		WELF_MACRO
		UIAlertAction * backAction = [UIAlertAction actionWithTitle:localized(@"Ok") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
			[welf onBack:nil];
		}];
		
		[alert addAction:backAction];
		
		[self.navigationController presentViewController:alert animated:YES completion:nil];
	
	} else {
		CGRect c = self.scrollContentsView.frame;
		CGRect v = self.view.frame;
		BOOL enabled = c.size.height > v.size.height;
		[self.scrollView setScrollEnabled:enabled];
	
	}
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
	[super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
	
	CGRect c = self.scrollContentsView.frame;
	//	CGRect s = self.scrollView.frame;
//	CGRect v = self.view.frame;
	BOOL enabled = c.size.height > size.height;
	[self.scrollView setScrollEnabled:enabled];
}


- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
//	sdfsd
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	if ([segue.identifier isEqualToString:chooseVideosSegue]) {
		((ChooseViewController *)segue.destinationViewController).controllerType = ChooseViewControllerVideos;
	} else if ([segue.identifier isEqualToString:chooseDocumentSegue]) {
		((ChooseViewController *)segue.destinationViewController).controllerType = ChooseViewControllerDocuments;
	}
}

- (IBAction)onBack:(id)sender {
	[[PackageSession sharedSession] clearSession];
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onSupportButton:(id)sender {
}




@end
