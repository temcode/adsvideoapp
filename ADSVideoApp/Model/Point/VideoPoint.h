//
//  Point.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/4/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoPoint : NSObject

@property (strong, nonatomic) NSString * title;
@property (strong, nonatomic) NSString * timestamp;


+ (VideoPoint *)parsePointFrom:(NSDictionary *)dict;

- (NSTimeInterval)time;

@end
