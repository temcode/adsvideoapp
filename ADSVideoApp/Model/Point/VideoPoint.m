//
//  Point.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/4/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "VideoPoint.h"

@implementation VideoPoint

- (void)dealloc {
	self.title = nil;
	self.timestamp = nil;
}

+ (VideoPoint *)parsePointFrom:(NSDictionary *)dict {
	VideoPoint * point = [[VideoPoint alloc] init];
	point.title = dict[titleAPIKey];
	point.timestamp = dict[timestampAPIKey];
	return point;
}

- (NSTimeInterval)time {
	NSTimeInterval timestamp = 0.f;
	
	NSArray * timeArray = [self.timestamp componentsSeparatedByString:@":"];
	NSString * hoursString = timeArray[0];
	NSString * minutesString = timeArray[1];
	NSString * secondsString = timeArray[2];
	NSInteger hours = [hoursString integerValue];
	NSInteger minutes = [minutesString integerValue];
	NSInteger seconds = [secondsString integerValue];
	
	timestamp = hours * 60.f * 60.f + minutes * 60 + seconds;
	
	return timestamp;
}

@end
