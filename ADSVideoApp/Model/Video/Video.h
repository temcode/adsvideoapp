//
//  Video.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/4/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VideoPoint.h"

typedef NS_ENUM(NSInteger, VideoElement) {
	VideoElementFile,
	VideoElementHtml
};


@interface Video : NSObject

@property (assign, nonatomic) NSInteger identifier;
@property (strong, nonatomic) NSString * title;
@property (strong, nonatomic) NSArray <VideoPoint *> * points;

+ (Video *)parseVideoFrom:(NSDictionary *)dict;

+ (NSString *)pathExtensionFor:(VideoElement)element;

- (NSURL *)mp4URL;
- (NSString *)mp4Path;
- (NSString *)htmlPath;
- (NSString *)HTML;
- (NSString *)videoFolderPath;

@end
