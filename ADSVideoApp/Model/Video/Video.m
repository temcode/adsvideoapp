//
//  Video.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/4/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "Video.h"

@implementation Video

- (void)dealloc {
	self.title = nil;
	self.points = nil;
}

+ (Video *)parseVideoFrom:(NSDictionary *)dict {
	Video * video = [[Video alloc] init];
	video.title = dict[titleAPIKey];
	video.identifier = dict[idAPIKey] ? [dict[idAPIKey] integerValue] : -1;
	NSMutableArray * points = [NSMutableArray array];
	for (NSDictionary * pointDict in dict[pointsAPIKey]) {
		[points addObject:[VideoPoint parsePointFrom:pointDict]];
	}
	video.points = [NSArray arrayWithArray:points];
	
	return video;
}

+ (NSString *)pathExtensionFor:(VideoElement)element {
	NSString * pathExtension = nil;
	
	switch (element) {
		case VideoElementFile: {
			pathExtension = mp4Extension;
			break;
		}
		case VideoElementHtml: {
			pathExtension = htmlExtension;
			break;
		}
		default:
			break;
	}
	return pathExtension;
}

- (NSURL *)mp4URL {
	return videoFileURLById(self.identifier);
}

- (NSString *)mp4Path {
	return videoFilePathById(self.identifier);
}

- (NSString *)htmlPath {
	return videoHTMLPathById(self.identifier);
}

- (NSString *)HTML {
	return videoHTMLById(self.identifier);
}

- (NSString *)videoFolderPath {
	return videoFolderPathById(self.identifier);
}

@end
