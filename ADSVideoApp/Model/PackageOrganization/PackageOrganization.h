//
//  PackageOrganization.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 3/1/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface PackageOrganization : NSObject

@property (assign, nonatomic) NSInteger ID;
@property (strong, nonatomic) NSString * title;
@property (assign, nonatomic) BOOL isPublic;

@property (strong, nonatomic) NSString * supportEmailAddress;

@property (strong, nonatomic) NSString * created;
@property (strong, nonatomic) NSString * modified;

@property (strong, nonatomic) NSString * prefix;

@property (strong, nonatomic) UIColor * defaultPrimaryColor;
@property (strong, nonatomic) UIColor * defaultSecondaryColor;

@property (strong, nonatomic) NSString * defaultVideoButtonTitle;
@property (strong, nonatomic) NSString * defaultDocumentsButtonTitle;
@property (strong, nonatomic) NSString * defaultSupportButtonTitle;

@property (strong, nonatomic) NSString * defaultSupportEmailAddress;

- (instancetype)initWithId:(NSInteger)ID
					 title:(NSString *)title
	   supportEmailAddress:(NSString *)supportEmailAddress
				  isPublic:(BOOL)isPublic
				   created:(NSString *)created
				  modified:(NSString *)modified
					prefix:(NSString *)prefix
	   defaultPrimaryColor:(UIColor *)defaultPrimaryColor
	 defaultSecondaryColor:(UIColor *)defaultSecondaryColor
   defaultVideoButtonTitle:(NSString *)defaultVideoButtonTitle
defaultDocumentsButtonTitle:(NSString *)defaultDocumentsButtonTitle
 defaultSupportButtonTitle:(NSString *)defaultSupportButtonTitle
defaultSupportEmailAddress:(NSString *)defaultSupportEmailAddress;


- (instancetype)initWithDict:(NSDictionary *)dict;

- (NSDictionary *)prepareForDefaults;

- (NSString *)idString;


@end
