//
//  PackageOrganization.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 3/1/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "PackageOrganization.h"

@implementation PackageOrganization


- (instancetype)initWithId:(NSInteger)ID
					 title:(NSString *)title
	   supportEmailAddress:(NSString *)supportEmailAddress
				  isPublic:(BOOL)isPublic
				   created:(NSString *)created
				  modified:(NSString *)modified
					prefix:(NSString *)prefix
	   defaultPrimaryColor:(UIColor *)defaultPrimaryColor
	 defaultSecondaryColor:(UIColor *)defaultSecondaryColor
   defaultVideoButtonTitle:(NSString *)defaultVideoButtonTitle
defaultDocumentsButtonTitle:(NSString *)defaultDocumentsButtonTitle
 defaultSupportButtonTitle:(NSString *)defaultSupportButtonTitle
defaultSupportEmailAddress:(NSString *)defaultSupportEmailAddress {
	self = [super init];
	if (self) {
		self.ID = ID;
		self.title = title;
		self.isPublic = isPublic;
		self.supportEmailAddress = supportEmailAddress;
		self.created = created;
		self.modified = modified;
		self.prefix = prefix;
		self.defaultPrimaryColor = defaultPrimaryColor;
		self.defaultSecondaryColor = defaultSecondaryColor;
		self.defaultVideoButtonTitle = defaultVideoButtonTitle;
		self.defaultDocumentsButtonTitle = defaultDocumentsButtonTitle;
		self.defaultSupportButtonTitle = defaultSupportButtonTitle;
		self.defaultSupportEmailAddress = defaultSupportEmailAddress;
	}
	return self;
}

- (instancetype)initWithDict:(NSDictionary *)dict {
	self = [super init];
	if (self) {
		
		self.ID = [dict containsValueForKey:idAPIKey] ? [dict[idAPIKey] integerValue] : -1;
		self.title = [dict containsValueForKey:nameAPIKey] ? dict[nameAPIKey] : nil;
		self.isPublic = [dict containsValueForKey:isPublicAPIKey] ? [dict[isPublicAPIKey] boolValue] : NO;
		self.supportEmailAddress = [dict containsValueForKey:supportEmailAPIKey] ? dict[supportEmailAPIKey] : nil;
		self.defaultSupportEmailAddress = [dict containsValueForKey:supportEmailAPIKey] ? dict[supportEmailAPIKey] : nil;
		
//		self.created = [dict containsValueForKey:createdAPIKey] ? [(NSString *)dict[createdAPIKey] length] > 0 ? [Utils getDateFromString:dict[createdAPIKey]] : nil : nil ;
//		
//		self.modified = [dict containsValueForKey:modifiedAPIKey] ? [(NSString *)dict[modifiedAPIKey] length] > 0 ? [Utils getDateFromString:dict[modifiedAPIKey]] : nil : nil ;
		self.created = [dict containsValueForKey:createdAPIKey] ? dict[createdAPIKey] : nil ;
		
		self.modified = [dict containsValueForKey:modifiedAPIKey] ? dict[modifiedAPIKey] : nil ;
		
		self.prefix = [dict containsValueForKey:prefixAPIKey] ? dict[prefixAPIKey] : nil;
		
		self.defaultPrimaryColor = [dict containsValueForKey:defaultPrimaryColorAPIKey] ? uiColorWithHexString(dict[defaultPrimaryColorAPIKey]) : nil;
		self.defaultSecondaryColor = [dict containsValueForKey:defaultSecondaryColorAPIKey] ? uiColorWithHexString(dict[defaultSecondaryColorAPIKey]) : nil;
		
		self.defaultVideoButtonTitle = [dict containsValueForKey:defaultVideoButtonTitleAPIKey] ? dict[defaultVideoButtonTitleAPIKey] : nil;
		self.defaultDocumentsButtonTitle = [dict containsValueForKey:defaultDocumentsButtonTitleAPIKey] ? dict[defaultDocumentsButtonTitleAPIKey] : nil;
		self.defaultSupportButtonTitle = [dict containsValueForKey:defaultSupportButtonTitleAPIKey] ? dict[defaultSupportButtonTitleAPIKey] : nil;
		
		self.defaultSupportEmailAddress = [dict containsValueForKey:defaultSupportEmailAPIKey] ? dict[defaultSupportEmailAPIKey] : nil;
	}
	
	return self;
}

- (NSDictionary *)prepareForDefaults {
	NSMutableDictionary * dict = [NSMutableDictionary dictionary];
	
	[dict setObject:[self idNumber] forKey:idAPIKey];
	
	if (self.title && self.title.length > 0) {
		[dict setObject:self.title forKey:nameAPIKey];
	}
	
	if (self.supportEmailAddress && self.supportEmailAddress.length > 0) {
		[dict setObject:self.supportEmailAddress forKey:supportEmailAPIKey];
	}
	
	[dict setObject:[NSNumber numberWithBool:self.isPublic] forKey:isPublicAPIKey];
	
	if (self.created) {
//		NSString * createdDateString = [NSString stringWithFormat:@"%@", self.created];
		[dict setObject:self.created forKey:createdAPIKey];
	}
	
	if (self.modified) {
//		NSString * modifiedDateString = [NSString stringWithFormat:@"%@", self.modified];
		[dict setObject:self.modified forKey:modifiedAPIKey];
	}
	
	if (self.prefix) {
		[dict setObject:self.prefix forKey:prefixAPIKey];
	}
	
	if (self.defaultPrimaryColor) {
		[dict setObject:self.defaultPrimaryColor.hexStringValue forKey:defaultPrimaryColorAPIKey];
	}
	
	if (self.defaultSecondaryColor) {
		[dict setObject:self.defaultSecondaryColor.hexStringValue forKey:defaultSecondaryColorAPIKey];
	}
	
	if (self.defaultVideoButtonTitle) {
		[dict setObject:self.defaultVideoButtonTitle forKey:defaultVideoButtonTitleAPIKey];
	}
	
	
	if (self.defaultDocumentsButtonTitle) {
		[dict setObject:self.defaultDocumentsButtonTitle forKey:defaultDocumentsButtonTitleAPIKey];
	}
	
	
	if (self.defaultSupportButtonTitle) {
		[dict setObject:self.defaultSupportButtonTitle forKey:defaultSupportButtonTitleAPIKey];
	}
	
	if (self.defaultSupportEmailAddress) {
		[dict setObject:self.defaultSupportEmailAddress forKey:defaultSupportEmailAPIKey];
	}
	
	return dict;
}


- (NSNumber *)idNumber {
	return [NSNumber numberWithInteger:self.ID];
}

- (NSString *)idString {
	return [NSString stringWithFormat:@"%ld", (long)self.ID];
}
@end
