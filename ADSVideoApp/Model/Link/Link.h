//
//  Link.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 3/3/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, LinkType) {
	LinkTypeNotSpecified = -99,
	LinkTypeEmail = 0,
	LinkTypeWeb,
	LinkTypePhone
};


@interface Link : NSObject

@property (assign, nonatomic) NSInteger ID;
@property (assign, nonatomic) LinkType type;
@property (strong, nonatomic) NSString * linkString;
@property (strong, nonatomic) NSString * title;

- (instancetype)initWithDict:(NSDictionary *)dict;
- (instancetype)initWithId:(NSInteger)ID type:(LinkType)type link:(NSString *)link title:(NSString *)title;

- (NSDictionary *)prepareForDefults;

+ (LinkType)linkTypeWith:(NSString *)typeString;

+ (NSString *)typeStringWithLinkType:(LinkType)linkType;
- (NSString *)typeString;
- (NSURL *)URL;
@end
