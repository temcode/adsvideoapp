//
//  Link.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 3/3/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "Link.h"

@implementation Link

- (instancetype)initWithDict:(NSDictionary *)dict {
	self = [super init];
	if (self) {
		self.ID = [dict containsValueForKey:idAPIKey] ? [dict[idAPIKey] integerValue] : -99;
		self.type = [dict containsValueForKey:typeAPIKey] ? [Link linkTypeWith: dict[typeAPIKey]] : LinkTypeNotSpecified;
		self.linkString = [dict containsValueForKey:linkAPIKey] ? dict[linkAPIKey] : nil;
		self.title = [dict containsValueForKey:labelAPIKey] ? dict[labelAPIKey] : nil;
		
	}
	return self;
}

+ (LinkType)linkTypeWith:(NSString *)typeString {
	if ([typeString isEqualToString:emailLinkType]) {
		return LinkTypeEmail;
	} else if ([typeString isEqualToString:phoneLinkType]) {
		return LinkTypePhone;
	} else if ([typeString isEqualToString:webLinkType]) {
		return LinkTypeWeb;
	}
	return LinkTypeNotSpecified;
}

+ (NSString *)typeStringWithLinkType:(LinkType)linkType {
	switch (linkType) {
		case LinkTypeEmail: {
			return emailLinkType;
			break;
		}
		case LinkTypePhone: {
			return phoneLinkType;
			break;
		}
		case LinkTypeWeb: {
			return webLinkType;
			break;
		}
		default:
			break;
	}
	return nil;
}

- (NSString *)typeString {
	return [Link typeStringWithLinkType:self.type];
}

- (instancetype)initWithId:(NSInteger)ID type:(LinkType)type link:(NSString *)link title:(NSString *)title {
	self = [super init];
	if (self) {
		self.ID = ID;
		self.type = type;
		self.linkString = link;
		self.title = title;
	}
	return self;
}

- (NSDictionary *)prepareForDefults {

	NSMutableDictionary * dict = [NSMutableDictionary dictionary];
	
	[dict setObject:@(self.ID) forKey:idAPIKey];
	
	if (self.type != LinkTypeNotSpecified) {
		[dict setObject:[self typeString] forKey:typeAPIKey];
	}
	
	if (self.linkString) {
		[dict setObject:self.linkString forKey:linkAPIKey];
	}
	
	if (self.title) {
		[dict setObject:self.title forKey:labelAPIKey];
	}
	
	return dict;
}

- (NSURL *)URL {
	NSURL * url = nil;
	switch (self.type) {
		case LinkTypeEmail: {
			break;
		}
		case LinkTypePhone: {
			NSString *phoneURLString = [[NSString stringWithFormat:@"tel://%@", self.linkString] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
			url = [NSURL URLWithString:phoneURLString];
			break;
		}
		case LinkTypeWeb: {
			
			url = [NSURL URLWithString:self.linkString];
			break;
		}
		default:
			break;
	}
	
	return url;
}

@end
