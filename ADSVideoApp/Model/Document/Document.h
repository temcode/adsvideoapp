//
//  Document.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/4/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Document : NSObject

@property (strong, nonatomic) NSString * filename;
@property (strong, nonatomic) NSString * title;
@property (assign, nonatomic) NSInteger identifier;

+ (Document *)parseDocumentFrom:(NSDictionary *)dict;

- (NSURL *)URL;

@end
