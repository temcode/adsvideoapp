//
//  Document.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/4/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "Document.h"

@implementation Document

- (void)dealloc {
	self.title = nil;
	self.filename = nil;
}

+ (Document *)parseDocumentFrom:(NSDictionary *)dict {
	Document * document = [[Document alloc] init];
	
	document.title = dict[titleAPIKey];
	document.filename = dict[filenameAPIKey];
	document.identifier = dict[idAPIKey] ? [dict[idAPIKey] integerValue] : -1;
	
	return document;
}


- (NSURL *)URL {
	return documentFileURLById(self.identifier);
}

@end
