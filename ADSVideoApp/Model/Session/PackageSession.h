//
//  Session.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/4/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Package.h"

#import "Video.h"
#import "VideoPoint.h"
#import "Document.h"
#import "Link.h"


@interface PackageSession : NSObject

@property (strong, nonatomic) Package * package;

@property (strong, nonatomic) NSString * appTitle;
@property (strong, nonatomic) NSString * appInstructions;
@property (strong, nonatomic) NSString * supportActionTitle;
@property (strong, nonatomic) NSString * supportEmail;

@property (strong, nonatomic) NSString * videoActionTitle;
@property (strong, nonatomic) NSString * videoActivityTitle;

@property (strong, nonatomic) NSString * documentActionTitle;

@property (strong, nonatomic) NSString * linkActionTitle;

@property (strong, nonatomic) NSArray <Video *> * videos;
@property (strong, nonatomic) NSArray <Document *> * documents;
@property (strong, nonatomic) NSArray <Link *> * links;

@property (strong, nonatomic) NSError * error;

+ (PackageSession *)sharedSession;

- (void)clearSession;

@end
