//
//  Session.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/4/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "PackageSession.h"

@implementation PackageSession

+ (PackageSession *)sharedSession {
	static PackageSession *sharedSession = nil;
	static dispatch_once_t onceToken;
	
	dispatch_once(&onceToken, ^{
		sharedSession = [[PackageSession alloc] init];
	});
	
	return sharedSession;
}

- (instancetype)init {
	self = [super init];
	if (self) {
		[self clearSession];
		return self;
	}
	return nil;
}

- (void)clearSession {
	_package = nil;
	
	self.appTitle = nil;
	self.appInstructions = nil;
	
//	self.supportEmail = nil;
//	self.supportActionTitle = nil;

	self.links = nil;
	self.linkActionTitle = nil;
	
	self.videoActionTitle = nil;
	self.videoActivityTitle = nil;
	self.documentActionTitle = nil;
	
	self.videos = nil;
	self.documents = nil;
}

- (void)setPackage:(Package *)package {
	[self clearSession];
	_package = package;
	self.error = nil;
	NSError * error = nil;
	NSDictionary * sessionDict = [Utils getSessionConfigurationDictionaryForPackageWithError:&error];
	if (error) {
		self.error = error;
	}
	[self parseSessionFrom:sessionDict];
}

- (void)parseSessionFrom:(NSDictionary *)sessionDict {
	
	if (sessionDict) {
		self.appTitle = [sessionDict containsValueForKey:appTitleAPIKey] ? sessionDict[appTitleAPIKey] : nil;
		self.appInstructions = [sessionDict containsValueForKey:appInstructionAPIKey] ? sessionDict[appInstructionAPIKey] : nil;
		
//		if ([sessionDict containsValueForKey:supportActivityAPIKey]) {
//			NSDictionary * supportActivityDict = sessionDict[supportActivityAPIKey];
//			self.supportEmail = [supportActivityDict containsValueForKey:supportEmailAPIKey] ? supportActivityDict[supportEmailAPIKey] : nil;
//			self.supportActionTitle = [supportActivityDict containsValueForKey:supportActionTitleAPIKey] ? supportActivityDict[supportActionTitleAPIKey] : nil;
//		}
		
		[Colors shared].primary = [sessionDict containsValueForKey:appPrimaryColorAPIKey] ? uiColorWithHexString(sessionDict[appPrimaryColorAPIKey]) : (self.package ? (self.package.organization ? self.package.organization.defaultPrimaryColor : [Colors shared].primary) : [Colors shared].primary);
		
		[Colors shared].secondary = [sessionDict containsValueForKey:appSecondaryColorAPIKey] ? uiColorWithHexString(sessionDict[appSecondaryColorAPIKey]) : (self.package ? (self.package.organization ? self.package.organization.defaultSecondaryColor : [Colors shared].secondary) : [Colors shared].secondary);
		
		[SVProgressHUD setBackgroundColor:[Colors shared].primary];
		
		NSDictionary * videosActivityDict = [sessionDict containsValueForKey:videoActivityAPIKey] ? sessionDict[videoActivityAPIKey] : nil;
		
		if (videosActivityDict) {
			
			self.videoActionTitle = [videosActivityDict containsValueForKey:videoActionTitleAPIKey] ? videosActivityDict[videoActionTitleAPIKey] : nil;
			self.videoActivityTitle = [videosActivityDict containsValueForKey:titleAPIKey] ? videosActivityDict[titleAPIKey] : nil;
			if ([videosActivityDict containsValueForKey:videosAPIKey]) {
				NSMutableArray * videosArray = [NSMutableArray array];
				for (NSDictionary * videoDict in videosActivityDict[videosAPIKey]) {
					[videosArray addObject:[Video parseVideoFrom:videoDict]];
				}
				
				self.videos = [NSArray arrayWithArray:videosArray];
			}
		}
		
		NSDictionary * documentsActivityDict = [sessionDict containsValueForKey:documentActivityAPIKey] ? sessionDict[documentActivityAPIKey] : nil;
		if (documentsActivityDict) {
			self.documentActionTitle = [documentsActivityDict containsValueForKey:documentActionTitleAPIKey] ?  documentsActivityDict[documentActionTitleAPIKey] : nil;
			if ([documentsActivityDict containsValueForKey:documentsAPIKey]) {
				NSMutableArray * documentsArray = [NSMutableArray array];
				for (NSDictionary * documentDict in documentsActivityDict[documentsAPIKey]) {
					[documentsArray addObject:[Document parseDocumentFrom:documentDict]];
				}
				self.documents = [NSArray arrayWithArray:documentsArray];
			}
		}
		
		NSDictionary * linksActivityDict = [sessionDict containsValueForKey:linksActivityAPIKey] ? sessionDict[linksActivityAPIKey] : nil;
		if (linksActivityDict) {
			self.linkActionTitle = [linksActivityDict containsValueForKey:linksActionTitleAPIKey] ? linksActivityDict[linksActionTitleAPIKey] : nil;
			
			if ([linksActivityDict containsValueForKey:linksAPIKey]) {
				NSMutableArray <Link *> * links = [NSMutableArray array];
				for (NSDictionary * linkDict in linksActivityDict[linksAPIKey]) {
					[links addObject:[[Link alloc] initWithDict:linkDict]];
				}
				self.links = [links copy];
			}
		}
	} else {
		[self dummySession];
	}
}

- (void)dummySession {
	
	self.appTitle = @"App Title";
	self.documentActionTitle = @"Doc Action";
	self.videoActionTitle = @"Video Action";
	self.videos = @[];
	self.documents = @[];
}



@end
