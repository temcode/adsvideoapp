//
//  Organization.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/16/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "PackageOrganization.h"
#import "Package.h"

@interface Organization : PackageOrganization

@property (strong, nonatomic) NSArray <Package *> * packages;

- (instancetype)initWithId:(NSInteger)ID
					 title:(NSString *)title
				  packages:(NSArray <Package *> *)packages
	   supportEmailAddress:(NSString *)supportEmailAddress
				  isPublic:(BOOL)isPublic
				   created:(NSString *)created
				  modified:(NSString *)modified
					prefix:(NSString *)prefix
	   defaultPrimaryColor:(UIColor *)defaultPrimaryColor
defaultSecondaryColor:(UIColor *)defaultSecondaryColor
defaultVideoButtonTitle:(NSString *)defaultVideoButtonTitle
defaultDocumentsButtonTitle:(NSString *)defaultDocumentsButtonTitle
defaultSupportButtonTitle:(NSString *)defaultSupportButtonTitle
defaultSupportEmailAddress:(NSString *)defaultSupportEmailAddress;


- (PackageOrganization *)packageOrganization;
@end
