//
//  Organization.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/16/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "Organization.h"
#import "Package.h"

@implementation Organization


- (instancetype)initWithId:(NSInteger)ID
					 title:(NSString *)title
				  packages:(NSArray<Package *> *)packages
	   supportEmailAddress:(NSString *)supportEmailAddress
				  isPublic:(BOOL)isPublic
				   created:(NSString *)created
				  modified:(NSString *)modified
					prefix:(NSString *)prefix
	   defaultPrimaryColor:(UIColor *)defaultPrimaryColor
	 defaultSecondaryColor:(UIColor *)defaultSecondaryColor
   defaultVideoButtonTitle:(NSString *)defaultVideoButtonTitle
defaultDocumentsButtonTitle:(NSString *)defaultDocumentsButtonTitle
 defaultSupportButtonTitle:(NSString *)defaultSupportButtonTitle
defaultSupportEmailAddress:(NSString *)defaultSupportEmailAddress {
	self = [super initWithId:ID
					   title:title
		 supportEmailAddress:supportEmailAddress
					isPublic:isPublic
					 created:created
					modified:modified
					  prefix:prefix
		 defaultPrimaryColor:defaultPrimaryColor
	   defaultSecondaryColor:defaultSecondaryColor
	 defaultVideoButtonTitle:defaultVideoButtonTitle
 defaultDocumentsButtonTitle:defaultDocumentsButtonTitle
   defaultSupportButtonTitle:defaultSupportButtonTitle
  defaultSupportEmailAddress:defaultSupportEmailAddress];
	if (self) {
		
		self.packages = packages;
	}
	return self;
}

- (instancetype)initWithDict:(NSDictionary *)dict {
	self = [super initWithDict:dict];
	if (self) {
		self.packages = nil;
		if ([dict containsValueForKey:packagesAPIKey]) {
			NSArray * packageDictionaties = dict[packagesAPIKey];
			NSMutableArray <Package *> * packages = [NSMutableArray arrayWithCapacity:packageDictionaties.count];
			for (NSDictionary * dict in packageDictionaties) {
				[packages addObject:[[Package alloc] initWithDict:dict packageOrganiztion:self.packageOrganization]];
			}
			self.packages = packages;
		}
	}
	
	return self;
}


- (NSDictionary *)prepareForDefaults {
	NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithDictionary:[super prepareForDefaults]];
	
	if (self.packages && self.packages.count > 0) {
		NSMutableArray <NSDictionary *> * packageDicts = [NSMutableArray array];
		
		for (Package * p in self.packages) {
			[packageDicts addObject:[p prepareForDefaults]];
		}
		[dict setObject:packageDicts forKey:packagesAPIKey];
	}
	
	return dict;
}


- (PackageOrganization *)packageOrganization {
	PackageOrganization * packageOrganization = [[PackageOrganization alloc] initWithId:self.ID
																				  title:self.title
																	supportEmailAddress:self.supportEmailAddress
																			   isPublic:self.isPublic
																				created:self.created
																			   modified:self.modified
																				 prefix:self.prefix
																	defaultPrimaryColor:self.defaultPrimaryColor
																  defaultSecondaryColor:self.defaultSecondaryColor
																defaultVideoButtonTitle:self.defaultVideoButtonTitle
															defaultDocumentsButtonTitle:self.defaultDocumentsButtonTitle
															  defaultSupportButtonTitle:self.defaultSupportButtonTitle
															 defaultSupportEmailAddress:self.defaultSupportEmailAddress];
	return packageOrganization;
}
@end
