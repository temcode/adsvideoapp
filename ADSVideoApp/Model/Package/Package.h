//
//  Package.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/15/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PackageOrganization.h"

@interface Package : NSObject

@property (assign, nonatomic) NSInteger ID;
@property (strong, nonatomic) NSString * packageTitle;
@property (strong, nonatomic) NSString * urlString;
@property (strong, nonatomic) PackageOrganization * organization;
@property (assign, nonatomic) NSInteger orgID;
@property (strong, nonatomic) NSString * passcode;
@property (strong, nonatomic) NSString * appId;
@property (strong, nonatomic) NSString * created;
@property (strong, nonatomic) NSString * modified;

@property (strong, nonatomic) NSDictionary * matchingData;
@property (strong, nonatomic) NSDictionary * joinData;

- (instancetype)initWithId:(NSInteger)ID
			  packageTitle:(NSString *)packageTitle
				 urlString:(NSString *)urlString
					 orgID:(NSInteger)orgID
				  passcode:(NSString *)passcode
					 appId:(NSString *)appId
				   created:(NSString *)created
				  modified:(NSString *)modified
			  organization:(PackageOrganization *)organization;

- (instancetype)initWithDict:(NSDictionary *)dict;
- (instancetype)initWithDict:(NSDictionary *)dict packageOrganiztion:(PackageOrganization *)packOrg;
- (NSDictionary *)prepareForDefaults;

- (NSURL *)URL;
- (NSString *)idString;
- (NSString *)packageFolderPath;
- (NSString *)zipDownloadedFilePath;
- (NSString *)packageContentFolderPath;


@end
