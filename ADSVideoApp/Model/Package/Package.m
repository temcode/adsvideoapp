//
//  Package.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/15/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "Package.h"

@implementation Package

- (instancetype)initWithId:(NSInteger)ID
			  packageTitle:(NSString *)packageTitle
				 urlString:(NSString *)urlString
					 orgID:(NSInteger)orgID
				  passcode:(NSString *)passcode
					 appId:(NSString *)appId
				   created:(NSString *)created
				  modified:(NSString *)modified
			  organization:(PackageOrganization *)organization{
	self = [super init];
	if (self) {
		self.ID = ID;
		self.packageTitle = packageTitle;
		self.urlString = urlString;
		self.orgID = orgID;
		self.passcode = passcode;
		self.appId = appId;
		self.created = created;
		self.modified = modified;
		self.organization = organization;
	}
	return self;
}

- (instancetype)initWithDict:(NSDictionary *)dict {
	self = [super init];
	if (self) {
		self.ID = [dict containsValueForKey:idAPIKey] ? [dict[idAPIKey] integerValue] : -1;
		self.packageTitle = [dict containsValueForKey:titleAPIKey] ? dict[titleAPIKey] : nil;
		self.urlString = [dict containsValueForKey:urlAPIKey] ? dict[urlAPIKey] : nil;
		self.orgID = [dict containsValueForKey:orgIdAPIKey] ? [dict[orgIdAPIKey] integerValue] : -1;
		self.passcode = [dict containsValueForKey:passcodeAPIKey] ? dict[passcodeAPIKey] : nil;
		self.appId = [dict containsValueForKey:app_IdAPIKey] ? dict[app_IdAPIKey] : nil;
		
//		self.created = [dict containsValueForKey:createdAPIKey] ? [Utils getDateFromString:dict[createdAPIKey]] : nil;
//		self.modified = [dict containsValueForKey:modifiedAPIKey] ? [Utils getDateFromString:dict[modifiedAPIKey]] : nil;
		
		self.created = [dict containsValueForKey:createdAPIKey] ? dict[createdAPIKey] : nil ;
		self.modified = [dict containsValueForKey:modifiedAPIKey] ? dict[modifiedAPIKey] : nil ;
		
		self.matchingData = [dict containsValueForKey:matchingDataAPIKey] ? dict[matchingDataAPIKey] : nil;
		self.joinData = [dict containsValueForKey:joinDataAPIKey] ? dict[joinDataAPIKey] : nil;
		
		if (self.orgID == -1) {
			if (self.matchingData && [self.matchingData containsValueForKey:AppHasPackageHasOrgAPIKey]) {
				NSDictionary * appHasPackageHasOrg = self.matchingData[AppHasPackageHasOrgAPIKey];
				self.orgID = [appHasPackageHasOrg containsValueForKey:orgIdAPIKey] ? [appHasPackageHasOrg[orgIdAPIKey] integerValue] : -1;
			}
			if (self.orgID == -1 && self.joinData && [self.joinData containsValueForKey:orgIdAPIKey]) {
				self.orgID = [self.joinData[orgIdAPIKey] integerValue];
			}
		}
		
		self.organization = [dict containsValueForKey:@"packageOrganization"] ? [[PackageOrganization alloc] initWithDict:dict[@"packageOrganization"]] : nil;
	}
	return self;
}

- (instancetype)initWithDict:(NSDictionary *)dict packageOrganiztion:(PackageOrganization *)packOrg {
	self = [self initWithDict:dict];
	if (self) {
		self.organization = packOrg;
		self.orgID = packOrg.ID;
	}
	return self;
}

- (NSDictionary *)prepareForDefaults {
	NSMutableDictionary * dict = [NSMutableDictionary dictionary];
	
	[dict setObject:[NSString stringWithFormat:@"%ld", (long)self.ID] forKey:idAPIKey];
	
	if (self.packageTitle && self.packageTitle.length > 0) {
		[dict setObject:self.packageTitle forKey:titleAPIKey];
	}
	
	if (self.urlString && self.urlString.length > 0) {
		[dict setObject:self.urlString forKey:urlAPIKey];
	}
	
	if (self.orgID > 0) {
		[dict setObject:[NSString stringWithFormat:@"%ld", (long)self.orgID] forKey:orgIdAPIKey];
	}
	
	if (self.passcode) {
		[dict setObject:self.passcode forKey:passcodeAPIKey];
	}
	
	if (self.appId) {
		[dict setObject:self.appId forKey:app_IdAPIKey];
	}
	
	if (self.created) {
		[dict setObject:[NSString stringWithFormat:@"%@", self.created] forKey:createdAPIKey];
	}
	
	if (self.modified) {
		[dict setObject:[NSString stringWithFormat:@"%@", self.modified] forKey:modifiedAPIKey];
	}
	
	if (self.organization) {
		[dict setObject:[self.organization prepareForDefaults] forKey:@"packageOrganization"];
	}
	
	if (self.matchingData) {
		[dict setObject:self.matchingData forKey:matchingDataAPIKey];
	}
	
	if (self.joinData) {
		[dict setObject:self.joinData forKey:joinDataAPIKey];
	}
	
	return dict;
}

- (NSURL *)URL {
	return [NSURL URLWithString:[self.urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
}

- (NSString *)idString {
	return [NSString stringWithFormat:@"%ld", (long)self.ID];
}

- (NSNumber *)idNumber {
	return [NSNumber numberWithInteger:self.ID];
}

- (NSNumber *)orgIdNumber {
	return [NSNumber numberWithInteger:self.orgID];
}

- (NSString *)packageFolderPath {
	return [Utils packageFolderPathByPackageId:self.ID orgId:self.orgID];
}

- (NSString *)zipDownloadedFilePath {
	return [[self packageFolderPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_%@.zip", @(self.orgID), @(self.ID)]];
}

- (NSString *)packageContentFolderPath {
	return [[self packageFolderPath] stringByAppendingPathComponent:contentFolderName];
}


- (BOOL)isEqual:(id)object {
	if (object) {
		if ([object isKindOfClass:[Package class]]) {
			Package * comparingPackage = (Package *)object;
			return self.ID == comparingPackage.ID && self.orgID == comparingPackage.orgID;
		}
	}
	
	return NO;
}


@end
