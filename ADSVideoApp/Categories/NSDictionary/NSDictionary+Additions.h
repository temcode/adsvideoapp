//
//  NSDictionary+Additions.h
//  Sarzin
//
//  Created by Artem Selivanov on 2/12/17.
//  Copyright © 2017 Sarzin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Additions)

- (BOOL)containsValueForKey:(NSString *)key;

@end
