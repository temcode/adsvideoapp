//
//  NSDictionary+Additions.m
//  Sarzin
//
//  Created by Artem Selivanov on 2/12/17.
//  Copyright © 2017 Sarzin. All rights reserved.
//

#import "NSDictionary+Additions.h"

@implementation NSDictionary (Additions)

- (BOOL)containsValueForKey:(NSString *)key {
	return [self valueForKey:key] && [self valueForKey:key] != [NSNull null];
}

@end
