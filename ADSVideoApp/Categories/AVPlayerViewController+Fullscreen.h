//
//  AVPlayerViewController+Fullscreen.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/5/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <AVKit/AVKit.h>

@interface AVPlayerViewController (FullScreen)

- (void)goToFullscreen;
- (void)goFromFullscreen;

@end
