//
//  AVPlayerViewController+Fullscreen.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/5/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "AVPlayerViewController+Fullscreen.h"

@implementation AVPlayerViewController (FullScreen)

- (void)goToFullscreen {
	SEL fsSelector = NSSelectorFromString(@"_transitionToFullScreenViewControllerAnimated:completionHandler:");
	if ([self respondsToSelector:fsSelector]) {
		NSInvocation *inv = [NSInvocation invocationWithMethodSignature:[self methodSignatureForSelector:fsSelector]];
		[inv setSelector:fsSelector];
		[inv setTarget:self];
		BOOL animated = YES;
		id completionBlock = nil;
		[inv setArgument:&(animated) atIndex:2];
		[inv setArgument:&(completionBlock) atIndex:3];
		[inv invoke];
	}
}



- (void)goFromFullscreen {
	SEL fsSelector = NSSelectorFromString(@"_transitionFromFullScreenViewControllerAnimated:completionHandler:");
	if ([self respondsToSelector:fsSelector]) {
		NSInvocation *inv = [NSInvocation invocationWithMethodSignature:[self methodSignatureForSelector:fsSelector]];
		[inv setSelector:fsSelector];
		[inv setTarget:self];
		BOOL animated = YES;
		id completionBlock = nil;
		[inv setArgument:&(animated) atIndex:2];
		[inv setArgument:&(completionBlock) atIndex:3];
		[inv invoke];
	}
}

@end
