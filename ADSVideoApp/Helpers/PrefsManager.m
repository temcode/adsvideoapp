//
//  PrefsManager.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/17/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "PrefsManager.h"

#import "UIColor+Expanded.h"

#define defaults [NSUserDefaults standardUserDefaults]
#define manager PrefsManager


@implementation PrefsManager

+ (void)reset {
	
	NSSet * notResetable = [NSSet setWithArray:@[]];
	
	NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
	NSDictionary * dict = [defs dictionaryRepresentation];
	for (id key in dict) {
		if (![notResetable containsObject:key]) {
			[defs removeObjectForKey:key];
		}
	}
	[defs synchronize];
}


+ (void)setValue:(id)value forKey:(NSString *)key {
	[defaults setValue:value forKey:key];
	[defaults synchronize];
}

+ (void)setBool:(BOOL)boolean forKey:(NSString *)key {
	[defaults setBool:boolean forKey:key];
	[defaults synchronize];
}

+ (void)setString:(NSString *)str forKey:(NSString *)key {
	[defaults setObject:str forKey:key];
	[defaults synchronize];
}

+ (void)setInteger:(NSInteger)integer forKey:(NSString *)key {
	[defaults setInteger:integer forKey:key];
	[defaults synchronize];
}

+ (void)setObject:(id)obj forKey:(NSString *)key {
	[defaults setObject:obj forKey:key];
	[defaults synchronize];
}

+ (void)removeObjectForKey:(NSString *)key {
	[defaults removeObjectForKey:key];
	[defaults synchronize];
}


#pragma mark - Organizations Of Loaded Packages -

NSString * const kOrganizations = @"Organizations";


+ (void)setOrganizations:(NSArray <PackageOrganization *> *)organizations {
	NSMutableArray <NSDictionary *> * orgsDicts = [NSMutableArray array];
	
	for (PackageOrganization * packOrg in organizations) {
		[orgsDicts addObject:[packOrg prepareForDefaults]];
	}
	
	[manager setObject:orgsDicts forKey:kOrganizations];
}

+ (NSArray <PackageOrganization *> *)organizations {
	NSArray * arr = [defaults arrayForKey:kOrganizations];
	NSMutableArray <PackageOrganization *> * packOrgs = [NSMutableArray array];
	
	for (NSDictionary * packOrgDict in arr) {
		[packOrgs addObject:[[PackageOrganization alloc] initWithDict:packOrgDict]];
	}
	
	return packOrgs;
}

+ (void)addOrganization:(PackageOrganization *)packageOrganization {

	NSMutableArray <PackageOrganization *> * packOrgs = [NSMutableArray arrayWithArray:[manager organizations]];

	NSInteger replaceIndex = NSNotFound;
	for (PackageOrganization * packOrg in packOrgs) {
		if (packOrg.ID == packageOrganization.ID) {
			replaceIndex = [packOrgs indexOfObject:packOrg];
			break;
		}
	}
	
	if (replaceIndex == NSNotFound) {
		[packOrgs addObject:packageOrganization];
	} else {
		[packOrgs replaceObjectAtIndex:replaceIndex withObject:packageOrganization];
	}
	
	[manager setOrganizations:packOrgs];
}

+ (void)removeOrganizationById:(NSInteger)orgId {
	NSMutableArray <PackageOrganization *> * packOrgs = [NSMutableArray arrayWithArray:[manager organizations]];
	
	NSInteger removeIndex = NSNotFound;
	for (PackageOrganization * packOrg in packOrgs) {
		if (packOrg.ID == orgId) {
			removeIndex = [packOrgs indexOfObject:packOrg];
			break;
		}
	}
	
	if (removeIndex != NSNotFound) {
		[packOrgs removeObjectAtIndex:removeIndex];
	}
	
	[manager setOrganizations:packOrgs];
}

+ (PackageOrganization *)packageOrganizationById:(NSInteger)orgId {
	NSMutableArray <PackageOrganization *> * packOrgs = [NSMutableArray arrayWithArray:[manager organizations]];
	
	NSInteger getIndex = NSNotFound;
	for (PackageOrganization * packOrg in packOrgs) {
		if (packOrg.ID == orgId) {
			getIndex = [packOrgs indexOfObject:packOrg];
			break;
		}
	}
	
	if (getIndex != NSNotFound) {
		return packOrgs[getIndex];
	}
	
	return nil;
}

+ (BOOL)isPackagesLoadedForOrgWithId:(NSInteger)orgId {
	NSArray <Package *> * loadedPackages = [manager loadedPackages];
	
	NSInteger getIndex = NSNotFound;
	for (Package * package in loadedPackages) {
		if (package.orgID == orgId) {
			getIndex = [loadedPackages indexOfObject:package];
			break;
		}
	}
	
	return getIndex != NSNotFound;
}

#pragma mark - Loaded Packages -

NSString * const kLoadedPackages = @"LoadedPackages";

+ (void)setLoadedPackages:(NSArray <Package *> *)packages {
	
	if (packages && packages.count > 0) {
		NSMutableArray <NSDictionary *> * packageDicts = [NSMutableArray array];
		
		for (Package * p in packages) {
			[packageDicts addObject:[p prepareForDefaults]];
		}
		
		[manager setObject:packageDicts forKey:kLoadedPackages];
	} else {
		[manager removeObjectForKey:kLoadedPackages];
	}
}

+ (NSArray <Package *> *)loadedPackages {
	NSArray <NSDictionary *> * packageDicts = [defaults arrayForKey:kLoadedPackages];
	if (packageDicts && packageDicts.count > 0) {
		NSMutableArray <Package *> * packages = [NSMutableArray array];
		for (NSDictionary * pDict in packageDicts) {
			[packages addObject:[[Package alloc] initWithDict:pDict]];
		}
		return packages;
	}
	return nil;
}

+ (void)addPackageToLoaded:(Package *)package {
	NSMutableArray <Package *> * packages = [[PrefsManager loadedPackages] mutableCopy];
	if (!packages) {
		packages = [NSMutableArray array];
	}
	
	[packages addObject:package];
	[PrefsManager setLoadedPackages:packages];
}

+ (void)removePackageFromLoaded:(Package *)package {
	NSMutableArray <Package *> * packages = [[PrefsManager loadedPackages] mutableCopy];
	NSInteger removeIndex = [packages indexOfObjectPassingTest:^BOOL(Package * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
		return [obj isEqual:package];
	}];
	
	if (removeIndex != NSNotFound) {
		[packages removeObjectAtIndex:removeIndex];
	}
	
	[PrefsManager setLoadedPackages:packages];
}



#pragma mark - First Install Launch -

NSString * const kFirstInstallLaunch = @"FirstInstallLaunch";

+ (BOOL)isFirstInstallLaunch {
	return [defaults boolForKey:kFirstInstallLaunch];
}

+ (void)setIsFirstLaunch:(BOOL)isFirstlaunch {
	[defaults setBool:isFirstlaunch forKey:kFirstInstallLaunch];
}

#pragma mark - Was Already Launched -

NSString * const kWasAlreadyLaunched = @"WasAlreadyLaunched";

+ (BOOL)wasAlreadyLaunched {
	return [defaults boolForKey:kWasAlreadyLaunched];
}

+ (void)setWasAlreadyLaunched {
	[manager setBool:YES forKey:kWasAlreadyLaunched];
}

#pragma mark - Colors -

#pragma mark * Primary Color

NSString * const kPrimaryColor = @"kPrimaryColor";

+ (void)setPrimaryColor:(UIColor *)primaryColor {
	[manager setObject:[primaryColor stringValue] forKey:kPrimaryColor];
}
+ (UIColor *)getPrimaryColor {
	return [UIColor colorWithString:[defaults stringForKey:kPrimaryColor]];
}

#pragma mark * Secondary Color

NSString * const kSecondaryColor = @"kSecondaryColor";

+ (void)setSecondaryColor:(UIColor *)secondary {
	[manager setObject:[secondary stringValue] forKey:kSecondaryColor];
}
+ (UIColor *)getSecondaryColor {
	return [UIColor colorWithString:[defaults stringForKey:kSecondaryColor]];
}

#pragma mark * Primary Dark Color

NSString * const kPrimaryDarkColor = @"kPrimaryDarkColor";

+ (void)setPrimaryDarkColor:(UIColor *)primaryDarkColor {
	[manager setObject:[primaryDarkColor stringValue] forKey:kPrimaryDarkColor];
}

+ (UIColor *)getPrimaryDarkColor {
	return [UIColor colorWithString:[defaults stringForKey:kPrimaryDarkColor]];
}
#pragma mark * Accent Color

NSString * const kAccentColor = @"kAccentColor";

+ (void)setAccentColor:(UIColor *)accentColor {
	[manager setObject:[accentColor stringValue] forKey:kAccentColor];
}

+ (UIColor *)getAccentColor {
	return [UIColor colorWithString:[defaults stringForKey:kAccentColor]];
}

#pragma mark * Package Bg Color

NSString * const kPackageBgColor = @"kPackageBgColor";

+ (void)setPackageBgColor:(UIColor *)packageBgColor {
	[manager setObject:[packageBgColor stringValue] forKey:kPackageBgColor];
}

+ (UIColor *)getPackageBgColor {
	return [UIColor colorWithString:[defaults stringForKey:kPackageBgColor]];
}

#pragma mark * Package Buttons Color

NSString * const kPackageButtonsColor = @"kPackageButtonsColor";

+ (void)setPackageButtonsColor:(UIColor *)packageButtons {
	[manager setObject:[packageButtons stringValue] forKey:kPackageButtonsColor];
}

+ (UIColor *)getPackageButtonsColor {
	return [UIColor colorWithString:[defaults stringForKey:kPackageButtonsColor]];
}

@end
