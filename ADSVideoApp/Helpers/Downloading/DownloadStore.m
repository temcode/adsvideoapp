/*
 * Project: HWIFileDownload
 
 * File: HWIFileDownloader.m
 *
 */

/***************************************************************************
 
 Copyright (c) 2014-2016 Heiko Wichmann
 
 https://github.com/Heikowi/HWIFileDownload
 
 This software is provided 'as-is', without any expressed or implied warranty.
 In no event will the authors be held liable for any damages
 arising from the use of this software.
 
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 
 1. The origin of this software must not be misrepresented;
 you must not claim that you wrote the original software.
 If you use this software in a product, an acknowledgment
 in the product documentation would be appreciated
 but is not required.
 
 2. Altered source versions must be plainly marked as such,
 and must not be misrepresented as being the original software.
 
 3. This notice may not be removed or altered from any source distribution.
 
 ***************************************************************************/


/*
   DownloadStore.m
   ADSVideoApp
 
   Modified by Artem Selivanov on 3/4/17.
   Copyright © 2017 ArtSelDev. All rights reserved.
 */

#import "DownloadStore.h"
#import "DownloadItem.h"
#import "DownloadNotifications.h"
#import <HWIFileDownload/HWIFileDownloader.h>
#import "AppDelegate.h"
#import <UIKit/UIKit.h>
#import "Package.h"
#import <SSZipArchive/ZipArchive.h>


typedef void (^UnzippingCompletionBlock)(CompletionStatus status, NSString * unzippedContentPath, NSError * _Nullable error);
typedef void (^MoveFileCompletionBlock)(CompletionStatus status, NSString * resultFileLocation, NSError * _Nullable error);


static void *DownloadStoreProgressObserverContext = &DownloadStoreProgressObserverContext;

@interface DownloadStore()
@property (nonatomic, assign) NSUInteger networkActivityIndicatorCount;
@property (atomic, strong, readwrite, nonnull) NSMutableArray<DownloadItem *> *downloadItemsArray;
@property (nonatomic, strong, nonnull) NSProgress *progress;
@end



@implementation DownloadStore


- (nullable DownloadStore *)init
{
    self = [super init];
    if (self)
    {
        self.networkActivityIndicatorCount = 0;
        
        self.progress = [NSProgress progressWithTotalUnitCount:0];
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
        {
            [self.progress addObserver:self
                            forKeyPath:NSStringFromSelector(@selector(fractionCompleted))
                               options:NSKeyValueObservingOptionInitial
                               context:DownloadStoreProgressObserverContext];
        }
        
		[self setupDownloadItems:nil];
    }
    return self;
}

- (NSString *)downloadIdentifierWithPackage:(Package *)pack {
	return [NSString stringWithFormat:@"Package_%@_%@", @(pack.orgID), @(pack.ID)];
}

- (NSInteger)packageIdWithDownloadIdentifier:(NSString *)downloadIdentifier {
	NSArray <NSString *> * ids = [downloadIdentifier componentsSeparatedByString:@"_"];
	return [ids.lastObject integerValue];
}

- (NSInteger)packageOrgIdWithDownloadIdentifier:(NSString *)downloadIdentifier {
	NSArray <NSString *> * ids = [downloadIdentifier componentsSeparatedByString:@"_"];
	return [ids.firstObject integerValue];
}

- (void)setupDownloadItems:(NSArray <Package *> *)packagesToLoad {
	
	if (packagesToLoad) {
		
		for (DownloadItem * item in self.downloadItemsArray) {
				[self cancelDownloadWithDownloadIdentifier:item.downloadIdentifier];
		}
		
		for (Package * pack in packagesToLoad) {
			NSURL *aRemoteURL = [pack URL];
			NSString *aDownloadIdentifier = [self downloadIdentifierWithPackage:pack];
			
			DownloadItem *aDownloadItem = [[DownloadItem alloc] initWithDownloadIdentifier:aDownloadIdentifier
																				 remoteURL:aRemoteURL
																				   package:pack];
			
			[Utils deleteFolderOfPackage:pack error:nil];
			[self.downloadItemsArray addObject:aDownloadItem];
		}
		
		[self storeDownloadItems];
	} else {
		
		self.downloadItemsArray = [self restoredDownloadItems];
		NSMutableArray <DownloadItem *> * itemsToRestore = [NSMutableArray array];
		NSArray <Package *> * loadedPackages = [PrefsManager loadedPackages];
		
		for (DownloadItem * downloadItem in self.downloadItemsArray)
		{
			Package * pack = downloadItem.package;
			NSInteger downloadItemPackOrgId = pack.orgID;
			NSInteger downloadItemPackId = pack.ID;
			
			BOOL restoreCurrentItem = YES;
			for (Package * pack in loadedPackages) {
				if (pack.orgID == downloadItemPackOrgId && pack.ID == downloadItemPackId) {
					restoreCurrentItem = NO;
					break;
				}
			}
			
			if (restoreCurrentItem) {
				if (downloadItem.status != DownloadItemStatusCancelled &&
					downloadItem.status != DownloadItemStatusErrorMoved &&
					downloadItem.status != DownloadItemStatusErrorUnzip) {
					if (downloadItem.status == DownloadItemStatusStarted)
					{
						AppDelegate *theAppDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
						BOOL isDownloading = [theAppDelegate.fileDownloader isDownloadingIdentifier:downloadItem.downloadIdentifier];
						if (isDownloading == NO)
						{
							downloadItem.status = DownloadItemStatusInterrupted;
						}
					}
					
					[itemsToRestore addObject:downloadItem];
				} else if (downloadItem.status == DownloadItemStatusCancelled ||
						   downloadItem.status != DownloadItemStatusErrorMoved ||
						   downloadItem.status != DownloadItemStatusErrorUnzip) {
					[Utils deletePackageZip:downloadItem.package];
					[Utils deleteFolderOfPackage:downloadItem.package error:nil];
				}
			}
		}
		
		self.downloadItemsArray = itemsToRestore;
		[self storeDownloadItems];
	}
}


- (void)dealloc
{
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
    {
        [self.progress removeObserver:self
                           forKeyPath:NSStringFromSelector(@selector(fractionCompleted))
                              context:DownloadStoreProgressObserverContext];
    }
}

#pragma mark - 

- (void)moveAndUzipDownloadItem:(DownloadItem *)aCompletedDownloadItem loadedFile:(NSString *)aLocalFilePath {
	
	aCompletedDownloadItem.status = DownloadItemStatusMoved;
	[self storeDownloadItems];
	
	aCompletedDownloadItem.unzipProgress = @{@"localizedDescription" : localized(@"PrepareToExtract")};
	performBlockOnMainThread(^{
		[[NSNotificationCenter defaultCenter] postNotificationName:unzipProgressChangedNotification object:aCompletedDownloadItem];

	});
	
	
	WELF_MACRO
	[welf moveLoadedFileFrom:aLocalFilePath
				downloadItem:aCompletedDownloadItem
				  forPackage:aCompletedDownloadItem.package
			  withCompletion:^(CompletionStatus status, NSString *resultFileLocation, NSError * _Nullable error) {
		
		if (status == CompletionStatusSucces) {
			aCompletedDownloadItem.status = DownloadItemStatusZipped;
			[welf storeDownloadItems];
			
			[welf unzip:resultFileLocation
					 to:[aCompletedDownloadItem.package packageContentFolderPath]
				package:aCompletedDownloadItem.package
		   downloadItem:aCompletedDownloadItem
		 withCompletion:^(CompletionStatus status, NSString *unzippedContentPath, NSError * _Nullable error) {
			 
			 if (status == CompletionStatusSucces) {
				 
				 [PrefsManager addPackageToLoaded:aCompletedDownloadItem.package];
				 
				 aCompletedDownloadItem.status = DownloadItemStatusUnzipped;
				 [welf storeDownloadItems];
				 
				 performBlockOnMainThread(^{
					 [[NSNotificationCenter defaultCenter] postNotificationName:downloadDidCompleteNotification
																		 object:aCompletedDownloadItem];
				 });
				 
			 } else {
				 NSLog(@"Unzip Error - %@", [error localizedDescription]);
				 
				 [Utils deleteFolderOfPackage:aCompletedDownloadItem.package error:nil];
				 
				 aCompletedDownloadItem.status = DownloadItemStatusErrorUnzip;
				 aCompletedDownloadItem.downloadError = error;
				 [welf storeDownloadItems];
				 
				 aCompletedDownloadItem.unzipProgress = @{@"localizedDescription" : localized(@"ExtractionFailed")};
				 
				 performBlockOnMainThread(^{
					 [[NSNotificationCenter defaultCenter] postNotificationName:unzipProgressChangedNotification object:aCompletedDownloadItem];
					 [[NSNotificationCenter defaultCenter] postNotificationName:downloadDidCompleteNotification object:aCompletedDownloadItem];
				 });
			 }
			 [welf notifyUserIfComplete];
		 }];
		} else {
			
			 NSLog(@"Move Error - %@", [error localizedDescription]);
			
			[Utils deleteFolderOfPackage:aCompletedDownloadItem.package error:nil];
			
			if (aLocalFilePath) {
				if ([[NSFileManager defaultManager] fileExistsAtPath:aLocalFilePath]) {
					[[NSFileManager defaultManager] removeItemAtPath:aLocalFilePath error:nil];
				}
			}
			
			aCompletedDownloadItem.status = DownloadItemStatusErrorMoved;
			aCompletedDownloadItem.downloadError = error;
			[welf storeDownloadItems];
			
			aCompletedDownloadItem.unzipProgress = @{@"localizedDescription" : localized(@"ExtractionFailed")};
			
			performBlockOnMainThread(^{
				[[NSNotificationCenter defaultCenter] postNotificationName:unzipProgressChangedNotification object:aCompletedDownloadItem];
				[[NSNotificationCenter defaultCenter] postNotificationName:downloadDidCompleteNotification object:aCompletedDownloadItem];
			});
			
			[welf notifyUserIfComplete];
		}
	}];
}

- (void)trackFileProgressFrom:(NSString *)fromPath
						   to:(NSString *)toPath
		   downloadIdentifier:(NSString *)downloadIdentifier
					copyQueue:(dispatch_queue_t)copyQueue
					keepTrack:(BOOL)keepTrack {

	if ([[NSFileManager defaultManager] fileExistsAtPath:fromPath]) {
		
		NSUInteger aFoundDownloadItemIndex = [appDelegate.downloadStore.downloadItemsArray indexOfObjectPassingTest:^BOOL(DownloadItem *aDownloadItem, NSUInteger anIndex, BOOL *aStopFlag) {
			if ([aDownloadItem.downloadIdentifier isEqualToString:downloadIdentifier])
			{
				return YES;
			}
			return NO;
		}];
		
		if (aFoundDownloadItemIndex != NSNotFound) {
			DownloadItem * downloadItem = appDelegate.downloadStore.downloadItemsArray[aFoundDownloadItemIndex];
			
			if (downloadItem.status != DownloadItemStatusCancelled &&
				downloadItem.status != DownloadItemStatusZipped &&
				downloadItem.status != DownloadItemStatusErrorMoved &&
				downloadItem.status != DownloadItemStatusErrorUnzip &&
				downloadItem.status != DownloadItemStatusError) {
				NSDictionary *attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:fromPath error:NULL];
				unsigned long long fromFileSize = [attributes fileSize];
				
				unsigned long long toFileSize = 0;
				if ([[NSFileManager defaultManager] fileExistsAtPath:toPath]) {
					NSDictionary *attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:toPath error:NULL];
					toFileSize = [attributes fileSize];
				}
				
				if (toFileSize != fromFileSize) {
					performBlockOnMainThread(^{
						
						CGFloat toFileSizeInMB = toFileSize / (1024 * 1024);
						CGFloat fromFileSizeInMB = fromFileSize / (1024 * 1024);
							
							NSString * localizedDescription = localized(@"Copying");
							NSString * localizedAdditionalDescription = localizedWithFormat(@"{CopiedMBCount}Of{TotalMBCount}", toFileSizeInMB, fromFileSizeInMB);
							
							downloadItem.unzipProgress = @{@"localizedDescription": localizedDescription,
														   @"localizedAdditionalDescription" : localizedAdditionalDescription,
														   @"progress" : @((CGFloat)toFileSize / (CGFloat)fromFileSize)};
						
						
						[[NSNotificationCenter defaultCenter] postNotificationName:unzipProgressChangedNotification
																			object:downloadItem];
					});
				}
				
				if (keepTrack) {
					WELF_MACRO
					dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), copyQueue, ^{
						
						[welf trackFileProgressFrom:fromPath
												 to:toPath
								 downloadIdentifier:downloadIdentifier
										  copyQueue:copyQueue
										  keepTrack:fromFileSize - toFileSize > 1024 * 1024];
					});
				}
			}
		}
	}
}


- (void)unzip:(NSString *)zipPath
		   to:(NSString *)packageContentFolderPath
	  package:(Package *)package
 downloadItem:(DownloadItem *)downloadItem
withCompletion:(UnzippingCompletionBlock)completion {
	
	NSError * error = nil;
	BOOL isDirecroty = NO;
	BOOL success = NO;
	
	if (![[NSFileManager defaultManager] fileExistsAtPath:packageContentFolderPath isDirectory:&isDirecroty]) {
		success = [Utils createFolderAtPath:packageContentFolderPath error:&error];
	} else {
		success = YES;
		if (!isDirecroty) {
			[[NSFileManager defaultManager] removeItemAtPath:packageContentFolderPath error:nil];
			success = [Utils createFolderAtPath:packageContentFolderPath error:&error];
		}
	}
	
	if (success) {
		 NSLog(@"Start Unzip - %@\nFrom path: \n%@\nTo path:\n%@",downloadItem.package.packageTitle, zipPath, packageContentFolderPath);
		
		[SSZipArchive unzipFileAtPath:zipPath
						toDestination:packageContentFolderPath
					  progressHandler:^(NSString * _Nonnull entry, unz_file_info zipInfo, long entryNumber, long total) {
						  if (appDelegate.appIsActive && [UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
							  NSString * localizedDescription = localized(@"Extracting");
							  NSString * localizedAdditionalDescription = entry ? : @"Unzip";
							  
							  performBlockOnMainThread(^{
								  downloadItem.unzipProgress = @{@"localizedDescription": localizedDescription,
																 @"localizedAdditionalDescription" : localizedAdditionalDescription,
																 @"progress" : @((CGFloat)entryNumber / (CGFloat)total)};
								  
								  NSLog(@"%@",downloadItem.unzipProgress);
								  
								  [[NSNotificationCenter defaultCenter] postNotificationName:unzipProgressChangedNotification object:downloadItem];
							  });
						  }
					  }
					completionHandler:^(NSString * _Nonnull path, BOOL succeeded, NSError * _Nonnull error) {
						[Utils deletePackageZip:package];
						if (succeeded) {
							if (completion) {
								completion(CompletionStatusSucces, nil, nil);
							}
						} else {
							if (completion) {
								completion(CompletionStatusError, nil, error);
							}
						}
					}];
	} else {
		
		 NSLog(@"Unzip FAILED -> content folder for package %@ is absent at path - %@",downloadItem.package.packageTitle, packageContentFolderPath);
		
		if (completion) {
			completion(CompletionStatusError, nil, error);
		}
	}
}

- (void)moveLoadedFileFrom:(NSString *)fromPath
			  downloadItem:(DownloadItem *)item
				forPackage:(Package *)package
			withCompletion:(MoveFileCompletionBlock)completion {

	
	if (fromPath && fromPath.length > 0) {
		__block NSError * error = nil;
		BOOL isDirecroty = NO;
		BOOL success = NO;
		
		if (![[NSFileManager defaultManager] fileExistsAtPath:[package packageFolderPath] isDirectory:&isDirecroty]) {
			success = [Utils createFolderAtPath:[package packageFolderPath] error:&error];
		} else {
			success = YES;
			if (!isDirecroty) {
				[[NSFileManager defaultManager] removeItemAtPath:[package packageFolderPath] error:nil];
				success = [Utils createFolderAtPath:[package packageFolderPath] error:&error];
			}
		}
		
		if (success) {
			if([[NSFileManager defaultManager] fileExistsAtPath:fromPath]) {
				error = nil;
				NSString * toPath = [package zipDownloadedFilePath];
				
				if (appDelegate.appIsActive && [UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
					
					NSString * labelString = [NSString stringWithFormat:@"%@.copyQueue.%ld.%ld", [NSBundle mainBundle].bundleIdentifier, (long)package.ID, (long)package.orgID];
					const char *label = [labelString cStringUsingEncoding:[NSString defaultCStringEncoding]];
					dispatch_queue_t copyQueue = dispatch_queue_create(label, 0);
					
					WELF_MACRO
					dispatch_async(copyQueue, ^{
						[welf trackFileProgressFrom:fromPath
												 to:toPath
								 downloadIdentifier:item.downloadIdentifier
										  copyQueue:copyQueue
										  keepTrack:YES];
					});
					
					BOOL success = [[NSFileManager defaultManager] copyItemAtPath:fromPath toPath:toPath error:&error];
						if(success)
						{
							if (completion) {
								completion(CompletionStatusSucces, [package zipDownloadedFilePath], nil);
							}
							
						} else {
							if (completion) {
								completion(CompletionStatusError, nil, error);
							}
						}
						
						if ([[NSFileManager defaultManager] fileExistsAtPath:fromPath]) {
							[[NSFileManager defaultManager] removeItemAtPath:fromPath error:nil];
						}
				} else {
					performBlockOnMainThread(^{
						BOOL success = [[NSFileManager defaultManager] copyItemAtPath:fromPath toPath:toPath error:&error];
						if(success)
						{
							if (completion) {
								completion(CompletionStatusSucces, [package zipDownloadedFilePath], nil);
							}
							
						} else {
							if (completion) {
								completion(CompletionStatusError, nil, error);
							}
						}
						
						if ([[NSFileManager defaultManager] fileExistsAtPath:fromPath]) {
							[[NSFileManager defaultManager] removeItemAtPath:fromPath error:nil];
						}
					});
				}

			} else {
				if (completion) {
					
					NSString * s = localizedWithFormat(@"ErorrFileCantBeMoved{Path}", fromPath);
					NSError * error = [Utils errorWithLocalizedDescription:s code:1234 domainClass:[self class]];
					completion(CompletionStatusError, nil, error);
				}
			}
		} else {
			if (completion) {
				completion(CompletionStatusError, nil, error);
			}
		}
	} else {
		
		if (completion) {
			NSString * s = localized(@"ErrorFilePathIsEmpty");
			NSError * error = [Utils errorWithLocalizedDescription:s code:1234 domainClass:[self class]];
			
			completion(CompletionStatusError, nil, error);
		}
	}
}


- (void)notifyUser:(NSString *)message title:(NSString *)title {

	
	UILocalNotification * local = [[UILocalNotification alloc] init];
	local.alertTitle = title;
	local.alertBody = message;
	
	[[UIApplication sharedApplication] presentLocalNotificationNow:local];
}

- (void)notifyUserIfComplete {
	BOOL finished = YES;
	for (DownloadItem * item in self.downloadItemsArray) {
		if (item.status != DownloadItemStatusCancelled &&
			item.status != DownloadItemStatusUnzipped &&
			item.status != DownloadItemStatusError &&
			item.status != DownloadItemStatusErrorMoved &&
			item.status != DownloadItemStatusErrorUnzip) {
			
			finished = NO;
			break;
		}
	}
	
	if (finished && !appDelegate.appIsActive && [UIApplication sharedApplication].applicationState != UIApplicationStateActive) {
		[self notifyUser:localized(@"DownloadingFinished")
				   title:nil];
		[UIApplication sharedApplication].applicationIconBadgeNumber = 1;
	}
}


#pragma mark - HWIFileDownloadDelegate (mandatory)


- (void)downloadDidCompleteWithIdentifier:(nonnull NSString *)aDownloadIdentifier
                             localFileURL:(nonnull NSURL *)aLocalFileURL
{
	NSUInteger aFoundDownloadItemIndex = [self.downloadItemsArray indexOfObjectPassingTest:^BOOL(DownloadItem *aDownloadItem, NSUInteger anIndex, BOOL *aStopFlag) {
		if ([aDownloadItem.downloadIdentifier isEqualToString:aDownloadIdentifier])
		{
			return YES;
		}
		return NO;
	}];
	
	DownloadItem *aCompletedDownloadItem = nil;
	if (aFoundDownloadItemIndex != NSNotFound)
	{
		
		NSLog(@"INFO: Download completed (id: %@) (%@, %d)", aDownloadIdentifier, [NSString stringWithUTF8String:__FILE__].lastPathComponent, __LINE__);
		
		aCompletedDownloadItem = [self.downloadItemsArray objectAtIndex:aFoundDownloadItemIndex];
		
		aCompletedDownloadItem.status = DownloadItemStatusCompleted;
		[self storeDownloadItems];
		performBlockOnMainThread(^{
			[[NSNotificationCenter defaultCenter] postNotificationName:downloadDidCompleteNotification object:aCompletedDownloadItem];
		});
		
		WELF_MACRO
		if (appDelegate.appIsActive && [UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
			dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
				[welf moveAndUzipDownloadItem:aCompletedDownloadItem loadedFile:[aLocalFileURL path]];
			});
		} else {
			performBlockOnMainThread(^{
				[welf moveAndUzipDownloadItem:aCompletedDownloadItem loadedFile:[aLocalFileURL path]];
			});
		}
	}
	else
	{
		NSLog(@"ERR: Completed download item not found (id: %@) (%@, %d)", aDownloadIdentifier, [NSString stringWithUTF8String:__FILE__].lastPathComponent, __LINE__);
		
		[[NSNotificationCenter defaultCenter] postNotificationName:downloadDidCompleteNotification object:aCompletedDownloadItem];
	}
}

- (void)downloadFailedWithIdentifier:(nonnull NSString *)aDownloadIdentifier
                               error:(nonnull NSError *)anError
                      httpStatusCode:(NSInteger)aHttpStatusCode
                  errorMessagesStack:(nullable NSArray<NSString *> *)anErrorMessagesStack
                          resumeData:(nullable NSData *)aResumeData
{
    NSUInteger aFoundDownloadItemIndex = [self.downloadItemsArray indexOfObjectPassingTest:^BOOL(DownloadItem *aDownloadItem, NSUInteger anIndex, BOOL *aStopFlag) {
        if ([aDownloadItem.downloadIdentifier isEqualToString:aDownloadIdentifier])
        {
            return YES;
        }
        return NO;
    }];
    DownloadItem *aFailedDownloadItem = nil;
    if (aFoundDownloadItemIndex != NSNotFound)
    {
        aFailedDownloadItem = [self.downloadItemsArray objectAtIndex:aFoundDownloadItemIndex];
        aFailedDownloadItem.lastHttpStatusCode = aHttpStatusCode;
        aFailedDownloadItem.resumeData = aResumeData;
        aFailedDownloadItem.downloadError = anError;
        aFailedDownloadItem.downloadErrorMessagesStack = anErrorMessagesStack;
        
        // download status heuristics
        if (aFailedDownloadItem.status != DownloadItemStatusPaused)
        {
            if (aResumeData.length > 0)
            {
                aFailedDownloadItem.status = DownloadItemStatusInterrupted;
            }
            else if ([anError.domain isEqualToString:NSURLErrorDomain] && (anError.code == NSURLErrorCancelled))
            {
                aFailedDownloadItem.status = DownloadItemStatusCancelled;
            }
            else
            {
                aFailedDownloadItem.status = DownloadItemStatusError;
            }
        }
        [self storeDownloadItems];
        
        switch (aFailedDownloadItem.status) {
            case DownloadItemStatusError:
                NSLog(@"ERR: Download with error %@ (http status: %@) - id: %@ (%@, %d)", @(anError.code), @(aHttpStatusCode), aDownloadIdentifier, [NSString stringWithUTF8String:__FILE__].lastPathComponent, __LINE__);
                break;
            case DownloadItemStatusInterrupted:
                NSLog(@"ERR: Download interrupted with error %@ - id: %@ (%@, %d)", @(anError.code), aDownloadIdentifier, [NSString stringWithUTF8String:__FILE__].lastPathComponent, __LINE__);
                break;
            case DownloadItemStatusCancelled:
                NSLog(@"INFO: Download cancelled - id: %@ (%@, %d)", aDownloadIdentifier, [NSString stringWithUTF8String:__FILE__].lastPathComponent, __LINE__);
                break;
            case DownloadItemStatusPaused:
                NSLog(@"INFO: Download paused - id: %@ (%@, %d)", aDownloadIdentifier, [NSString stringWithUTF8String:__FILE__].lastPathComponent, __LINE__);
                break;
                
            default:
                break;
        }
        
    }
    else
    {
        NSLog(@"ERR: Failed download item not found (id: %@) (%@, %d)", aDownloadIdentifier, [NSString stringWithUTF8String:__FILE__].lastPathComponent, __LINE__);
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:downloadDidCompleteNotification object:aFailedDownloadItem];
}


- (void)incrementNetworkActivityIndicatorActivityCount
{
    [self toggleNetworkActivityIndicatorVisible:YES];
}


- (void)decrementNetworkActivityIndicatorActivityCount
{
    [self toggleNetworkActivityIndicatorVisible:NO];
}


#pragma mark HWIFileDownloadDelegate (optional)


- (void)downloadProgressChangedForIdentifier:(nonnull NSString *)aDownloadIdentifier
{
    NSUInteger aFoundDownloadItemIndex = [self.downloadItemsArray indexOfObjectPassingTest:^BOOL(DownloadItem *aDownloadItem, NSUInteger anIndex, BOOL *aStopFlag) {
        if ([aDownloadItem.downloadIdentifier isEqualToString:aDownloadIdentifier])
        {
            return YES;
        }
        return NO;
    }];
    DownloadItem *aChangedDownloadItem = nil;
    if (aFoundDownloadItemIndex != NSNotFound)
    {
        aChangedDownloadItem = [self.downloadItemsArray objectAtIndex:aFoundDownloadItemIndex];
        AppDelegate *theAppDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        HWIFileDownloadProgress *aFileDownloadProgress = [theAppDelegate.fileDownloader downloadProgressForIdentifier:aDownloadIdentifier];
        if (aFileDownloadProgress)
        {
            aChangedDownloadItem.progress = aFileDownloadProgress;
            if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
            {
                aChangedDownloadItem.progress.lastLocalizedDescription = aChangedDownloadItem.progress.nativeProgress.localizedDescription;
                aChangedDownloadItem.progress.lastLocalizedAdditionalDescription = aChangedDownloadItem.progress.nativeProgress.localizedAdditionalDescription;
            }
        }
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:downloadProgressChangedNotification object:aChangedDownloadItem];
}


- (void)downloadPausedWithIdentifier:(nonnull NSString *)aDownloadIdentifier
                          resumeData:(nullable NSData *)aResumeData
{
    NSUInteger aFoundDownloadItemIndex = [self.downloadItemsArray indexOfObjectPassingTest:^BOOL(DownloadItem *aDownloadItem, NSUInteger anIndex, BOOL *aStopFlag) {
        if ([aDownloadItem.downloadIdentifier isEqualToString:aDownloadIdentifier])
        {
            return YES;
        }
        return NO;
    }];
    if (aFoundDownloadItemIndex != NSNotFound)
    {
        NSLog(@"INFO: Download paused - id: %@ (%@, %d)", aDownloadIdentifier, [NSString stringWithUTF8String:__FILE__].lastPathComponent, __LINE__);
        
        DownloadItem *aPausedDownloadItem = [self.downloadItemsArray objectAtIndex:aFoundDownloadItemIndex];
        aPausedDownloadItem.status = DownloadItemStatusPaused;
        aPausedDownloadItem.resumeData = aResumeData;
        [self storeDownloadItems];
    }
    else
    {
        NSLog(@"ERR: Paused download item not found (id: %@) (%@, %d)", aDownloadIdentifier, [NSString stringWithUTF8String:__FILE__].lastPathComponent, __LINE__);
    }
}


- (void)resumeDownloadWithIdentifier:(nonnull NSString *)aDownloadIdentifier
{
    NSUInteger aFoundDownloadItemIndex = [self.downloadItemsArray indexOfObjectPassingTest:^BOOL(DownloadItem *aDownloadItem, NSUInteger anIndex, BOOL *aStopFlag) {
        if ([aDownloadItem.downloadIdentifier isEqualToString:aDownloadIdentifier])
        {
            return YES;
        }
        return NO;
    }];
    if (aFoundDownloadItemIndex != NSNotFound)
    {
        DownloadItem *aDownloadItem = [self.downloadItemsArray objectAtIndex:aFoundDownloadItemIndex];
        [self startDownloadWithDownloadItem:aDownloadItem];
    }
}


- (BOOL)downloadAtLocalFileURL:(nonnull NSURL *)aLocalFileURL isValidForDownloadIdentifier:(nonnull NSString *)aDownloadIdentifier
{
    BOOL anIsValidFlag = YES;
    
    // just checking for file size
    // you might want to check by converting into expected data format (like UIImage) or by scanning for expected content
    
    NSError *anError = nil;
    NSDictionary <NSString *, id> *aFileAttributesDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:aLocalFileURL.path error:&anError];
    if (anError)
    {
        NSLog(@"ERR: Error on getting file size for item at %@: %@ (%@, %d)", aLocalFileURL, anError.localizedDescription, [NSString stringWithUTF8String:__FILE__].lastPathComponent, __LINE__);
        anIsValidFlag = NO;
    }
    else
    {
        unsigned long long aFileSize = [aFileAttributesDictionary fileSize];
        if (aFileSize == 0)
        {
            anIsValidFlag = NO;
        }
        else
        {
            if (aFileSize < 40000)
            {
//                NSError *anError = nil;
//                NSString *aString = [NSString stringWithContentsOfURL:aLocalFileURL encoding:NSUTF8StringEncoding error:&anError];
//                if (anError)
//                {
//                    NSLog(@"ERR: %@ (%@, %d)", anError.localizedDescription, [NSString stringWithUTF8String:__FILE__].lastPathComponent, __LINE__);
//                }
//                else
//                {
//                    NSLog(@"INFO: Downloaded file content for download identifier %@: %@ (%@, %d)", aDownloadIdentifier, aString, [NSString stringWithUTF8String:__FILE__].lastPathComponent, __LINE__);
//                }
//                anIsValidFlag = NO;
            }
        }
    }
    return anIsValidFlag;
}

/*
- (void)onAuthenticationChallenge:(nonnull NSURLAuthenticationChallenge *)aChallenge
               downloadIdentifier:(nonnull NSString *)aDownloadIdentifier
                completionHandler:(void (^ _Nonnull)(NSURLCredential * _Nullable aCredential, NSURLSessionAuthChallengeDisposition disposition))aCompletionHandler
{
    if (aChallenge.previousFailureCount == 0)
    {
        NSURLCredential *aCredential = [NSURLCredential credentialWithUser:@"username" password:@"password" persistence:NSURLCredentialPersistenceNone];
        aCompletionHandler(aCredential, NSURLSessionAuthChallengeUseCredential);
    }
    else
    {
        aCompletionHandler(nil, NSURLSessionAuthChallengeRejectProtectionSpace);
    }
}
*/

- (nullable NSProgress *)rootProgress
{
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
    {
        return self.progress;
    }
    else
    {
        return nil;
    }
}


- (void)customizeBackgroundSessionConfiguration:(nonnull NSURLSessionConfiguration *)aBackgroundSessionConfiguration
{
    NSMutableDictionary *aHTTPAdditionalHeadersDict = [aBackgroundSessionConfiguration.HTTPAdditionalHeaders mutableCopy];
    if (aHTTPAdditionalHeadersDict == nil) {
        aHTTPAdditionalHeadersDict = [[NSMutableDictionary alloc] init];
    }
    [aHTTPAdditionalHeadersDict setObject:@"identity" forKey:@"Accept-Encoding"];
    aBackgroundSessionConfiguration.HTTPAdditionalHeaders = aHTTPAdditionalHeadersDict;
	
	
	aBackgroundSessionConfiguration.sessionSendsLaunchEvents = YES;
	aBackgroundSessionConfiguration.discretionary = NO;
}


#pragma mark - NSProgress


- (void)observeValueForKeyPath:(nullable NSString *)aKeyPath
                      ofObject:(nullable id)anObject
                        change:(nullable NSDictionary<NSString*, id> *)aChange
                       context:(nullable void *)aContext
{
    if (aContext == DownloadStoreProgressObserverContext)
    {
        NSProgress *aProgress = anObject; // == self.progress
        if ([aKeyPath isEqualToString:@"fractionCompleted"])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:totalDownloadProgressChangedNotification object:aProgress];
        }
        else
        {
            NSLog(@"ERR: Invalid keyPath (%@, %d)", [NSString stringWithUTF8String:__FILE__].lastPathComponent, __LINE__);
        }
    }
    else
    {
        [super observeValueForKeyPath:aKeyPath
                             ofObject:anObject
                               change:aChange
                              context:aContext];
    }
}


- (void)resetProgressIfNoActiveDownloadsRunning
{
    AppDelegate *theAppDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    BOOL aHasActiveDownloadsFlag = [theAppDelegate.fileDownloader hasActiveDownloads];
    if (aHasActiveDownloadsFlag == NO)
    {
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
        {
            [self.progress removeObserver:self forKeyPath:NSStringFromSelector(@selector(fractionCompleted))];
        }
		
		self.progress = [NSProgress progressWithTotalUnitCount:0];
		
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
        {
            [self.progress addObserver:self
                            forKeyPath:NSStringFromSelector(@selector(fractionCompleted))
                               options:NSKeyValueObservingOptionInitial
                               context:DownloadStoreProgressObserverContext];
        }
    }
}


#pragma mark - Start Download


- (void)startDownloadWithDownloadItem:(nonnull DownloadItem *)aDownloadItem
{
    [self resetProgressIfNoActiveDownloadsRunning];
	
 
	if ((aDownloadItem.status != DownloadItemStatusCancelled) &&
		(aDownloadItem.status != DownloadItemStatusCompleted) &&
		(aDownloadItem.status != DownloadItemStatusMoved)  &&
		(aDownloadItem.status != DownloadItemStatusZipped)  &&
		(aDownloadItem.status != DownloadItemStatusUnzipped))
    {
        AppDelegate *theAppDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        BOOL isDownloading = [theAppDelegate.fileDownloader isDownloadingIdentifier:aDownloadItem.downloadIdentifier];
        if (isDownloading == NO)
        {
            aDownloadItem.status = DownloadItemStatusStarted;
            
            [self storeDownloadItems];
            
            // kick off individual download
            if (aDownloadItem.resumeData.length > 0)
            {
                [theAppDelegate.fileDownloader startDownloadWithIdentifier:aDownloadItem.downloadIdentifier usingResumeData:aDownloadItem.resumeData];
            }
            else
            {
				[Utils deleteFolderOfPackage:aDownloadItem.package error:nil];
                [theAppDelegate.fileDownloader startDownloadWithIdentifier:aDownloadItem.downloadIdentifier fromRemoteURL:aDownloadItem.remoteURL];
            }
		} else {
			if (aDownloadItem.status == DownloadItemStatusInterrupted) {
				aDownloadItem.status = DownloadItemStatusStarted;
				[self storeDownloadItems];
			}
		}
	} else if (aDownloadItem.status == DownloadItemStatusZipped) {
		if (appDelegate.appIsActive && [UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
			WELF_MACRO
			dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
				[welf unzipPackageFromDownloadItemOnRestart:aDownloadItem];
			});
		} else {
			[self unzipPackageFromDownloadItemOnRestart:aDownloadItem];
		}
	}
}

- (void)unzipPackageFromDownloadItemOnRestart:(DownloadItem *)aDownloadItem {
	
		[self unzip:[aDownloadItem.package zipDownloadedFilePath]
				 to:[aDownloadItem.package packageContentFolderPath]
			package:aDownloadItem.package
	   downloadItem:aDownloadItem
	 withCompletion:^(CompletionStatus status, NSString *unzippedContentPath, NSError * _Nullable error) {
		 
		 if (status == CompletionStatusSucces) {
			 
			 [PrefsManager addPackageToLoaded:aDownloadItem.package];
			 
			 aDownloadItem.status = DownloadItemStatusUnzipped;
			 [self storeDownloadItems];
			 
			 performBlockOnMainThread(^{
				 [[NSNotificationCenter defaultCenter] postNotificationName:downloadDidCompleteNotification
																	 object:aDownloadItem];
			 });
			 
		 } else {
			 NSLog(@"Unzip Error - %@", [error localizedDescription]);
			 
			 [Utils deleteFolderOfPackage:aDownloadItem.package error:nil];
			 
			 aDownloadItem.status = DownloadItemStatusErrorUnzip;
			 aDownloadItem.downloadError = error;
			 [self storeDownloadItems];
			 
			 aDownloadItem.unzipProgress = @{@"localizedDescription" : localized(@"ExtractionFailed")};
			 
			 performBlockOnMainThread(^{
				 [[NSNotificationCenter defaultCenter] postNotificationName:unzipProgressChangedNotification object:aDownloadItem];
				 [[NSNotificationCenter defaultCenter] postNotificationName:downloadDidCompleteNotification object:aDownloadItem];
			 });
		 }
		 [self notifyUserIfComplete];
	 }];
}

- (void)resumeDownloadWithDownloadIdentifier:(nonnull NSString *)aDownloadIdentifier
{
    [self resetProgressIfNoActiveDownloadsRunning];
	
    NSUInteger aFoundDownloadItemIndex = [self.downloadItemsArray indexOfObjectPassingTest:^BOOL(DownloadItem *aDownloadItem, NSUInteger anIndex, BOOL *aStopFlag) {
        if ([aDownloadItem.downloadIdentifier isEqualToString:aDownloadIdentifier])
        {
            return YES;
        }
        return NO;
    }];
    if (aFoundDownloadItemIndex != NSNotFound)
    {
        DownloadItem *aDownloadItem = [self.downloadItemsArray objectAtIndex:aFoundDownloadItemIndex];
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_8_4)
        {
            if (aDownloadItem.progress.nativeProgress)
            {
                [aDownloadItem.progress.nativeProgress resume];
            }
            else
            {
                [self startDownloadWithDownloadItem:aDownloadItem];
            }
        }
        else
        {
            [self startDownloadWithDownloadItem:aDownloadItem];
        }
    }
}


#pragma mark - Cancel Download


- (void)cancelDownloadWithDownloadIdentifier:(nonnull NSString *)aDownloadIdentifier
{
	
	BOOL isDownloading = [appDelegate.fileDownloader isDownloadingIdentifier:aDownloadIdentifier];
	if (isDownloading)
	{
			HWIFileDownloadProgress *aFileDownloadProgress = [appDelegate.fileDownloader downloadProgressForIdentifier:aDownloadIdentifier];
			[aFileDownloadProgress.nativeProgress cancel];
	}
	
    NSUInteger aFoundDownloadItemIndex = [self.downloadItemsArray indexOfObjectPassingTest:^BOOL(DownloadItem *aDownloadItem, NSUInteger anIndex, BOOL *aStopFlag) {
        if ([aDownloadItem.downloadIdentifier isEqualToString:aDownloadIdentifier])
        {
            return YES;
        }
        return NO;
    }];
	
    if (aFoundDownloadItemIndex != NSNotFound)
    {
		DownloadItem *aCancelledDownloadItem = [self.downloadItemsArray objectAtIndex:aFoundDownloadItemIndex];
		aCancelledDownloadItem.status = DownloadItemStatusCancelled;
		[self.downloadItemsArray removeObject:aCancelledDownloadItem];
		[Utils deletePackageZip:aCancelledDownloadItem.package];
        [self storeDownloadItems];
    }
    else
    {
        NSLog(@"ERR: Cancelled download item not found (id: %@) (%@, %d)", aDownloadIdentifier, [NSString stringWithUTF8String:__FILE__].lastPathComponent, __LINE__);
    }
}


#pragma mark - Network Activity Indicator


- (void)toggleNetworkActivityIndicatorVisible:(BOOL)visible
{
    visible ? self.networkActivityIndicatorCount++ : self.networkActivityIndicatorCount--;
    NSLog(@"INFO: NetworkActivityIndicatorCount: %@", @(self.networkActivityIndicatorCount));
    [UIApplication sharedApplication].networkActivityIndicatorVisible = (self.networkActivityIndicatorCount > 0);
}


#pragma mark - Persistence


- (void)storeDownloadItems
{
    NSMutableArray <NSData *> *aDownloadItemsArchiveArray = [NSMutableArray arrayWithCapacity:self.downloadItemsArray.count];
    for (DownloadItem *aDownloadItem in self.downloadItemsArray)
    {
        NSData *aDownloadItemEncoded = [NSKeyedArchiver archivedDataWithRootObject:aDownloadItem];
        [aDownloadItemsArchiveArray addObject:aDownloadItemEncoded];
    }
    NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
    [userData setObject:aDownloadItemsArchiveArray forKey:@"downloadItems"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (nonnull NSMutableArray<DownloadItem *> *)restoredDownloadItems
{
    NSMutableArray <DownloadItem *> *aRestoredMutableDownloadItemsArray = [NSMutableArray array];
    NSMutableArray <NSData  *> *aRestoredMutableDataItemsArray = [[[NSUserDefaults standardUserDefaults] objectForKey:@"downloadItems"] mutableCopy];
    if (aRestoredMutableDataItemsArray == nil)
    {
        aRestoredMutableDataItemsArray = [NSMutableArray array];
    }
    for (NSData *aDataItem in aRestoredMutableDataItemsArray)
    {
        DownloadItem *aDownloadItem = [NSKeyedUnarchiver unarchiveObjectWithData:aDataItem];
        [aRestoredMutableDownloadItemsArray addObject:aDownloadItem];
    }
    return aRestoredMutableDownloadItemsArray;
}

@end
