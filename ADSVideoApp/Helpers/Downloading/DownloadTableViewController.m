
/*
 * Project: HWIFileDownload
 
 * File: HWIFileDownloader.m
 *
 */

/***************************************************************************
 
 Copyright (c) 2014-2016 Heiko Wichmann
 
 https://github.com/Heikowi/HWIFileDownload
 
 This software is provided 'as-is', without any expressed or implied warranty.
 In no event will the authors be held liable for any damages
 arising from the use of this software.
 
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 
 1. The origin of this software must not be misrepresented;
 you must not claim that you wrote the original software.
 If you use this software in a product, an acknowledgment
 in the product documentation would be appreciated
 but is not required.
 
 2. Altered source versions must be plainly marked as such,
 and must not be misrepresented as being the original software.
 
 3. This notice may not be removed or altered from any source distribution.
 
 ***************************************************************************/


/*
 DownloadTableViewController.m
 ADSVideoApp
 
 Modified by Artem Selivanov on 3/4/17.
 Copyright © 2017 ArtSelDev. All rights reserved.
 */

#import "DownloadTableViewController.h"

#import "AppDelegate.h"
#import "DownloadStore.h"
#import "DownloadItem.h"
#import "DownloadNotifications.h"
#import <HWIFileDownload/HWIFileDownloader.h>

#import "DialogViewController.h"

@interface DownloadTableViewController () <DialogContainerProtocol>

@property (nonatomic, assign) NSInteger fileNameLabelTag;
@property (nonatomic, assign) NSInteger infoTextLabelTag;
@property (nonatomic, assign) NSInteger progressViewTag;
@property (nonatomic, assign) NSInteger pauseOrResumeButtonTag;
@property (nonatomic, assign) NSInteger downloadCancelOrStateButtonTag;
@property (nonatomic, strong) NSString *downloadChar;
@property (nonatomic, strong) NSString *cancelChar;
@property (nonatomic, strong) NSString *pauseChar;
@property (nonatomic, strong) NSString *resumeChar;
@property (nonatomic, strong) NSString *completedChar;
@property (nonatomic, strong) NSString *errorChar;
@property (nonatomic, strong) NSString *cancelledChar;

@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, weak) UIProgressView *totalProgressView;
@property (nonatomic, weak) UILabel *totalProgressLocalizedDescriptionLabel;

@property (nonatomic, strong, nullable) NSDate *lastProgressChangedUpdate;
@end



@implementation DownloadTableViewController


- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithStyle:(UITableViewStyle)aTableViewStyle
{
    self = [super initWithStyle:aTableViewStyle];
    if (self)
    {
        self.fileNameLabelTag = 1;
        self.infoTextLabelTag = 2;
        self.progressViewTag = 3;
        self.pauseOrResumeButtonTag = 4;
        self.downloadCancelOrStateButtonTag = 5;
        
        self.downloadChar = @"\uf0ed"; // fa-cloud-download
        self.cancelChar = @"\uf00d"; // fa-times (Aliases: fa-remove, fa-close)
        self.pauseChar = @"\uf04c"; // fa-pause
        self.resumeChar = @"\uf021"; // fa-refresh
        self.completedChar = @"\uf00c"; // fa-check
        self.errorChar = @"\uf0e7"; // fa-bolt (Aliases: fa-flash)
        self.cancelledChar = @"\uf05e"; // fa-ban
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onDownloadDidComplete:) name:downloadDidCompleteNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onProgressDidChange:) name:downloadProgressChangedNotification object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(unzipProgressDidChange:) name:unzipProgressChangedNotification object:nil];
		
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
        {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onTotalProgressDidChange:) name:totalDownloadProgressChangedNotification object:nil];
        }
		
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
    }
    return self;
}


- (void)applicationWillResignActive:(NSNotification *)notification {
	[self dismissIfAllFinished];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.rowHeight = 109.0;
    [self.tableView registerNib:[UINib nibWithNibName:@"DownloadTableViewCell" bundle:[NSBundle mainBundle]]
		 forCellReuseIdentifier:@"DownloadTableViewCell"];
    
    UIBarButtonItem *aRightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
																			style:UIBarButtonItemStylePlain
																		   target:self
																		   action:@selector(onCancellAll)];
	aRightBarButtonItem.tintColor = [UIColor whiteColor];
    self.parentViewController.navigationItem.rightBarButtonItem = aRightBarButtonItem;
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.tableView reloadData];

	for (DownloadItem * item in appDelegate.downloadStore.downloadItemsArray) {
		[appDelegate.downloadStore startDownloadWithDownloadItem:item];
	}
	
	if (![self dismissIfAllFinished]) {
		[(DialogViewController *)self.parentViewController resize];
	}
 }

- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
	
	if (self.delegate && [self.delegate respondsToSelector:@selector(downloadControllerWillBeDismissed)]) {
		[self.delegate downloadControllerWillBeDismissed];
	}
}


#pragma mark - UITableViewDataSource


- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)aSection
{
    AppDelegate *theAppDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    return [theAppDelegate downloadStore].downloadItemsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)anIndexPath
{
    UITableViewCell *aTableViewCell = [aTableView dequeueReusableCellWithIdentifier:@"DownloadTableViewCell" forIndexPath:anIndexPath];
    
    UIButton *aPauseOrResumeButton = (UIButton *)[aTableViewCell viewWithTag:self.pauseOrResumeButtonTag];
    UIButton *aDownloadCancelOrStateButton = (UIButton *)[aTableViewCell viewWithTag:self.downloadCancelOrStateButtonTag];
    
    [aPauseOrResumeButton.titleLabel setFont:[UIFont fontWithName:@"FontAwesome" size:20.0]];
    [aDownloadCancelOrStateButton.titleLabel setFont:[UIFont fontWithName:@"FontAwesome" size:20.0]];
    
    UILabel *anInfoTextLabel = (UILabel *)[aTableViewCell viewWithTag:self.infoTextLabelTag];
    if ([UIFont respondsToSelector:@selector(monospacedDigitSystemFontOfSize:weight:)])
    {
        [anInfoTextLabel setFont:[UIFont monospacedDigitSystemFontOfSize:10.0 weight:UIFontWeightRegular]];
    }
    else
    {
        [anInfoTextLabel setFont:[UIFont systemFontOfSize:10.0]];
    }
    
    AppDelegate *theAppDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    DownloadItem *aDownloadItem = [[theAppDelegate downloadStore].downloadItemsArray objectAtIndex:anIndexPath.row];
    
    [self prepareTableViewCell:aTableViewCell withDownloadItem:aDownloadItem];
    
    return aTableViewCell;
}


- (CGFloat)tableView:(UITableView *)aTableView heightForHeaderInSection:(NSInteger)aSection
{
    CGFloat aHeaderHeight = 0.0;
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1 && appDelegate.downloadStore.downloadItemsArray.count > 1)
    {
        if (aSection == 0)
        {
            aHeaderHeight = 20.0;
        }
    }
    return aHeaderHeight;
}


- (UIView *)tableView:(UITableView *)aTableView viewForHeaderInSection:(NSInteger)aSection
{
	if (appDelegate.downloadStore.downloadItemsArray.count > 1) {
		if (self.headerView == nil)
		{
			if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
			{
				if (aSection == 0)
				{
					UIView *aHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 20.0)];
					[aHeaderView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
					[aHeaderView setBackgroundColor:[Colors shared].packageBg];
					// total progress view
					UIProgressView *aProgressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
					CGRect aProgressViewRect = aProgressView.frame;
					aProgressViewRect.size.width = aHeaderView.frame.size.width;
					[aProgressView setFrame:aProgressViewRect];
					[aProgressView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
					
					aProgressView.tintColor = [Colors shared].primary;
					aProgressView.trackTintColor = [Colors shared].secondary;
					
					[aHeaderView addSubview:aProgressView];
					self.totalProgressView = aProgressView;
					// total progress localized description view
					UILabel *aLocalizedDescriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, CGRectGetMaxY(self.totalProgressView.frame), aHeaderView.frame.size.width - 20.0, 14.0)];
					if ([UIFont respondsToSelector:@selector(monospacedDigitSystemFontOfSize:weight:)])
					{
						[aLocalizedDescriptionLabel setFont:[UIFont monospacedDigitSystemFontOfSize:10.0 weight:UIFontWeightRegular]];
					}
					else
					{
						[aLocalizedDescriptionLabel setFont:[UIFont systemFontOfSize:10.0]];
					}
					[aLocalizedDescriptionLabel setTextAlignment:NSTextAlignmentCenter];
					[aLocalizedDescriptionLabel setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin)];
					[aHeaderView addSubview:aLocalizedDescriptionLabel];
					self.totalProgressLocalizedDescriptionLabel = aLocalizedDescriptionLabel;
					self.headerView = aHeaderView;
				}
			}
		}
		return self.headerView;
	}
	
	return nil;
}


#pragma mark - Actions


- (void)onCancellAll
{
	
	NSMutableArray <NSString *> *identifiers = [NSMutableArray array];
	
	for (DownloadItem * item in appDelegate.downloadStore.downloadItemsArray) {
		[identifiers addObject:item.downloadIdentifier];
	}
	
	for (NSString * identifier in identifiers) {
		[self cancelDownloadWithIdentifier:identifier];
	}
	
	[self.parentViewController.navigationController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}


- (void)onStartIndividualDownload:(id)aSender
{
    UITableViewCell *aTableViewCell = nil;
    UIView *aCurrView = (UIView *)aSender;
    while (aTableViewCell == nil)
    {
        UIView *aSuperView = [aCurrView superview];
        if ([aSuperView isKindOfClass:[UITableViewCell class]])
        {
            aTableViewCell = (UITableViewCell *)aSuperView;
        }
        aCurrView = aSuperView;
    }
    NSIndexPath *anIndexPath = [self.tableView indexPathForCell:aTableViewCell];
    if (anIndexPath)
    {
        AppDelegate *theAppDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        DownloadItem *aDownloadItem = [[theAppDelegate downloadStore].downloadItemsArray objectAtIndex:anIndexPath.row];
        
        [theAppDelegate.downloadStore startDownloadWithDownloadItem:aDownloadItem];
    }
}


- (void)onCancelIndividualDownload:(id)aSender
{
    UITableViewCell *aTableViewCell = nil;
    UIView *aCurrView = (UIView *)aSender;
    while (aTableViewCell == nil)
    {
        UIView *aSuperView = [aCurrView superview];
        if ([aSuperView isKindOfClass:[UITableViewCell class]])
        {
            aTableViewCell = (UITableViewCell *)aSuperView;
        }
        aCurrView = aSuperView;
    }
    NSIndexPath *anIndexPath = [self.tableView indexPathForCell:aTableViewCell];
    if (anIndexPath)
    {
        AppDelegate *theAppDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        DownloadItem *aDownloadItem = [[theAppDelegate downloadStore].downloadItemsArray objectAtIndex:anIndexPath.row];
        
        [self cancelDownloadWithIdentifier:aDownloadItem.downloadIdentifier];
    }
	
	[self dismissIfAllFinished];
}


- (void)onPauseResumeIndividualDownload:(id)aSender
{
    UITableViewCell *aTableViewCell = nil;
    UIView *aCurrView = (UIView *)aSender;
    while (aTableViewCell == nil)
    {
        UIView *aSuperView = [aCurrView superview];
        if ([aSuperView isKindOfClass:[UITableViewCell class]])
        {
            aTableViewCell = (UITableViewCell *)aSuperView;
        }
        aCurrView = aSuperView;
    }
    NSIndexPath *anIndexPath = [self.tableView indexPathForCell:aTableViewCell];
    if (anIndexPath)
    {
        AppDelegate *theAppDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        DownloadItem *aDownloadItem = [[theAppDelegate downloadStore].downloadItemsArray objectAtIndex:anIndexPath.row];
        
        UIButton *aButton = (UIButton *)aSender;
        if ([[aButton titleForState:UIControlStateNormal] isEqualToString:self.pauseChar])
        {
            [self pauseDownloadWithIdentifier:aDownloadItem.downloadIdentifier];
        }
        else if ([[aButton titleForState:UIControlStateNormal] isEqualToString:self.resumeChar])
        {
            [self resumeDownloadWithIdentifier:aDownloadItem.downloadIdentifier];
        }
    }
}


- (void)cancelDownloadWithIdentifier:(NSString *)aDownloadIdentifier
{
        [appDelegate.downloadStore cancelDownloadWithDownloadIdentifier:aDownloadIdentifier];
        
        NSUInteger aFoundDownloadItemIndex = [[appDelegate downloadStore].downloadItemsArray indexOfObjectPassingTest:^BOOL(DownloadItem *aDownloadItem, NSUInteger anIndex, BOOL *aStopFlag) {
            if ([aDownloadItem.downloadIdentifier isEqualToString:aDownloadIdentifier])
            {
                return YES;
            }
            return NO;
        }];
	
        if (aFoundDownloadItemIndex != NSNotFound)
        {
            NSIndexPath *anIndexPath = [NSIndexPath indexPathForRow:aFoundDownloadItemIndex inSection:0];
            [self.tableView reloadRowsAtIndexPaths:@[anIndexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
	
}


- (void)pauseDownloadWithIdentifier:(NSString *)aDownloadIdentifier
{
    AppDelegate *theAppDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    BOOL isDownloading = [theAppDelegate.fileDownloader isDownloadingIdentifier:aDownloadIdentifier];
    if (isDownloading)
    {
        HWIFileDownloadProgress *aFileDownloadProgress = [theAppDelegate.fileDownloader downloadProgressForIdentifier:aDownloadIdentifier];
        [aFileDownloadProgress.nativeProgress pause];
    }
}


- (void)resumeDownloadWithIdentifier:(NSString *)aDownloadIdentifier
{
    AppDelegate *theAppDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [theAppDelegate.downloadStore resumeDownloadWithDownloadIdentifier:aDownloadIdentifier];
}


#pragma mark - Download Notifications


- (void)onDownloadDidComplete:(NSNotification *)aNotification
{
    DownloadItem *aDownloadedDownloadItem = (DownloadItem *)aNotification.object;
    
    AppDelegate *theAppDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    NSUInteger aFoundDownloadItemIndex = [[theAppDelegate downloadStore].downloadItemsArray indexOfObjectPassingTest:^BOOL(DownloadItem *aDownloadItem, NSUInteger anIndex, BOOL *aStopFlag) {
        if ([aDownloadItem.downloadIdentifier isEqualToString:aDownloadedDownloadItem.downloadIdentifier])
        {
            return YES;
        }
        return NO;
    }];
	
    if (aFoundDownloadItemIndex != NSNotFound)
    {
        NSIndexPath *anIndexPath = [NSIndexPath indexPathForRow:aFoundDownloadItemIndex inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[anIndexPath] withRowAnimation:UITableViewRowAnimationNone];
	
		if (aDownloadedDownloadItem.status == DownloadItemStatusCancelled ||
			aDownloadedDownloadItem.status == DownloadItemStatusErrorMoved ||
			aDownloadedDownloadItem.status == DownloadItemStatusErrorUnzip ||
			aDownloadedDownloadItem.status == DownloadItemStatusError) {
			showMainWindowToastMessage(localizedWithFormat(@"{PackageName}DownloadFailed", aDownloadedDownloadItem.package.packageTitle, [aDownloadedDownloadItem.downloadError localizedDescription]));
		} else if (aDownloadedDownloadItem.status == DownloadItemStatusUnzipped) {
			if (self.delegate && [self.delegate respondsToSelector:@selector(packageLoaded:)])
				[self.delegate packageLoaded:aDownloadedDownloadItem.package];
		}
		
		[self dismissIfAllFinished];
	}
    else
    {
        NSLog(@"WARN: Completed download item not found (%@, %d)", [NSString stringWithUTF8String:__FILE__].lastPathComponent, __LINE__);
    }
	
}


- (void)onTotalProgressDidChange:(NSNotification *)aNotification
{
    NSProgress *aProgress = aNotification.object;
    self.totalProgressView.progress = (float)aProgress.fractionCompleted;
    if (aProgress.completedUnitCount != aProgress.totalUnitCount)
    {
        self.totalProgressLocalizedDescriptionLabel.text = aProgress.localizedDescription;
    }
    else
    {
        self.totalProgressLocalizedDescriptionLabel.text = @"";
    }
}


- (void)onProgressDidChange:(NSNotification *)aNotification
{
    NSTimeInterval aLastProgressChangedUpdateDelta = 0.0;
    if (self.lastProgressChangedUpdate)
    {
        aLastProgressChangedUpdateDelta = [[NSDate date] timeIntervalSinceDate:self.lastProgressChangedUpdate];
    }
    // refresh progress display about four times per second
    if ((aLastProgressChangedUpdateDelta == 0.0) || (aLastProgressChangedUpdateDelta > 0.25))
    {
        [self.tableView reloadData];
        self.lastProgressChangedUpdate = [NSDate date];
    }
}

- (void)unzipProgressDidChange:(NSNotification *)notification {
	DownloadItem * item = (DownloadItem *)notification.object;
	
	NSInteger index = [appDelegate.downloadStore.downloadItemsArray indexOfObject:item];
	
	[self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}


#pragma mark - Table View


- (void)prepareTableViewCell:(UITableViewCell *)aTableViewCell withDownloadItem:(DownloadItem *)aDownloadItem
{
    UILabel *aFileNameLabel = (UILabel *)[aTableViewCell viewWithTag:self.fileNameLabelTag];
    UILabel *anInfoTextLabel = (UILabel *)[aTableViewCell viewWithTag:self.infoTextLabelTag];
    
    UIButton *aPauseOrResumeButton = (UIButton *)[aTableViewCell viewWithTag:self.pauseOrResumeButtonTag];
	[aPauseOrResumeButton setTintColor:[Colors shared].secondary];
	
    UIButton *aDownloadCancelOrStateButton = (UIButton *)[aTableViewCell viewWithTag:self.downloadCancelOrStateButtonTag];
    [aDownloadCancelOrStateButton setTintColor:[Colors shared].secondary];
	
	
    AppDelegate *theAppDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    UIProgressView *aProgressView = (UIProgressView *)[aTableViewCell viewWithTag:self.progressViewTag];
    [aProgressView setHidden:YES];
	aProgressView.tintColor = [Colors shared].primary;
	aProgressView.trackTintColor = [Colors shared].secondary;

    aFileNameLabel.text = [NSString stringWithFormat:@"%@ - %@", aDownloadItem.package.organization.title, aDownloadItem.package.packageTitle];
    
    [self prepareDownloadCancelOrStateButton:aDownloadCancelOrStateButton forDownloadItemStatus:aDownloadItem.status];
    [self preparePauseResumeButton:aPauseOrResumeButton forDownloadItemStatus:aDownloadItem.status];
    
    if (aDownloadItem.status == DownloadItemStatusNotStarted)
    {
        anInfoTextLabel.text = localized(@"NotStarted");
    }
    else if (aDownloadItem.status == DownloadItemStatusStarted)
    {
        BOOL isWaitingForDownload = [theAppDelegate.fileDownloader isWaitingForDownloadOfIdentifier:aDownloadItem.downloadIdentifier];
        if (isWaitingForDownload)
        {
            aProgressView.progress = 0.0;
            anInfoTextLabel.text = localized(@"WaitingForDownload");
        }
        else
        {
            HWIFileDownloadProgress *aFileDownloadProgress = [theAppDelegate.fileDownloader downloadProgressForIdentifier:aDownloadItem.downloadIdentifier];
            if (aFileDownloadProgress)
            {
                [aProgressView setHidden:NO];
                float aProgress = 0.0;
                if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
                {
                    aProgress = aFileDownloadProgress.nativeProgress.fractionCompleted;
                }
                else
                {
                    aProgress = aFileDownloadProgress.downloadProgress;
                }
                aProgressView.progress = aProgress;
                if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
                {
                    anInfoTextLabel.text = aFileDownloadProgress.nativeProgress.localizedAdditionalDescription;
                }
                else
                {
                    anInfoTextLabel.text = [DownloadTableViewController displayStringForRemainingTime:aFileDownloadProgress.estimatedRemainingTime];
                }
            }
            else
            {
                anInfoTextLabel.text = localized(@"StartedWithNoProgress");
            }
        }
    }
    else if (aDownloadItem.status == DownloadItemStatusCompleted)
    {
        aFileNameLabel.text = [NSString stringWithFormat:@"%@ - %@", aDownloadItem.package.organization.title, aDownloadItem.package.packageTitle];
        anInfoTextLabel.text = localized(@"DownloadCompletedPreparingExtract");
    }
    else if (aDownloadItem.status == DownloadItemStatusPaused)
    {
        [aProgressView setHidden:NO];
        aProgressView.progress = aDownloadItem.progress.downloadProgress;
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
        {
            anInfoTextLabel.text = aDownloadItem.progress.lastLocalizedAdditionalDescription;
        }
        else
        {
            anInfoTextLabel.text = [DownloadTableViewController displayStringForRemainingTime:aDownloadItem.progress.estimatedRemainingTime];
        }
    }
    else if (aDownloadItem.status == DownloadItemStatusCancelled)
    {
        anInfoTextLabel.text = localized(@"Cancelled");
    }
    else if ((aDownloadItem.status == DownloadItemStatusError) || (aDownloadItem.status == DownloadItemStatusInterrupted))
    {
        if (aDownloadItem.downloadError)
        {
            NSString *aFinalErrorMessage = nil;
            if (aDownloadItem.downloadErrorMessagesStack.count > 0)
            {
                aFinalErrorMessage = [NSString stringWithFormat:@"http status: %@\n%@\n%@", @(aDownloadItem.lastHttpStatusCode), [aDownloadItem.downloadErrorMessagesStack componentsJoinedByString:@"\n"], aDownloadItem.downloadError.localizedDescription];
            }
            else
            {
                aFinalErrorMessage = [NSString stringWithFormat:@"http status: %@\n%@", @(aDownloadItem.lastHttpStatusCode), aDownloadItem.downloadError.localizedDescription];
            }
            anInfoTextLabel.text = aFinalErrorMessage;
        }
        else
        {
            anInfoTextLabel.text = [NSString stringWithFormat:@"Error (http status: %@)", @(aDownloadItem.lastHttpStatusCode)];
        }
    }
	else if (aDownloadItem.status == DownloadItemStatusErrorMoved)
	{
		anInfoTextLabel.text = aDownloadItem.unzipProgress[@"localizedDescription"];

	}
	else if (aDownloadItem.status == DownloadItemStatusErrorUnzip)
	{
		anInfoTextLabel.text = aDownloadItem.unzipProgress[@"localizedDescription"];
	}
	else if (aDownloadItem.status == DownloadItemStatusMoved)
	{
		NSString * localizedDescription = aDownloadItem.unzipProgress[@"localizedDescription"] ? : @"Copying Error";
		NSString * localizedAdditionalDescription = aDownloadItem.unzipProgress[@"localizedAdditionalDescription"] ? : @"Copying Error";
		
		anInfoTextLabel.text = [NSString stringWithFormat:@"%@ - %@", localizedDescription, localizedAdditionalDescription];
		
		[aProgressView setHidden:NO];
		aProgressView.progress =  [aDownloadItem.unzipProgress[@"progress"] floatValue];
	}
	else if (aDownloadItem.status == DownloadItemStatusZipped)
	{
		NSString * localizedDescription = aDownloadItem.unzipProgress[@"localizedDescription"] ? : @"Extarcting Error";
		NSString * localizedAdditionalDescription = aDownloadItem.unzipProgress[@"localizedAdditionalDescription"] ? : @"Extarcting Error";
		
		anInfoTextLabel.text = [NSString stringWithFormat:@"%@ - %@", localizedDescription, localizedAdditionalDescription];
		CGFloat progress = aDownloadItem.unzipProgress[@"progress"] ? [aDownloadItem.unzipProgress[@"progress"] floatValue] : 0.f;
		[aProgressView setHidden:NO];
		aProgressView.progress = progress;
	}
	else if (aDownloadItem.status == DownloadItemStatusUnzipped)
	{
		anInfoTextLabel.text = localized(@"Completed");
	}
    else
    {
        NSLog(@"ERR: Unhandled download status %@ (%@, %d)", @(aDownloadItem.status), [NSString stringWithUTF8String:__FILE__].lastPathComponent, __LINE__);
    }
}


- (void)prepareDownloadCancelOrStateButton:(UIButton *)aButton forDownloadItemStatus:(DownloadItemStatus)aStatus
{
    NSString *aButtonTitle = [aButton titleForState:UIControlStateNormal];
    
    switch (aStatus) {
            
        case DownloadItemStatusNotStarted:
            if ([aButtonTitle isEqualToString:self.downloadChar] == NO)
            {
                [aButton setEnabled:YES];
                [aButton removeTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
                [aButton addTarget:self action:@selector(onStartIndividualDownload:) forControlEvents:UIControlEventTouchUpInside];
                [aButton setTitle:self.downloadChar forState:UIControlStateNormal];
            }
            break;
            
        case DownloadItemStatusStarted:
        case DownloadItemStatusPaused:
            if ([aButtonTitle isEqualToString:self.cancelChar] == NO)
            {
                [aButton setEnabled:YES];
                [aButton removeTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
                [aButton addTarget:self action:@selector(onCancelIndividualDownload:) forControlEvents:UIControlEventTouchUpInside];
                [aButton setTitle:self.cancelChar forState:UIControlStateNormal];
            }
            break;
		case DownloadItemStatusMoved:
		case DownloadItemStatusZipped:
		case DownloadItemStatusUnzipped:
        case DownloadItemStatusCompleted:
            if ([aButtonTitle isEqualToString:self.completedChar] == NO)
            {
                [aButton setEnabled:NO];
                [aButton setTitle:self.completedChar forState:UIControlStateNormal];
            }
            break;
            
        case DownloadItemStatusCancelled:
            if ([aButtonTitle isEqualToString:self.cancelledChar] == NO)
            {
                [aButton setEnabled:NO];
                [aButton setTitle:self.cancelledChar forState:UIControlStateNormal];
            }
            break;
			
        case DownloadItemStatusError:
		case DownloadItemStatusErrorMoved:
		case DownloadItemStatusErrorUnzip:
        case DownloadItemStatusInterrupted:
            if ([aButtonTitle isEqualToString:self.errorChar] == NO)
            {
                [aButton setEnabled:NO];
                [aButton setTitle:self.errorChar forState:UIControlStateNormal];
            }
			break;
        default:
            NSLog(@"ERR: Invalid status %@ (%@, %d)", @(aStatus), [NSString stringWithUTF8String:__FILE__].lastPathComponent, __LINE__);
            break;
    }
}


- (void)preparePauseResumeButton:(UIButton *)aButton forDownloadItemStatus:(DownloadItemStatus)aStatus
{
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
    {
        switch (aStatus) {
                
            case DownloadItemStatusStarted:
            {
                NSString *aButtonTitle = [aButton titleForState:UIControlStateNormal];
                if ([aButtonTitle isEqualToString:self.pauseChar] == NO)
                {
                    [aButton removeTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
                    [aButton addTarget:self action:@selector(onPauseResumeIndividualDownload:) forControlEvents:UIControlEventTouchUpInside];
                    [aButton setHidden:NO];
                    [aButton setTitle:self.pauseChar forState:UIControlStateNormal];
                }
            }
                break;
                
            case DownloadItemStatusPaused:
            case DownloadItemStatusError:
            case DownloadItemStatusInterrupted:
            {
                NSString *aButtonTitle = [aButton titleForState:UIControlStateNormal];
                if ([aButtonTitle isEqualToString:self.resumeChar] == NO)
                {
                    [aButton removeTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
                    [aButton addTarget:self action:@selector(onPauseResumeIndividualDownload:) forControlEvents:UIControlEventTouchUpInside];
                    [aButton setHidden:NO];
                    [aButton setTitle:self.resumeChar forState:UIControlStateNormal];
                }
            }
                break;
                
            default:
            {
                NSString *aButtonTitle = [aButton titleForState:UIControlStateNormal];
                if (aButtonTitle.length > 0)
                {
                    [aButton setHidden:YES];
                    [aButton setTitle:@"" forState:UIControlStateNormal];
                }
            }
                
                break;
        }
    }
    else
    {
        switch (aStatus) {
                
            case DownloadItemStatusError:
            case DownloadItemStatusInterrupted:
            {
                NSString *aButtonTitle = [aButton titleForState:UIControlStateNormal];
                if ([aButtonTitle isEqualToString:self.resumeChar] == NO)
                {
                    [aButton removeTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
                    [aButton addTarget:self action:@selector(onPauseResumeIndividualDownload:) forControlEvents:UIControlEventTouchUpInside];
                    [aButton setHidden:NO];
                    [aButton setTitle:self.resumeChar forState:UIControlStateNormal];
                }
            }
                break;
                
            default:
            {
                NSString *aButtonTitle = [aButton titleForState:UIControlStateNormal];
                if (aButtonTitle.length > 0)
                {
                    [aButton setHidden:YES];
                    [aButton setTitle:@"" forState:UIControlStateNormal];
                }
            }
                
                break;
        }
    }
}


#pragma mark - Utilities


+ (nonnull NSString *)displayStringForRemainingTime:(NSTimeInterval)aRemainingTime
{
    NSNumberFormatter *aNumberFormatter = [[NSNumberFormatter alloc] init];
    [aNumberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [aNumberFormatter setMinimumFractionDigits:1];
    [aNumberFormatter setMaximumFractionDigits:1];
    [aNumberFormatter setDecimalSeparator:@"."];
	return localizedWithFormat(@"EstimatedRemainig{Time}", [aNumberFormatter stringFromNumber:@(aRemainingTime)]);
}


- (BOOL)dismissIfAllFinished {
	BOOL shouldDissmiss = YES;
	for (DownloadItem * item in appDelegate.downloadStore.downloadItemsArray) {
		if (item.status != DownloadItemStatusCancelled &&
			item.status != DownloadItemStatusUnzipped &&
			item.status != DownloadItemStatusErrorMoved &&
			item.status != DownloadItemStatusErrorUnzip &&
			item.status != DownloadItemStatusError) {
			
			shouldDissmiss = NO;
			break;
		}
	}
	
	if (shouldDissmiss) {
		[self onCancellAll];
	}
	return shouldDissmiss;
}

#pragma mark - DialogContainerProtocol -

- (CGSize)neededSizeForTable:(UITableView *)table numberOfRows:(NSInteger)count{
	CGSize s = self.parentViewController.view.frame.size;
	CGFloat h = 0;
	
	for (int i = 0; i < count; i++) {
		h += [table rectForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]].size.height;
	}
	
	s.height = h;
	return s;
}

- (CGSize)neededCotentSize {
	CGSize s = [self neededSizeForTable:self.tableView numberOfRows:[self tableView:self.tableView numberOfRowsInSection:0]];
	return CGSizeMake(s.width, s.height + (appDelegate.downloadStore.downloadItemsArray.count > 1 ? 20.f : 0.f));
}


- (BOOL)shouldDismissPresentedControllerByOutsideTap {
	return NO;
}

- (void)presentedControllerWillBeDismissed {}


- (BOOL)shouldHideNavBar {
	return NO;
}


- (NSArray <DialogAction *> *)dialogActionsForDialog:(DialogViewController *)dialog {
	return nil;
}



@end
