/*
 * Project: HWIFileDownload
 
 * File: HWIFileDownloader.m
 *
 */

/***************************************************************************
 
 Copyright (c) 2014-2016 Heiko Wichmann
 
 https://github.com/Heikowi/HWIFileDownload
 
 This software is provided 'as-is', without any expressed or implied warranty.
 In no event will the authors be held liable for any damages
 arising from the use of this software.
 
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 
 1. The origin of this software must not be misrepresented;
 you must not claim that you wrote the original software.
 If you use this software in a product, an acknowledgment
 in the product documentation would be appreciated
 but is not required.
 
 2. Altered source versions must be plainly marked as such,
 and must not be misrepresented as being the original software.
 
 3. This notice may not be removed or altered from any source distribution.
 
 ***************************************************************************/



/*
 //
 //  DownloadStore.h
 //  ADSVideoApp
 //
 //  Modified by Artem Selivanov on 3/4/17.
 //  Copyright © 2017 ArtSelDev. All rights reserved.
 //
 */

#import <Foundation/Foundation.h>
#import <HWIFileDownload/HWIFileDownloadDelegate.h>

@class DownloadItem;

@interface DownloadStore : NSObject<HWIFileDownloadDelegate>


@property (atomic, strong, readonly, nonnull) NSMutableArray<DownloadItem *> *downloadItemsArray;

- (void)setupDownloadItems:(NSArray <Package *> * _Nullable)packagesToLoad;

- (void)startDownloadWithDownloadItem:(nonnull DownloadItem *)aDownloadItem;

- (void)cancelDownloadWithDownloadIdentifier:(nonnull NSString *)aDownloadIdentifier;

- (void)resumeDownloadWithDownloadIdentifier:(nonnull NSString *)aDownloadIdentifier;

@end
