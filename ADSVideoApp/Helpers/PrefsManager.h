//
//  PrefsManager.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/17/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class Package;
@class PackageOrganization;

@interface PrefsManager : NSObject

#pragma mark - First Install Launch -

+ (BOOL)isFirstInstallLaunch;
+ (void)setIsFirstLaunch:(BOOL)isFirstlaunch;

#pragma mark - Was Launched -

+ (BOOL)wasAlreadyLaunched;
+ (void)setWasAlreadyLaunched;


#pragma mark - Loaded Packages -

+ (void)setLoadedPackages:(NSArray <Package *> *)packages;
+ (NSArray <Package *> *)loadedPackages;

+ (void)addPackageToLoaded:(Package *)package;
+ (void)removePackageFromLoaded:(Package *)package;

#pragma mark - Organizations Of Loaded Packages -

+ (void)setOrganizations:(NSArray <PackageOrganization *> *)organizations;
+ (NSArray <PackageOrganization *> *)organizations;

+ (void)addOrganization:(PackageOrganization *)packageOrganization;
+ (void)removeOrganizationById:(NSInteger)orgId;

+ (PackageOrganization *)packageOrganizationById:(NSInteger)orgId;

+ (BOOL)isPackagesLoadedForOrgWithId:(NSInteger)orgId;

#pragma mark - Colors -

+ (void)setPrimaryColor:(UIColor *)primaryColor;
+ (UIColor *)getPrimaryColor;

+ (void)setSecondaryColor:(UIColor *)secondary;
+ (UIColor *)getSecondaryColor;

+ (void)setPrimaryDarkColor:(UIColor *)primaryDarkColor;
+ (UIColor *)getPrimaryDarkColor;

+ (void)setAccentColor:(UIColor *)accentColor;
+ (UIColor *)getAccentColor;

+ (void)setPackageBgColor:(UIColor *)packageBgColor;
+ (UIColor *)getPackageBgColor;

+ (void)setPackageButtonsColor:(UIColor *)packageBgColor;
+ (UIColor *)getPackageButtonsColor;


@end
