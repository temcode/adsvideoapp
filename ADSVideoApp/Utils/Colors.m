//
//  Colors.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/19/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "Colors.h"

#define colorPrimaryHexString @"#666666"

#define colorSecondaryHexString @"#4cacc0"

#define colorPrimaryDarkHexString @"#999999"

#define colorAccentHexString @"#999999"

#define colorOrangeHexString @"#C3971A"

#define colorPackageBgHexString @"#EEEEEE"



@interface Colors ()


@end


@implementation Colors

+ (Colors *)shared {
	static Colors *colors = nil;
	static dispatch_once_t onceToken;
	
	dispatch_once(&onceToken, ^{
		colors = [[Colors alloc] init];
	});
	
	return colors;
}

- (instancetype)init {
	self = [super init];
	if (self) {
		self.primary = [PrefsManager getPrimaryColor] ? : uiColorWithHexString(colorPrimaryHexString);
		self.secondary = [PrefsManager getSecondaryColor] ? : uiColorWithHexString(colorSecondaryHexString);
		self.primaryDark = [PrefsManager getPrimaryDarkColor] ? : uiColorWithHexString(colorPrimaryDarkHexString);
		self.accent = [PrefsManager getAccentColor] ? : uiColorWithHexString(colorAccentHexString);
		self.packageBg = [PrefsManager getPackageBgColor] ? : uiColorWithHexString(colorPackageBgHexString);
		self.packagesButtons = [PrefsManager getPackageButtonsColor] ? : uiColorWithHexString(colorOrangeHexString);
	}
	return self;
}

#pragma mark - Setters -

- (void)setPrimary:(UIColor *)primary {
	_primary = primary;
	[PrefsManager setPrimaryColor:primary];
}

- (void)setPrimaryDark:(UIColor *)primaryDark {
	_primaryDark = primaryDark;
	[PrefsManager setPrimaryDarkColor:primaryDark];
}

- (void)setAccent:(UIColor *)accent {
	_accent = accent;
	[PrefsManager setAccentColor:accent];
}

- (void)setPackageBg:(UIColor *)packageBg {
	_packageBg = packageBg;
	[PrefsManager setPackageBgColor:packageBg];
}

- (void)setPackagesButtons:(UIColor *)packagesButtons {
	_packagesButtons = packagesButtons;
	[PrefsManager setPackageButtonsColor:packagesButtons];
}


@end
