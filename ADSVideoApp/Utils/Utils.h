//
//  Utils.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/4/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

#define WELF_MACRO __weak __block typeof(self) welf = self;

#define IS_SIMULATOR [Utils isSimulator]

#define appDelegate ((AppDelegate *)[UIApplication sharedApplication].delegate)

#define localized(key) [NSBundle.mainBundle localizedStringForKey:(key) value:@"" table:nil]
#define localizedWithFormat(format, ...) [NSString localizedStringWithFormat:localized(format), ## __VA_ARGS__]

#define mainURL [NSURL URLWithString:[baseURLString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]

#define defaultNotificationCenter [NSNotificationCenter defaultCenter]

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define IS_OS_8 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0 && [[[UIDevice currentDevice] systemVersion] floatValue] < 9.0)
#define IS_OS_9_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0)

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define IS_LANDSCAPE UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation])
#define IS_PORTRAIT UIDeviceOrientationIsPortrait([[UIDevice currentDevice] orientation])

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_SIZE [[UIScreen mainScreen] bounds].size

#define toastPositionMainWindow [NSValue valueWithCGPoint:CGPointMake(topWindow.bounds.size.width / 2, (topWindow.bounds.size.height - 100.0))]

#define topWindow [Utils getTopWindow]

#define showMainWindowToastMessage(message) [Utils showMainWindowToastMessage:message]

#define videosFolderName videosAPIKey
#define documentsFolderName documentsAPIKey

#define manifestPath [Utils getManifestJsonPath]
#define splashPath [Utils getSplashPath]
#define logoPath [Utils getLogoPath]
#define documentsFolderPath [Utils getDocumentsFolderPath]
#define videosFolderPath [Utils getVideosFolderPath]
#define contentFolderPath [Utils getContentFolderPath]
#define videoFolderPathById(id) [Utils getVideoFolderPathById:id]
#define documentFolderPathById(id) [Utils getDocumentFolderPathById:id]
#define videoFilePathById(id) [Utils getVideoFilePathById:id]
#define documentFilePathById(id) [Utils getDocumentFilePathById:id]
#define videoFileURLById(id) [Utils getVideoFileURLById:id]
#define documentFileURLById(id) [Utils getDocumentFileURLById:id]
#define videoFileURLByPath(path) [Utils getVideoFileURLByPath:path]
#define videoHTMLPathById(id) [Utils getVideoHTMLPathById:id]
#define videoHTMLById(id) [Utils getVideoHTMLById:id]
#define videoHTMLByPath(path) [Utils getVideoHTMLByPath:path]




typedef NS_ENUM(NSInteger, CompletionStatus) {
	CompletionStatusSucces,
	CompletionStatusError
};

typedef NS_ENUM(NSInteger, ChooseViewControllerType) {
	ChooseViewControllerTypeNotSpecified = -1,
	ChooseViewControllerDocuments = 0,
	ChooseViewControllerVideos
};

typedef void (^CompletionBlock)(CompletionStatus status, id _Nullable resultObj, NSError * _Nullable error);
typedef void (^VoidBlock)(void);

@interface Utils : NSObject

+ (BOOL)isSimulator;

+ (NSError * _Nullable)errorWithLocalizedDescription:(NSString * _Nonnull)description
												code:(NSInteger)code
										 domainClass:(Class _Nonnull)domainClass;


+ (NSString * _Nullable)getContentFolderPath ;
+ (NSString * _Nullable)getVideosFolderPath;
+ (NSString * _Nullable)getDocumentsFolderPath;
+ (NSString * _Nullable)getLogoPath;
+ (NSString * _Nullable)getSplashPath;
+ (NSString * _Nullable)getManifestJsonPath;
+ (BOOL)isFileExistsAtPath:(NSString * _Nullable)path isDirectory:(BOOL * _Nullable)isDirectory;
+ (BOOL)isFileExistsAtPath:(NSString * _Nullable)path;

+ (NSString * _Nullable)getVideoFolderPathById:(NSInteger)identifier;
+ (NSString * _Nullable)getVideoFilePathById:(NSInteger)identifier;
+ (NSURL * _Nullable)getVideoFileURLById:(NSInteger)identifier;
+ (NSURL * _Nullable)getVideoFileURLByPath:(NSString * _Nullable)path;
+ (NSString * _Nullable)getVideoHTMLPathById:(NSInteger)identifier;
+ (NSString * _Nullable)getVideoHTMLById:(NSUInteger)identifier;
+ (NSString * _Nullable)getVideoHTMLByPath:(NSString * _Nullable)path;

+ (NSString * _Nullable)getDocumentFolderPathById:(NSInteger)identifier;
+ (NSString * _Nullable)getDocumentFilePathById:(NSInteger)identifier;
+ (NSURL * _Nullable)getDocumentFileURLById:(NSInteger)identifier;

+ (UIWindow * _Nonnull)getMainWindow;
+ (UIWindow * _Nonnull)getTopWindow;

+ (NSDate * _Nonnull)getDateFromString:(NSString * _Nonnull)dateString;

+ (void)showMainWindowToastMessage:(NSString * _Nullable)message;

#pragma mark - File Manager -

+ (NSString * _Nullable)appDocumentsDirectoryPath;

+ (NSString * _Nullable)organizationsFolderPath;
+ (NSString * _Nullable)organizationFolderPathByOrgId:(NSInteger)orgID;

+ (NSString * _Nullable)packageFolderPathByPackageId:(NSInteger)packageID orgId:(NSInteger)orgID;
+ (NSString * _Nullable)sessionPackageFolderPath;

NS_ASSUME_NONNULL_BEGIN

+ (NSDictionary * _Nullable)getSessionConfigurationDictionaryForPackageWithError:(NSError **  _Nullable)error;

+ (BOOL)createFolderForPackage:(Package * _Nonnull)package error:(NSError **)error;
+ (BOOL)createFolderForPackageContent:(Package * _Nonnull)package error:(NSError **)error;
+ (BOOL)createFolderAtPath:(NSString * _Nonnull)path error:(NSError **)error;
+ (BOOL)deleteFolderOfPackage:(Package * _Nonnull)package error:(NSError **)error;
NS_ASSUME_NONNULL_END


+ (void)deleteAllPackages;
+ (BOOL)deletePackageZip:(Package * _Nonnull)package;
+ (BOOL)needUpdatePackage:(Package * _Nonnull)package;

@end

#pragma mark - Inlines -

NS_INLINE void performBlockOnQueue(NSOperationQueue * _Nullable queue, VoidBlock _Nullable block, BOOL needWait) {
	
	if (block) {
		if (queue) {
			if ([queue isEqual:[NSOperationQueue currentQueue]]) {
				block();
			} else {
				if (needWait) {
					[queue addOperations:@[[NSBlockOperation blockOperationWithBlock:block]] waitUntilFinished:YES];
				} else {
					[queue addOperationWithBlock:block];
				}
			}
		} else {
			NSLog(@"Trying to perform block on Nil Queue");
		}
	} else {
		NSLog(@"Trying to perform Nil block on Queue: %@", queue);
	}
}

NS_INLINE void performBlockOnMainThreadSync(VoidBlock _Nullable block) {
	if (block) {
		performBlockOnQueue([NSOperationQueue mainQueue], block, YES);
	}
}

NS_INLINE void performBlockOnMainThread(VoidBlock _Nullable block) {
	if (block) {
		performBlockOnQueue([NSOperationQueue mainQueue], block, NO);
	}
}
