//
//  Constants.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/4/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#ifndef Constants_h
#define Constants_h

#pragma mark - Strings -

extern CGFloat const toastDuration;

#pragma mark - API -


extern NSString * const appIdAPIString;
extern NSString * const baseURLString;

extern NSString * const appIdAPIKey;
extern NSString * const appCodeAPIKey;
extern NSString * const appInstructionAPIKey;
extern NSString * const appPrimaryColorAPIKey;
extern NSString * const appSecondaryColorAPIKey;
extern NSString * const supportActivityAPIKey;
extern NSString * const supportActionTitleAPIKey;
extern NSString * const supportEmailAPIKey;

extern NSString * const appTitleAPIKey;
extern NSString * const videoActivityAPIKey;
extern NSString * const videoActionTitleAPIKey;
extern NSString * const titleAPIKey;
extern NSString * const videosAPIKey;
extern NSString * const idAPIKey;
extern NSString * const pointsAPIKey;
extern NSString * const timestampAPIKey;
extern NSString * const documentActivityAPIKey;
extern NSString * const documentActionTitleAPIKey;
extern NSString * const documentsAPIKey;
extern NSString * const filenameAPIKey;
extern NSString * const manifestAPIKey;
extern NSString * const splashAPIKey;
extern NSString * const logoAPIKey;

extern NSString * const isPublicAPIKey;
extern NSString * const nameAPIKey;
extern NSString * const packagesAPIKey;
extern NSString * const supportEmailAPIKey;


extern NSString * const createdAPIKey;
extern NSString * const modifiedAPIKey;
extern NSString * const prefixAPIKey;
extern NSString * const defaultPrimaryColorAPIKey;
extern NSString * const defaultSecondaryColorAPIKey;
extern NSString * const defaultVideoButtonTitleAPIKey;
extern NSString * const defaultDocumentsButtonTitleAPIKey;
extern NSString * const defaultSupportButtonTitleAPIKey;
extern NSString * const defaultSupportEmailAPIKey;

extern NSString * const orgIdAPIKey;
extern NSString * const app_IdAPIKey;
extern NSString * const passcodeAPIKey;
extern NSString * const matchingDataAPIKey;

extern NSString * const joinDataAPIKey;
extern NSString * const packageIdAPIKey;
extern NSString * const AppHasPackageHasOrgAPIKey;

extern NSString * const urlAPIKey;

extern NSString * const typeAPIKey;
extern NSString * const linkAPIKey;
extern NSString * const labelAPIKey;

extern NSString * const linksActivityAPIKey;
extern NSString * const linksActionTitleAPIKey;
extern NSString * const linksAPIKey;

extern NSString * const orgsAPIKey;
extern NSString * const contentKey;

extern NSString * const emailLinkType;
extern NSString * const phoneLinkType;
extern NSString * const webLinkType;


extern NSString * const organizationsFolderName;
extern NSString * const packagesFolderName;
extern NSString * const contentFolderName;
extern NSString * const manifestJsonName;
extern NSString * const splashName;
extern NSString * const logoName;

extern NSString * const pngExtension;
extern NSString * const pdfExtension;
extern NSString * const jsonExtension;
extern NSString * const mp4Extension;
extern NSString * const htmlExtension;


#endif /* Constants_h */

