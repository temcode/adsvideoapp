//
//  Utils.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/4/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "Utils.h"
#import "Video.h"


@implementation Utils

+ (BOOL)isSimulator {
#if TARGET_IPHONE_SIMULATOR
	return YES;
#else
	return NO;
#endif
}

#pragma mark - Errors -

+ (NSError *)errorWith:(NSString *)description
	   localizedReason:(NSString *)reason
   localizedSuggestion:(NSString *)suggestion
				  code:(NSInteger)code
		   domainClass:(Class _Nonnull)domainClass {
	
	NSDictionary *userInfo = @{
							   NSLocalizedDescriptionKey: description,
							   NSLocalizedFailureReasonErrorKey: reason,
							   NSLocalizedRecoverySuggestionErrorKey: suggestion
							   };
	NSError *error = [NSError errorWithDomain:[NSString stringWithFormat:@"%@.ErrorDomain.%@", [[NSBundle mainBundle] bundleIdentifier], NSStringFromClass(domainClass)]
										 code:code
									 userInfo:userInfo];
	
	return error;
}

+ (NSError *)errorWithLocalizedDescription:(NSString *)description
									  code:(NSInteger)code
							   domainClass:(Class _Nonnull)domainClass {
	
	return [Utils errorWith:description localizedReason:@"" localizedSuggestion:@"" code:code domainClass:domainClass];
}

#pragma mark - File Manager -

+ (NSString *)appDocumentsDirectoryPath {
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
	return documentsDirectory;
}

+ (NSString *)organizationsFolderPath {
	return [[Utils appDocumentsDirectoryPath] stringByAppendingPathComponent:organizationsFolderName];
}

+ (NSString *)organizationFolderPathByOrgId:(NSInteger)orgID {
	return [[Utils organizationsFolderPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%ld", (long)orgID]];
}


+ (NSString *)packageFolderPathByPackageId:(NSInteger)packageID orgId:(NSInteger)orgID{
	return  [[Utils organizationFolderPathByOrgId:orgID] stringByAppendingPathComponent:[NSString stringWithFormat:@"%ld", (long)packageID]];
}

+ (NSString *)packageZipPath:(Package *)package {
	return [[Utils packageFolderPathByPackageId:package.ID orgId:package.orgID] stringByAppendingPathComponent:@"content.zip"];
}

+ (NSString *)sessionPackageFolderPath {
	return  [Utils packageFolderPathByPackageId:[PackageSession sharedSession].package.ID
										  orgId:[PackageSession sharedSession].package.orgID];
}

+ (BOOL)createFolderForPackage:(Package *)package error:(NSError ** _Nullable)error {
	return [Utils createFolderAtPath:[package packageFolderPath] error:error];

}

+ (BOOL)createFolderForPackageContent:(Package *)package error:(NSError ** _Nullable)error {
	return [Utils createFolderAtPath:[package packageContentFolderPath] error:error];
}

+ (BOOL)createFolderAtPath:(NSString *)path error:(NSError **)error {
	BOOL succes = NO;
	BOOL isDirectory = NO;
	if ([[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&isDirectory]) {
		if (!isDirectory) {
			succes = [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:error];
			if (!succes && *error) {
				NSLog(@"%@", [*error localizedDescription]);
			}
		} else {
			succes = YES;
		}
	} else {
		succes = [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:error];
		if (!succes && *error) {
			NSLog(@"%@", [*error localizedDescription]);
		}
	}
	
	return succes;
}

+ (BOOL)deleteFolderOfPackage:(Package *)package error:(NSError **)error {
	
	NSString * packageFolderPath = [package packageFolderPath];
	NSFileManager * fileManager = [NSFileManager defaultManager];
	if ([fileManager fileExistsAtPath:packageFolderPath]) {
		if (![fileManager removeItemAtPath:packageFolderPath error:error]) {
			if (*error) {
				NSLog(@"deleteContentOfPackage - %ld - Error: %@", (long)package.ID, [*error localizedDescription]);
			}
		} else {
			[PrefsManager removePackageFromLoaded:package];
			NSString * orgForlder = [Utils organizationFolderPathByOrgId:package.orgID];
			
			if ([fileManager fileExistsAtPath:orgForlder]) {
				if ([fileManager contentsOfDirectoryAtPath:orgForlder error:error].count == 0) {
					if (![fileManager removeItemAtPath:orgForlder error:error]) {
						if (*error) {
							NSLog(@"deleteContentOfPackage - %ld - Error: %@", (long)package.ID, [*error localizedDescription]);
						}
					}
				}
			}
			return YES;
		}
	} else {
		return YES;
	}
	
	return NO;
}

+ (BOOL)deletePackageZip:(Package *)package {
	if ([[NSFileManager defaultManager] fileExistsAtPath:[Utils packageZipPath:package]]) {
		NSError * error = nil;
		return [[NSFileManager defaultManager] removeItemAtPath:[Utils packageZipPath:package] error:&error];
	}
	
	return YES;
	
}

+ (void)deleteAllPackages {
	NSFileManager * fileManager = [NSFileManager defaultManager];
	
	NSString * organizationsFolderPath = [Utils organizationsFolderPath];
	BOOL isDirectory = NO;
	
	if ([fileManager fileExistsAtPath:organizationsFolderPath isDirectory:&isDirectory]) {
		NSError * error = nil;
		if (![fileManager removeItemAtPath:organizationsFolderPath error:&error]) {
			if (error) {
				NSLog(@"deleteAllPackages Error: %@", [error localizedDescription]);
			}
		} else {
			[PrefsManager setLoadedPackages:nil];
		}
	}
}


+ (BOOL)needUpdatePackage:(Package *)package {
	NSArray <Package *> * loadedPackages = [PrefsManager loadedPackages];
	
	for (Package * loaded in loadedPackages) {
		if (loaded.orgID == package.orgID && loaded.ID == package.ID) {
//			if ([package.created compare:loaded.created] == NSOrderedDescending) {
//				return YES;
//				break;
//			}
//			
//			if ([package.modified compare:loaded.modified] == NSOrderedDescending) {
//				return YES;
//				break;
//			}
			if (![package.modified isEqualToString:loaded.modified]) {
				return YES;
				break;
			}
			
			return NO;
			break;
		}
	}
	return YES;
}

#pragma mark - Parsing -


+ (NSString *)getContentFolderPath {
	return [[Utils sessionPackageFolderPath] stringByAppendingPathComponent:contentFolderName];
}

+ (NSString *)getVideosFolderPath {
	return [[Utils getContentFolderPath] stringByAppendingPathComponent:videosFolderName];
}

+ (NSString *)getDocumentsFolderPath {
	return [[Utils getContentFolderPath] stringByAppendingPathComponent:documentsFolderName];
}

+ (NSString *)getLogoPath {
	return [[[Utils getContentFolderPath] stringByAppendingPathComponent:logoName] stringByAppendingPathExtension:pngExtension];
}

+ (NSString *)getSplashPath {
	return [[[Utils getContentFolderPath] stringByAppendingPathComponent:splashName] stringByAppendingPathExtension:pngExtension];
}

+ (NSString *)getManifestJsonPath {
	NSString * path = [[[Utils getContentFolderPath] stringByAppendingPathComponent:manifestJsonName] stringByAppendingPathExtension:jsonExtension];
	return path;
}

+ (BOOL)isFileExistsAtPath:(NSString *)path isDirectory:(BOOL *)isDirectory {
	return path && path.length > 0 ? [[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:isDirectory] : NO;
}

+ (BOOL)isFileExistsAtPath:(NSString *)path {
	
	return [Utils isFileExistsAtPath:path isDirectory:nil];
}

+ (NSDictionary *)getSessionConfigurationDictionaryForPackageWithError:(NSError **)error {
	NSDictionary * sessionConfigurationDict = nil;
	NSString * path = manifestPath;
	if ([Utils isFileExistsAtPath:path]) {
		NSURL * localFileURL = [NSURL fileURLWithPath:manifestPath];
		
		NSData * contentOfLocalFile = [NSData dataWithContentsOfURL:localFileURL];
		id object = [NSJSONSerialization JSONObjectWithData:contentOfLocalFile
													options:NSJSONReadingAllowFragments | NSJSONReadingMutableContainers
													  error:error];
		
		if (*error) {
			NSLog(@"Error - %@", [*error localizedDescription]);
		} else {
			if ([NSJSONSerialization isValidJSONObject:object]) {
				sessionConfigurationDict = (NSDictionary *)object;
			}
		}
	}
	
	return sessionConfigurationDict;
}

+ (NSString *)getVideoFolderPathById:(NSInteger)identifier {
	return [videosFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%ld", (long)identifier]];
}

+ (NSString *)getVideoFilePathById:(NSInteger)identifier {
	return [Utils getVideoFilePathById:identifier forElement:VideoElementFile];
}

+ (NSString *)getVideoHTMLPathById:(NSInteger)identifier {
	return [Utils getVideoFilePathById:identifier forElement:VideoElementHtml];
}

+ (NSString *)getVideoFilePathById:(NSInteger)identifier forElement:(VideoElement)element {
	NSString * videoFilePath = nil;
	NSString * videoDirPath = videoFolderPathById(identifier);
	
	BOOL isDirectory = NO;
	BOOL exists = [Utils isFileExistsAtPath:videoDirPath isDirectory:&isDirectory];
	
	if (exists && isDirectory) {
		NSFileManager *fileManager = [NSFileManager defaultManager];
		NSArray *contents = [fileManager contentsOfDirectoryAtURL:[NSURL URLWithString:videoDirPath]
									   includingPropertiesForKeys:@[]
														  options:NSDirectoryEnumerationSkipsHiddenFiles
															error:nil];
		
		NSString * pathExtension = [Video pathExtensionFor:element];
		if (pathExtension) {
			NSString * videoFilename = nil;
			NSString * format = [NSString stringWithFormat:@"pathExtension == '%@'", pathExtension];
			NSPredicate *predicate = [NSPredicate predicateWithFormat:format];
			NSArray * files = [contents filteredArrayUsingPredicate:predicate];
			if (files.count > 0) {
				videoFilename = ((NSURL *)files.firstObject).lastPathComponent;
			}
			if (videoFilename) {
				videoFilePath = [videoDirPath stringByAppendingPathComponent:videoFilename];
			}
		}
	}
	
	return videoFilePath;
}


+ (NSURL *)getVideoFileURLById:(NSInteger)identifier {
	NSURL * videoFileURL = nil;
	NSString * videoFilePath = videoFilePathById(identifier);
	
	if (videoFilePath) {
		videoFileURL = [NSURL fileURLWithPath:videoFilePath];
	}
	
	return videoFileURL;
}

+ (NSString *)getDocumentFolderPathById:(NSInteger)identifier {
	return [documentsFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%ld", (long)identifier]];
}

+ (NSString *)getDocumentFilePathById:(NSInteger)identifier {
	NSString * documentFilePath = nil;
	NSString * documentsDirPath = documentFolderPathById(identifier);
	
	BOOL isDirectory = NO;
	BOOL exists = [Utils isFileExistsAtPath:documentsDirPath isDirectory:&isDirectory];
	
	if (exists && isDirectory) {
		NSFileManager *fileManager = [NSFileManager defaultManager];
		NSArray *contents = [fileManager contentsOfDirectoryAtURL:[NSURL URLWithString:documentsDirPath]
									   includingPropertiesForKeys:@[]
														  options:NSDirectoryEnumerationSkipsHiddenFiles
															error:nil];
		
		
		NSString * videoFilename = nil;
		NSString * format = [NSString stringWithFormat:@"pathExtension == 'pdf'"];
		NSPredicate *predicate = [NSPredicate predicateWithFormat:format];
		NSArray * files = [contents filteredArrayUsingPredicate:predicate];
		if (files.count > 0) {
			videoFilename = ((NSURL *)files.firstObject).lastPathComponent;
		}
		if (videoFilename) {
			documentFilePath = [documentsDirPath stringByAppendingPathComponent:videoFilename];
		}
		
	}
	
	return documentFilePath;
}

+ (NSURL *)getDocumentFileURLById:(NSInteger)identifier {
	NSURL * docFileURL = nil;
	NSString * docFilePath = documentFilePathById(identifier);
	
	if (docFilePath) {
		docFileURL = [NSURL fileURLWithPath:docFilePath];
	}
	
	return docFileURL;
}

+ (NSURL *)getVideoFileURLByPath:(NSString *)path {
	NSURL * videoFileURL = nil;
	
	if (path) {
		videoFileURL = [NSURL fileURLWithPath:path];
	}
	
	return videoFileURL;
}

+ (NSString *)getVideoHTMLById:(NSUInteger)identifier {

	return [Utils getVideoHTMLByPath:videoHTMLPathById(identifier)];
}

+ (NSString *)getVideoHTMLByPath:(NSString *)path {
	
	NSString * htmlString = nil;
	if ([Utils isFileExistsAtPath:path]) {
		NSURL * localFileURL = [NSURL fileURLWithPath:path];
		NSError * deserializingError = nil;
		htmlString = [NSString stringWithContentsOfURL:localFileURL
											  encoding:NSUTF8StringEncoding
												 error:&deserializingError];
	
		if (deserializingError) {
			NSLog(@"Error - %@", [deserializingError localizedDescription]);
			return nil;
		}
	}
	
	return htmlString;
}

+ (UIWindow *)getMainWindow {
	return [UIApplication sharedApplication].keyWindow;
}

+ (UIWindow *)getTopWindow {
//	return [[UIApplication sharedApplication] windows].lastObject;
	return [UIApplication sharedApplication].keyWindow ;
}

+ (NSDate *)getDateFromString:(NSString *)dateString {
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
	NSDate *date = [dateFormatter dateFromString:dateString];
	return date;
}

#pragma mark - Toast -

+ (void)showMainWindowToastMessage:(NSString *)message {
	[topWindow hideToasts];
	[topWindow makeToast:message duration:toastDuration position:toastPositionMainWindow];
}



@end
