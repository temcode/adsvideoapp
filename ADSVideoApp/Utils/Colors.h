//
//  Colors.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/19/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UIColor+Expanded.h"

#pragma mark - Macro -

#define uiColorRGBA(r,g,b,a) [UIColor colorWithRed:r/255.f green:g/255.f blue:b/255.f alpha:a]
#define uiColorWithHexString(hexString) [UIColor colorWithHexString:hexString]
#define cgColor(uiColor) [uiColor CGColor]
#define cgColorWithRGBA(r,g,b,a) [uiColorRGBA(r,g,b,a) CGColor]
#define cgColorWithHexString(hexString) [uiColorWithHexString(hexString) CGColor]

@interface Colors : NSObject

@property (strong, nonatomic) UIColor * primary;
@property (strong, nonatomic) UIColor * secondary;
@property (strong, nonatomic) UIColor * primaryDark;
@property (strong, nonatomic) UIColor * accent;
@property (strong, nonatomic) UIColor * packagesButtons;
@property (strong, nonatomic) UIColor * packageBg;

+ (Colors *)shared;

@end
