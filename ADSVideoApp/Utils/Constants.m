//
//  Constants.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/4/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <Foundation/Foundation.h>

CGFloat const toastDuration						= 3.f;

NSString * const baseURLString					= @"http://packages.sonatatechnologysolutions.com";
NSString * const appIdAPIString					= @"com.sonatalearning.quickstep.ios";

NSString * const appCodeAPIKey					= @"appCode";
NSString * const appIdAPIKey					= @"appID";

NSString * const appInstructionAPIKey			= @"appInstruction";
NSString * const appPrimaryColorAPIKey			= @"appPrimaryColor";
NSString * const appSecondaryColorAPIKey		= @"appSecondaryColor";
NSString * const supportActivityAPIKey			= @"supportActivity";
NSString * const supportActionTitleAPIKey		= @"supportActionTitle";
NSString * const supportEmailAPIKey				= @"support_email_address";

NSString * const appTitleAPIKey					= @"appTitle";
NSString * const videoActivityAPIKey			= @"videoActivity";
NSString * const videoActionTitleAPIKey			= @"videoActionTitle";
NSString * const titleAPIKey					= @"title";
NSString * const videosAPIKey					= @"videos";
NSString * const idAPIKey						= @"id";
NSString * const pointsAPIKey					= @"points";
NSString * const timestampAPIKey				= @"timestamp";
NSString * const documentActivityAPIKey         = @"documentActivity";
NSString * const documentActionTitleAPIKey      = @"documentActionTitle";
NSString * const documentsAPIKey				= @"documents";
NSString * const filenameAPIKey					= @"filename";
NSString * const manifestAPIKey					= @"manifest";
NSString * const splashAPIKey					= @"splash";
NSString * const logoAPIKey						= @"logoAPIKey";

NSString * const isPublicAPIKey					= @"is_public";
NSString * const nameAPIKey						= @"name";
NSString * const packagesAPIKey					= @"package";

NSString * const urlAPIKey						= @"url";

NSString * const AppHasPackageHasOrgAPIKey						= @"AppHasPackageHasOrg";

NSString * const typeAPIKey = @"type";
NSString * const linkAPIKey = @"link";
NSString * const labelAPIKey = @"label";

NSString * const linksActivityAPIKey = @"linksActivity";
NSString * const linksActionTitleAPIKey = @"linksActionTitle";
NSString * const linksAPIKey = @"links";


NSString * const createdAPIKey							= @"created";
NSString * const modifiedAPIKey							= @"modified";
NSString * const prefixAPIKey							= @"prefix";
NSString * const defaultPrimaryColorAPIKey						= @"default_primary_color";
NSString * const defaultSecondaryColorAPIKey						= @"default_secondary_color";
NSString * const defaultVideoButtonTitleAPIKey						= @"default_video_button_label";
NSString * const defaultDocumentsButtonTitleAPIKey						= @"default_document_button_label";
NSString * const defaultSupportButtonTitleAPIKey						= @"default_support_button_label";
NSString * const defaultSupportEmailAPIKey						= @"default_support_email_address";


NSString * const orgIdAPIKey = @"org_id";

NSString * const app_IdAPIKey = @"app_Id";
NSString * const passcodeAPIKey = @"passcode";
NSString * const matchingDataAPIKey = @"_matchingData";
NSString * const joinDataAPIKey = @"_joinData";
NSString * const packageIdAPIKey = @"package_id";

NSString * const orgsAPIKey						= @"orgs";

NSString * const contentKey						= @"content";

NSString * const emailLinkType = @"email";
NSString * const phoneLinkType = @"phone";
NSString * const webLinkType = @"web";

NSString * const organizationsFolderName		= @"Organizations";
NSString * const packagesFolderName				= @"Packages";
NSString * const contentFolderName				= @"Content";
NSString * const manifestJsonName				= @"manifest";
NSString * const splashName						= @"splash";
NSString * const logoName						= @"logo";

NSString * const pngExtension					= @"png";
NSString * const pdfExtension					= @"pdf";
NSString * const jsonExtension					= @"json";
NSString * const mp4Extension					= @"mp4";
NSString * const htmlExtension					= @"html";



