//
//  VideoViewController.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/4/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "VideoViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import <WebKit/WebKit.h>

@interface VideoViewController () <AVPlayerViewControllerDelegate, WKNavigationDelegate>

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIView *playerContainerView;
@property (weak, nonatomic) IBOutlet UIView *playerGestureView;

@property (weak, nonatomic) IBOutlet UIButton *playVideoButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *scrollContentView;

@property (strong, nonatomic) WKWebView *webView;

@property (weak, nonatomic) AVPlayerViewController * playerController;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *playerHeightConstraint;

@property (assign, nonatomic) BOOL playerFullScreen;
@property (assign, nonatomic) BOOL isFirstLoad;
@property (assign, nonatomic) BOOL observingBounds;

@property (assign, nonatomic) CGRect lastContentOverlayBounds;

@property (strong, nonatomic) NSLayoutConstraint * webViewTopConstraint;

@property (assign, nonatomic) CMTime currentStopTime;
@end

@implementation VideoViewController

- (void)dealloc {
	
	
	
	if (self.observingBounds) {
		[self.playerController removeObserver:self forKeyPath:@"videoBounds"];
	}
	
	[self.playerController.player pause];
	self.playerController.player = nil;
	self.video = nil;
	self.point = nil;
	self.playerController = nil;
	
	if (self.playerGestureView && self.playerGestureView.gestureRecognizers.count > 0) {
		for (UIGestureRecognizer *gesture in self.playerGestureView.gestureRecognizers) {
			[self.playerGestureView removeGestureRecognizer:gesture];
		}
	}
	
	self.webViewTopConstraint = nil;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	self.playerFullScreen = NO;
	self.isFirstLoad = YES;
	self.observingBounds = NO;
	
	self.lastContentOverlayBounds = CGRectZero;
	
	[self.backButton setTitle:self.video.title forState:UIControlStateNormal];
	
	[self configureWebView];
	[[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback
									 withOptions:AVAudioSessionCategoryOptionDuckOthers
										   error:nil];
	
	[self.playerGestureView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToFullscreen)]];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	if (self.isFirstLoad) {
	
	}
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	if (self.isFirstLoad) {
		self.lastContentOverlayBounds = self.playerController.contentOverlayView.bounds;
		[self.playerController addObserver:self forKeyPath:@"videoBounds" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:NULL];
		self.observingBounds = YES;
		
		if (self.point) {
			[self goToFullscreen];
		}
		
		NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%lu%%'",(unsigned long)250];
		[self.webView evaluateJavaScript:jsString completionHandler:nil];
		
		self.isFirstLoad = NO;
	}
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
	[super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
	
	self.webViewTopConstraint.constant = size.height / 2.f;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Player -

- (void)configurePlayer {
	self.playerController.player = [[AVPlayer alloc] initWithURL:[self.video mp4URL]];
	
	if (IS_OS_9_OR_LATER && [self.playerController respondsToSelector:@selector(setAllowsPictureInPicturePlayback:)]) {
		self.playerController.allowsPictureInPicturePlayback = NO;
	}
	NSTimeInterval pointTimestamp = 0;
	if (self.point) {
		pointTimestamp = [self.point time];
		self.currentStopTime = CMTimeMake(pointTimestamp, 1);
	}
	
	self.currentStopTime = CMTimeMake(pointTimestamp, 1);
}

- (IBAction)onPlayVideo:(id)sender {
	[self.playerController goToFullscreen];
	[self.playerController.player play];
}

- (void)goToFullscreen {
	[self.playVideoButton setHighlighted:YES];
	[self.playerController.player seekToTime:self.currentStopTime];
	[self.playerController goToFullscreen];
	[self.playerController.player play];
	[self.playVideoButton setHighlighted:NO];
}


- (void)didEnterFullScreen {
	
}

- (void)didReturnedFromFullScreen{
	self.currentStopTime = [self.playerController.player currentTime];
	WELF_MACRO
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
		if ([welf isPlaying]) {
			[welf.playerController.player pause];
		}
	});
}

- (BOOL)isPlaying {

	if ((self.playerController.player.rate != 0) && (self.playerController.player.error == nil)) {
		return YES;
	}
	return NO;
}

#pragma mark - KVO -

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *, id> *)change context:(void *)context {
	if (object == self.playerController) {
		if ([keyPath isEqualToString:@"videoBounds"]) {
			
			if (!CGSizeEqualToSize(self.lastContentOverlayBounds.size, self.playerController.contentOverlayView.bounds.size)) {
			
				CGFloat lastSquare = self.lastContentOverlayBounds.size.height * self.lastContentOverlayBounds.size.width;
				CGFloat currentSquare = self.playerController.contentOverlayView.bounds.size.height * self.playerController.contentOverlayView.bounds.size.width;
				
				if (lastSquare < currentSquare) {
					[self didEnterFullScreen];
				} else if (lastSquare > currentSquare) {
					[self didReturnedFromFullScreen];
				}
				
				self.lastContentOverlayBounds = self.playerController.contentOverlayView.bounds;
			}
		}
	}
}

#pragma mark - Configure Web View -

- (void)configureWebView {
	WKWebViewConfiguration * conf = [[WKWebViewConfiguration alloc] init];
	conf.preferences.minimumFontSize = 17.f;
	self.webView = [[WKWebView alloc] initWithFrame:CGRectMake(0,
															   self.playerContainerView.frame.size.height,
															   self.view.frame.size.width,
															   self.view.frame.size.height - self.playerContainerView.frame.size.height)
									  configuration:conf];
	self.webView.translatesAutoresizingMaskIntoConstraints = NO;
	self.webView.scrollView.scrollEnabled = YES;
	self.webView.clipsToBounds = NO;

	NSString * htmlString = [self.video HTML];
	if (htmlString) {
		[self.webView loadHTMLString:htmlString baseURL:[[NSBundle mainBundle] resourceURL]];
	}
	
	[self.scrollContentView addSubview:self.webView];

	[[NSLayoutConstraint constraintWithItem:self.webView
								 attribute:NSLayoutAttributeTop
								 relatedBy:NSLayoutRelationEqual
									toItem:self.playerContainerView
								 attribute:NSLayoutAttributeBottom
								multiplier:1.0
								  constant:16.0f] setActive:YES];
	
	[[NSLayoutConstraint constraintWithItem:self.webView
								  attribute:NSLayoutAttributeBottom
								  relatedBy:NSLayoutRelationLessThanOrEqual
									 toItem:self.scrollContentView
								  attribute:NSLayoutAttributeBottom
								 multiplier:1.0
								   constant:16.0] setActive:YES];
	
	[[NSLayoutConstraint constraintWithItem:self.webView
								  attribute:NSLayoutAttributeLeading
								  relatedBy:NSLayoutRelationEqual
									 toItem:self.scrollContentView
								  attribute:NSLayoutAttributeLeading
								 multiplier:1.0
								   constant:16.0] setActive:YES];
	[[NSLayoutConstraint constraintWithItem:self.scrollContentView
								  attribute:NSLayoutAttributeTrailing
								  relatedBy:NSLayoutRelationEqual
									 toItem:self.webView
								  attribute:NSLayoutAttributeTrailing
								 multiplier:1.0
								   constant:16.0] setActive:YES];
	
	self.webViewTopConstraint = [NSLayoutConstraint constraintWithItem:self.webView
								  attribute:NSLayoutAttributeHeight
								  relatedBy:NSLayoutRelationEqual
									 toItem:nil
								  attribute:NSLayoutAttributeNotAnAttribute
								 multiplier:1.0
								   constant:self.view.frame.size.height / 2.f];
	
	[self.webViewTopConstraint setActive:YES];

	[self.view layoutIfNeeded];
}

#pragma mark - Navigation -

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	
	if ([segue.identifier isEqualToString:@"videoPlayerControllerSegue"]) {
		self.playerController = (AVPlayerViewController *)segue.destinationViewController;
		[self configurePlayer];
	}
}


- (IBAction)onBack:(id)sender {
	
	if (self.navigationController.presentingViewController) {
		[self.navigationController dismissViewControllerAnimated:YES completion:nil];
	} else {
		[self.navigationController popViewControllerAnimated:YES];
	}
}

@end
