//
//  LinkTableViewCell.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 3/3/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "LinkTableViewCell.h"

NSString * const linkTableViewCellReuseId = @"linkTableViewCellReuseId";

@interface LinkTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *linkTitle;
@end

@implementation LinkTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
	
	self.linkTitle.textColor = [Colors shared].secondary;
	
	[self prepareForReuse];
}

- (void)prepareForReuse {
	[super prepareForReuse];
	
	self.linkTitle.text = nil;
}

- (void)configureCellWithTitle:(NSString *)title {
	self.linkTitle.text = title;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

	
}

@end
