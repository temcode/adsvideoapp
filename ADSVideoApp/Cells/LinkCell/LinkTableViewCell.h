//
//  LinkTableViewCell.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 3/3/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const linkTableViewCellReuseId;

@interface LinkTableViewCell : UITableViewCell

- (void)configureCellWithTitle:(NSString *)title;

@end
