//
//  ChooseTableViewCell.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/4/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "ChooseTableViewCell.h"
#import "SubTableViewCell.h"

#import "Video.h"
#import "Document.h"

NSString * const chooseCellReuseId = @"ChooseCellReuseId";

@interface ChooseTableViewCell () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UILabel *mainTitleLabel;
@property (weak, nonatomic) IBOutlet UITableView *innerTableView;
@property (weak, nonatomic) IBOutlet UIImageView *arrowLeft
;
@property (weak, nonatomic) IBOutlet UIImageView *arrowDown;
@property (weak, nonatomic) IBOutlet UIView *separatorView;

@property (strong, nonatomic) NSArray <VideoPoint *> * dataSource;
@property (strong, nonatomic) NSIndexPath * indexPath;
@property (assign, nonatomic) ChooseViewControllerType type;

@property (weak, nonatomic) IBOutlet UIButton *expandCell;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *innerTableHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainCellViewTopConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *innerTableTableViewWidth;
@property (assign, nonatomic) BOOL configured;
@property (assign, nonatomic) BOOL showDownSeparator;

@end

@implementation ChooseTableViewCell

- (void)dealloc {
	self.dataSource = nil;
	self.indexPath = nil;
}

- (void)awakeFromNib {
    [super awakeFromNib];
	
	
	self.innerTableView.delegate = self;
	self.innerTableView.dataSource = self;
	
	self.innerTableView.rowHeight = UITableViewAutomaticDimension;
	self.innerTableView.estimatedRowHeight = 50.f;
	self.innerTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
	
	self.mainView.backgroundColor = [Colors shared].secondary;
	
	[self prepareForReuse];
}

- (void)prepareForReuse {
	[super prepareForReuse];

	if ([self.mainView gestureRecognizers].count > 0) {
		for (UIGestureRecognizer *gesture in [self gestureRecognizers]) {
			[self.mainView removeGestureRecognizer:gesture];
		}
	}
	
	if ([self.mainTitleLabel gestureRecognizers].count > 0) {
		for (UIGestureRecognizer *gesture in [self gestureRecognizers]) {
			[self.mainTitleLabel removeGestureRecognizer:gesture];
		}
	}
	
	self.mainTitleLabel.text = @"";
	self.arrowLeft.hidden = YES;
	self.arrowDown.hidden = YES;
	self.expandCell.enabled = NO;
	
	self.indexPath = nil;
	self.type = ChooseViewControllerTypeNotSpecified;
	self.dataSource = nil;
	self.innerTableView.hidden = YES;
	
	self.innerTableHeightConstraint.constant = 0.f;
	self.mainCellViewTopConstraint.constant = 8.f;
	
	self.separatorView.hidden = YES;
	
	[self layoutIfNeeded];
	
	self.configured = NO;
	
	
	
	[self.innerTableView reloadData];
	
}

- (void)scaleCell {
	if (self.delegate && [self.delegate respondsToSelector:@selector(scaleCell:atIndexPath:)]) {
		[self.delegate scaleCell:self atIndexPath:self.indexPath];
	}
}

- (void)openVideo {
	if (self.delegate && [self.delegate respondsToSelector:@selector(openVideoForCell:atIndexPath:)]) {
		[self.delegate openVideoForCell:self atIndexPath:self.indexPath];
	}
}

- (void)configureCellWith:(id)item
				 selected:(BOOL)selected
				  clipped:(BOOL)clipped
					 showDownSeparator:(BOOL)showDownSeparator
			  atIndexPath:(NSIndexPath *)indexPath {
	self.indexPath = indexPath;
	self.innerTableTableViewWidth.constant = SCREEN_WIDTH;
	[self layoutIfNeeded];
	
	if ([item isKindOfClass:[Video class]]) {
		Video * video = (Video *)item;
		self.type = ChooseViewControllerVideos;
		self.dataSource = video.points;
		
		UITapGestureRecognizer * singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openVideo)];
		singleTap.numberOfTapsRequired = 1;
		singleTap.numberOfTouchesRequired = 1;
		[self.mainView addGestureRecognizer:singleTap];
		
		
		UITapGestureRecognizer * singleTapLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openVideo)];
		singleTapLabel.numberOfTapsRequired = 1;
		singleTapLabel.numberOfTouchesRequired = 1;
		[self.mainTitleLabel addGestureRecognizer:singleTapLabel];
		
		if (self.dataSource &&
			self.dataSource.count > 0) {
			
			self.arrowLeft.hidden = NO;
			self.expandCell.enabled = YES;
			
			if (selected) {
				
				self.arrowLeft.hidden = YES;
				self.arrowDown.hidden = NO;
							
				self.innerTableView.hidden = NO;
				[self.innerTableView reloadData];

				[self layoutSubviews];
				
				CGFloat h = 0.f;
				
				for (int i = 0; i < [self tableView:self.innerTableView numberOfRowsInSection:0]; i++) {
					h += [self.innerTableView rectForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]].size.height;
				}

				self.innerTableHeightConstraint.constant = h;
			}
		}
	
		if (clipped) {
			self.mainCellViewTopConstraint.constant = 0.f;
		}
		
		self.mainTitleLabel.text = video.title;
	} else {
		Document * document = (Document *)item;
		self.type = ChooseViewControllerDocuments;
		self.mainTitleLabel.text = document.title;
	}
	
	self.configured = YES;
}

- (IBAction)onEpander:(id)sender {
	if (self.dataSource && self.dataSource.count > 0) {
		[self scaleCell];
	}
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark - TableView Protocols -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return self.dataSource ? self.dataSource.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	SubTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:subCellReuseId forIndexPath:indexPath];
	[cell configureCellWith:self.dataSource[indexPath.row] atIndexPath:indexPath];
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	if (self.delegate && [self.delegate respondsToSelector:@selector(point:wasSelectedForCell:atIndexPath:)]) {
		[self.delegate point:self.dataSource[indexPath.row] wasSelectedForCell:self atIndexPath:self.indexPath];
	}

}


@end
