//
//  ChooseTableViewCell.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/4/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const chooseCellReuseId;

@class ChooseTableViewCell;

@protocol ChooseCellDelegate <NSObject>

- (void)point:(VideoPoint *)point wasSelectedForCell:(ChooseTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (void)scaleCell:(ChooseTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (void)openVideoForCell:(ChooseTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@end

@interface ChooseTableViewCell : UITableViewCell

@property (weak, nonatomic) id<ChooseCellDelegate> delegate;

- (void)configureCellWith:(id)item
				 selected:(BOOL)selected
				  clipped:(BOOL)clipped
					 showDownSeparator:(BOOL)showDownSeparator
			  atIndexPath:(NSIndexPath *)indexPath;

@end
