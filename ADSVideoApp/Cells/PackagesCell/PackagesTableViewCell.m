//
//  PackagesTableViewCell.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/15/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "PackagesTableViewCell.h"

NSString * const packagesCellReuseId = @"PackagesTableViewCellReuseID";

@interface PackagesTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *packageTitle;
@property (weak, nonatomic) IBOutlet UILabel *organizationTitle;
@property (weak, nonatomic) IBOutlet UIView *shadowView;

@property (strong, nonatomic) NSIndexPath * indexPath;

@property (strong, nonatomic) UILongPressGestureRecognizer * longPressGesture;
@property (assign, nonatomic) BOOL wasLongPressed;

@end


@implementation PackagesTableViewCell

- (void)dealloc {

	if (self.gestureRecognizers.count > 0) {
		for (UIGestureRecognizer * gesture in self.gestureRecognizers) {
			[self removeGestureRecognizer:gesture];
		}
	}
	
	[self removeGestureRecognizer:self.longPressGesture];
	self.longPressGesture = nil;
	
	self.indexPath = nil;
}

- (void)awakeFromNib {
    [super awakeFromNib];
	

	self.shadowView.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
	self.shadowView.layer.shadowOffset = CGSizeMake(0, 1.5f);
	self.shadowView.layer.shadowRadius = 3.f;
	self.shadowView.layer.shadowOpacity = 0.7;
	self.shadowView.layer.masksToBounds = NO;
	
	self.shadowView.layer.cornerRadius = 5.f;
	
	[self prepareForReuse];
}

- (void)prepareForReuse {
	[super prepareForReuse];
	
	for (UIGestureRecognizer * gesture in self.gestureRecognizers) {
		[self removeGestureRecognizer:gesture];
	}
	
	self.longPressGesture = nil;
	self.packageTitle.text = nil;
	self.organizationTitle.text = nil;
	self.indexPath = nil;
}

- (void)configureCellWithPackageTitle:(NSString *)packageTitle
					organizationTitle:(NSString *)organizationTitle
							indexPath:(NSIndexPath *)indexPath {
	self.indexPath = indexPath;
	
	UILongPressGestureRecognizer * longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
	longPress.minimumPressDuration = 1.f;
	[self addGestureRecognizer:longPress];
	self.wasLongPressed = NO;
	
	self.shadowView.backgroundColor = [Colors shared].packageBg;
	
	self.organizationTitle.text = organizationTitle;
	self.organizationTitle.textColor= [Colors shared].secondary;
	self.packageTitle.text = packageTitle;
	self.packageTitle.textColor = [Colors shared].primary;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

	if (selected) {
		self.contentView.backgroundColor = [UIColor darkGrayColor];
	} else {
		self.contentView.backgroundColor = [UIColor clearColor];
	}
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)longGesture {
	
	if (!self.wasLongPressed) {
		self.wasLongPressed = YES;
		[self removeGestureRecognizer:self.longPressGesture];
		self.longPressGesture = nil;
		
		
		if (self.delegate && [self.delegate respondsToSelector:@selector(cell:longPressedAtIndexPath:)]) {
			[self.delegate cell:self longPressedAtIndexPath:self.indexPath];
		}
	}
}

@end
