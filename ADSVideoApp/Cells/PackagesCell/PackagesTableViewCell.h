//
//  PackagesTableViewCell.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/15/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PackagesTableViewCell;

@protocol PackageTableCellDelegate <NSObject>

- (void)cell:(PackagesTableViewCell *)cell longPressedAtIndexPath:(NSIndexPath *)indexPath;

@end

extern NSString * const packagesCellReuseId;

@interface PackagesTableViewCell : UITableViewCell

@property (weak, nonatomic) id<PackageTableCellDelegate> delegate;

- (void)configureCellWithPackageTitle:(NSString *)package organizationTitle:(NSString *)organizationTitle indexPath:(NSIndexPath *)indexPath;

@end
