//
//  SelectPackageTableCell.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/16/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "SelectPackageTableCell.h"

NSString * const selectPackageTableCellReuseId = @"selectPackageTableCellReuseId";

@interface SelectPackageTableCell ()

@property (strong, nonatomic) NSIndexPath * indexPath;
@property (weak, nonatomic) IBOutlet UILabel *title;

@end

@implementation SelectPackageTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
	
	self.title.textColor = [Colors shared].primary;
	
	[self prepareForReuse];
}

- (void)prepareForReuse {
	[super prepareForReuse];
	
	self.indexPath = nil;
	self.title.text = nil;
}

- (void)configureCell:(NSString *)title indexPath:(NSIndexPath *)indexPath {
	self.indexPath = indexPath;
	self.title.text = title;
}


@end
