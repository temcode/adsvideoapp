//
//  SelectPackageTableCell.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/16/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const selectPackageTableCellReuseId;

@interface SelectPackageTableCell : UITableViewCell

- (void)configureCell:(NSString *)title indexPath:(NSIndexPath *)indexPath;

@end
