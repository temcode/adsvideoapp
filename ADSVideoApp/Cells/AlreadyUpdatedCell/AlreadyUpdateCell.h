//
//  AlreadyUpdateCell.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 3/2/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const alreadyUpdateCellReuseId;

@interface AlreadyUpdateCell : UITableViewCell
- (void)configureCellWith:(NSString *)packTitle orgTitle:(NSString *)orgTitle;
@end
