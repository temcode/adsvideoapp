//
//  AlreadyUpdateCell.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 3/2/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "AlreadyUpdateCell.h"

NSString * const alreadyUpdateCellReuseId = @"alreadyUpdateCellReuseId";

@interface AlreadyUpdateCell ()
@property (weak, nonatomic) IBOutlet UILabel *packageTitle;
@property (weak, nonatomic) IBOutlet UILabel *orgTitle;

@end


@implementation AlreadyUpdateCell

- (void)awakeFromNib {
    [super awakeFromNib];
	self.orgTitle.textColor= [Colors shared].secondary;
	self.packageTitle.textColor = [Colors shared].primary;
	[self prepareForReuse];
}

- (void)prepareForReuse {
	[super prepareForReuse];
	self.packageTitle.text = nil;
	self.orgTitle.text = nil;

}

- (void)configureCellWith:(NSString *)packTitle orgTitle:(NSString *)orgTitle {
	self.packageTitle.text = packTitle;
	self.orgTitle.text = orgTitle;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
