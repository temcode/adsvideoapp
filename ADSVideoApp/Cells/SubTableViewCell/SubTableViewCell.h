//
//  SubTableViewCell.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/4/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoPoint.h"

extern NSString * const subCellReuseId;

@interface SubTableViewCell : UITableViewCell

- (void)configureCellWith:(VideoPoint *)point atIndexPath:(NSIndexPath *)indexPath;

@end
