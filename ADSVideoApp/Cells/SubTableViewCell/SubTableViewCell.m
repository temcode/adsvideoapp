//
//  SubTableViewCell.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/4/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "SubTableViewCell.h"


NSString * const subCellReuseId = @"SubCellReuseId";

@interface SubTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *playImage;
@property (weak, nonatomic) IBOutlet UIView *labelsImageView;
@property (weak, nonatomic) IBOutlet UILabel *idLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *lineLabel;

@property (strong, nonatomic) NSIndexPath * indexPath;
@end

@implementation SubTableViewCell

- (void)dealloc {
	self.indexPath = nil;
}

- (void)awakeFromNib {
	[super awakeFromNib];
	
	[self prepareForReuse];
	
}

- (void)prepareForReuse {
	[super prepareForReuse];
	
	self.titleLabel.text = @"";
	
	self.idLabel.text = @"";
	self.indexPath = nil;
}

- (void)configureCellWith:(VideoPoint *)point atIndexPath:(NSIndexPath *)indexPath {
	self.indexPath = indexPath;
	
	self.lineLabel.textColor = [Colors shared].secondary;
	
	self.titleLabel.text = point.title;
	self.titleLabel.textColor = [Colors shared].secondary;
	self.idLabel.text = [NSString stringWithFormat:@"%ld", (long)(indexPath.row + 1)];
	self.idLabel.textColor = [Colors shared].secondary;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
