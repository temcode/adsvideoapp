//
//  AppDelegate.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/3/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "AppDelegate.h"

#import <HWIFileDownload/HWIFileDownloader.h>
#import "DownloadStore.h"

#import "DownloadTableViewController.h"

@interface AppDelegate ()
@property (nonnull, nonatomic, strong, readwrite) DownloadStore *downloadStore;
@property (nonnull, nonatomic, strong, readwrite) HWIFileDownloader *fileDownloader;


@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	
	self.appIsActive = NO;
	self.appInBackground = YES;
	
	if (![PrefsManager wasAlreadyLaunched]) {
		[PrefsManager setIsFirstLaunch:YES];
		[PrefsManager setWasAlreadyLaunched];
	} else if ([PrefsManager isFirstInstallLaunch]) {
		[PrefsManager setIsFirstLaunch:NO];
	}

	[SVProgressHUD setDefaultStyle:SVProgressHUDStyleCustom];
	[SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
	[SVProgressHUD setForegroundColor:[UIColor whiteColor]];
	[SVProgressHUD setBackgroundColor:[Colors shared].primary];
	
	// setup app download store
	self.downloadStore = [[DownloadStore alloc] init];
	
	// setup downloadere
	self.fileDownloader = [[HWIFileDownloader alloc] initWithDelegate:self.downloadStore maxConcurrentDownloads:10];
	[self.fileDownloader setupWithCompletion:nil];
	
	UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeAlert
																						 | UIUserNotificationTypeBadge
																						 | UIUserNotificationTypeSound) categories:nil];
	[application registerUserNotificationSettings:settings];
	
	
	return YES;
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
	NSLog(@"didRegisterUserNotificationSettings");
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
	
}


- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler {
	[self.fileDownloader setBackgroundSessionCompletionHandlerBlock:completionHandler];
}

- (void)applicationWillResignActive:(UIApplication *)application {
	self.appIsActive = NO;
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
	self.appInBackground = YES;
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
	self.appInBackground = NO;
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
	self.appIsActive = YES;
}


- (void)applicationWillTerminate:(UIApplication *)application {
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
