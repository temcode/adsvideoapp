//
//  PackagesViewController.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/15/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "PackagesViewController.h"
#import "PackagesTableViewCell.h"
#import "Package.h"
#import "Organization.h"

#import "AddPackageViaCodeDialogContainer.h"
#import "SelectPackageDialogContainerViewController.h"
#import "ChooseAddPackageWayViewController.h"
#import "DeletePackageDialogContainer.h"
#import "AlreadyUpdatePackagesContainer.h"

#import "DownloadTableViewController.h"

#import "DownloadStore.h"
#import "DownloadItem.h"
#import "DownloadNotifications.h"

#import "CustomInfiniteIndicator.h"
#import "PackagesListAPIService.h"

#import "DialogViewController.h"

#import "QRCodeReaderViewController.h"

#import <MTBBarcodeScanner/MTBBarcodeScanner.h>

@interface PackagesViewController () <UITableViewDelegate, UITableViewDataSource, PackageTableCellDelegate, ChooseAddPackageWayDelegate, CodeAddingDelegate, BrowsingPackageDelegate, DeletePackageDialogContainerDelegate, IBActionSheetDelegate, QRCodeReaderDelegate, DownloadControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray <Package *> * dataSource;
@property (weak, nonatomic) IBOutlet UIButton *sortButton;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoIconHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sortButtonHeight;
@property (strong, nonatomic) IBActionSheet * addSheet;
@property (strong, nonatomic) IBActionSheet * sortSheet;

@property (strong, nonatomic) UIRefreshControl * refreshControl;

@property (strong, nonatomic) CustomInfiniteIndicator *refreshControlCustomSpinner;

@property (weak, nonatomic) IBOutlet UILabel *deletinHintLabel;
@property (weak, nonatomic) IBOutlet UILabel *noPackagesHintLabel;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *noPackagesHalfScreenWidth;

@property (assign, nonatomic) BOOL isFirstLoad;
@end

@implementation PackagesViewController

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	[self.dataSource removeAllObjects];
	self.dataSource = nil;
	
	self.addSheet = nil;
	self.sortSheet = nil;
	
	[self.refreshControl removeFromSuperview];
	self.refreshControl = nil;
	
	self.noPackagesHalfScreenWidth = nil;
}

- (void)viewDidLoad {
	[super viewDidLoad];

	self.isFirstLoad = YES;
	
	self.dataSource = [[PrefsManager loadedPackages] mutableCopy];
	if (!self.dataSource) {
		self.dataSource = [NSMutableArray array];
	}
	
	self.noPackagesHintLabel.text = localized(@"NoPackagesHint");
	self.deletinHintLabel.text = localized(@"DeletionHint");
	self.titleLabel.text = localized(@"Packages");
	
	self.tableView.rowHeight = UITableViewAutomaticDimension;
	self.tableView.estimatedRowHeight = 80.f;
	self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
	
	[self addTableViewPullToRefresh:self.tableView];
	
	self.addButton.layer.shadowColor = [[UIColor blackColor] CGColor];
	self.addButton.layer.shadowOffset = CGSizeMake(0, 1.5f);
	self.addButton.layer.shadowRadius = 3.f;
	self.addButton.layer.shadowOpacity = 0.3;
	self.addButton.layer.cornerRadius = self.addButton.frame.size.height / 2.f;

	if (IS_LANDSCAPE) {
		self.sortButtonHeight.constant = 25.f;
		self.logoIconHeight.constant = 25.f;
	} else {
		self.sortButtonHeight.constant = 32.f;
		self.logoIconHeight.constant = 32.f;
	}
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(orientationChanged:)
												 name:UIDeviceOrientationDidChangeNotification
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(appDidBecomeActive:)
												 name:UIApplicationDidBecomeActiveNotification
											   object:nil];
}



- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	self.navigationController.navigationBar.barTintColor = [Colors shared].primary;
	
	[self reloadTable];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	[UIApplication sharedApplication].applicationIconBadgeNumber = 0;
	
	if (self.isFirstLoad) {
		if ([PrefsManager isFirstInstallLaunch]) {
			[self onAddAction:nil];
		}
		
		if (IS_IPAD) {
			[self.noPackagesHalfScreenWidth setActive:YES];
		}
		
		self.isFirstLoad = NO;
	}
	
	if (appDelegate.downloadStore.downloadItemsArray.count > 0) {
		
		BOOL shouldDissmiss = YES;
		for (DownloadItem * item in appDelegate.downloadStore.downloadItemsArray) {
			if (item.status != DownloadItemStatusCancelled &&
				item.status != DownloadItemStatusUnzipped &&
				item.status != DownloadItemStatusErrorMoved &&
				item.status != DownloadItemStatusErrorUnzip &&
				item.status != DownloadItemStatusError) {
				
				shouldDissmiss = NO;
				break;
			}
		}
		
		if (!shouldDissmiss) {
			DownloadTableViewController *downloadTableViewController = [[DownloadTableViewController alloc] initWithStyle:UITableViewStylePlain];
			downloadTableViewController.delegate = self;
			DialogViewController * dialog = [DialogViewController dialogWithTitle:localized(@"Downloading") containerController:downloadTableViewController];
			[dialog presentWith:self.navigationController];
		}
	}
}

- (void)appDidBecomeActive:(NSNotification *)notification {
	[self reloadTable];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void)orientationChanged:(NSNotification *)notificaion {
	if (self.addSheet)
		[self.addSheet rotateToCurrentOrientation];
	else if (self.sortSheet)
		[self.sortSheet rotateToCurrentOrientation];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
	[super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];

	if (IS_LANDSCAPE) {
		self.sortButtonHeight.constant = 25.f;
		self.logoIconHeight.constant = 25.f;
	} else {
		self.sortButtonHeight.constant = 32.f;
		self.logoIconHeight.constant = 32.f;
	}
	
	[self.navigationController.navigationBar layoutIfNeeded];
	
}

- (void)addTableViewPullToRefresh:(UITableView *)owner {
	
	self.refreshControl = [[UIRefreshControl alloc] init];
	
	[self.refreshControl addTarget:self action:@selector(pullToRefresh) forControlEvents:UIControlEventValueChanged];
	self.refreshControl.tintColor = [UIColor clearColor];
	self.refreshControl.translatesAutoresizingMaskIntoConstraints = YES;
	
	UITableViewController *tableViewController = [[UITableViewController alloc] init];
	tableViewController.tableView = owner;
	tableViewController.refreshControl = self.refreshControl;
	
	self.refreshControlCustomSpinner = [[CustomInfiniteIndicator alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width / 2.f - 30.f / 2.f,
																								 self.refreshControl.center.y - 30.f / 2.f,
																								 30.f,
																								 30.f)];
	self.refreshControlCustomSpinner.translatesAutoresizingMaskIntoConstraints = NO;
	self.refreshControlCustomSpinner.innerColor = [UIColor darkGrayColor];
	self.refreshControlCustomSpinner.hidden = YES;

	[self.refreshControl addSubview:self.refreshControlCustomSpinner];
	[self.refreshControl bringSubviewToFront:self.refreshControlCustomSpinner];

	NSLayoutConstraint * tabLoadingIndicatorCenterX = [NSLayoutConstraint constraintWithItem:self.refreshControlCustomSpinner
																				   attribute:NSLayoutAttributeCenterX
																				   relatedBy:NSLayoutRelationEqual
																					  toItem:self.refreshControl
																				   attribute:NSLayoutAttributeCenterX
																				  multiplier:1.0f
																					constant:0.f];
	
	NSLayoutConstraint * tabLoadingIndicatorCenterY = [NSLayoutConstraint constraintWithItem:self.refreshControlCustomSpinner
																				   attribute:NSLayoutAttributeCenterY
																				   relatedBy:NSLayoutRelationEqual
																					  toItem:self.refreshControl
																				   attribute:NSLayoutAttributeCenterY
																				  multiplier:1.0f
																					constant:0.f];
	
	NSLayoutConstraint * tabLoadingIndicatorWidth = [NSLayoutConstraint constraintWithItem:self.refreshControlCustomSpinner
																				 attribute:NSLayoutAttributeWidth
																				 relatedBy:NSLayoutRelationEqual
																					toItem:nil
																				 attribute:NSLayoutAttributeNotAnAttribute
																				multiplier:1.0f
																				  constant:30.f];
	[tabLoadingIndicatorWidth setActive:YES];
	
	
	NSLayoutConstraint * tabLoadingIndicatorHeight = [NSLayoutConstraint constraintWithItem:self.refreshControlCustomSpinner
																				  attribute:NSLayoutAttributeHeight
																				  relatedBy:NSLayoutRelationEqual
																					 toItem:nil
																				  attribute:NSLayoutAttributeNotAnAttribute
																				 multiplier:1.0f
																				   constant:30.f];
	[tabLoadingIndicatorHeight setActive:YES];
	
	[self.view addConstraints:@[tabLoadingIndicatorCenterX, tabLoadingIndicatorCenterY]];
	
	[owner addSubview:self.refreshControl];
}


- (void)pullToRefresh {
	[self.refreshControlCustomSpinner startAnimating];
	self.refreshControlCustomSpinner.hidden = NO;

	self.dataSource = [[PrefsManager loadedPackages] mutableCopy];
	
	[self reloadTable];
	
	WELF_MACRO
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
		dispatch_async(dispatch_get_main_queue(), ^{
			welf.refreshControlCustomSpinner.hidden = YES;
			[welf.refreshControlCustomSpinner stopAnimating];
			[welf.refreshControl endRefreshing];
		});
	});
}


- (void)requestPackagesViaCode:(NSString *)code {
	PackagesListAPIService * service = [[PackagesListAPIService alloc] initWithCode:code];
	WELF_MACRO
	[service callServiceForController:self withCompletion:^(CompletionStatus status, id  _Nullable resultObj, NSError * _Nullable error) {
		performBlockOnMainThread(^{
			if (status == CompletionStatusError) {
				showMainWindowToastMessage([error localizedDescription]);
			} else {
				NSArray <Package *> * packages = (NSArray <Package *> *)resultObj;
				[welf tryToLoadPackages:packages];
			}
		});
	}];
	
}

- (void)checkPackagesShouldBeLoaded:(NSArray <Package *> *)requestedPackages withCompletion:(void (^)(NSArray <Package *> * packagesToLoad))completion {

	NSMutableArray <Package *> * packagesToLoad = [NSMutableArray array];
	NSMutableArray <Package *> * alreadyUpdatedPackages = [NSMutableArray array];
	
	
	for (Package * requestedPackage in requestedPackages) {
		if ([Utils needUpdatePackage:requestedPackage]) {
			[packagesToLoad addObject:requestedPackage];
		} else {
			[alreadyUpdatedPackages addObject:requestedPackage];
		}
	}
	
	if (alreadyUpdatedPackages.count > 0) {
		
		[DialogAction setBeetweenButtons:5.f];
		[DialogAction setButtonViewMargin:10.f];
		[DialogAction setButtonsTitleFontSize:14.f];
		[DialogAction setDialogButtonHeight:35.f];
	
		AlreadyUpdatePackagesContainer * alreadyContainer = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AlreadyUpdatePackagesContainer"];
		
//		Package * package0 = alreadyUpdatedPackages[0];
//		
//		for (int i = 0; i < 20; i++) {
//			PackageOrganization * organization = [[PackageOrganization alloc] initWithId:package0.organization.ID
//																				   title:package0.organization.title
//																	 supportEmailAddress:package0.organization.supportEmailAddress
//																				isPublic:package0.organization.isPublic
//																				 created:package0.organization.created
//																				modified:package0.organization.modified
//																				  prefix:package0.organization.prefix
//																	 defaultPrimaryColor:package0.organization.defaultPrimaryColor
//																   defaultSecondaryColor:package0.organization.defaultSecondaryColor
//																 defaultVideoButtonTitle:package0.organization.defaultVideoButtonTitle
//															 defaultDocumentsButtonTitle:package0.organization.defaultDocumentsButtonTitle
//															   defaultSupportButtonTitle:package0.organization.defaultSupportButtonTitle
//															  defaultSupportEmailAddress:package0.organization.defaultSupportEmailAddress];
//			
//			Package * package = [[Package alloc] initWithId:package0.ID + 1 + i
//											   packageTitle:[NSString stringWithFormat:@"%d-pack", i]
//												  urlString:package0.urlString
//													  orgID:package0.orgID
//												   passcode:package0.passcode
//													  appId:package0.appId
//													created:package0.created
//												   modified:package0.modified
//											   organization:organization];
//			
//			[alreadyUpdatedPackages addObject:package];
//		}

		
		
		alreadyContainer.packages = alreadyUpdatedPackages;
		alreadyContainer.updatePackagesBlock = ^(NSArray <Package *> * packagesToUpdate){
			if (packagesToUpdate && packagesToUpdate.count > 0) {
				[packagesToLoad addObjectsFromArray:packagesToUpdate];
				if (completion) {
					completion(packagesToLoad);
				}
			} else {
				if (packagesToLoad.count > 0) {
					if (completion) {
						completion(packagesToLoad);
					}
				}
			}
		};
		
		DialogViewController * dialog = [DialogViewController dialogWithTitle:nil containerController:alreadyContainer];
		
		WELF_MACRO
		performBlockOnMainThread(^{
			[dialog presentWith:welf.navigationController];
		});

	} else {
		if (completion) {
			completion(packagesToLoad);
		}
	}
}

- (void)tryToLoadPackages:(NSArray <Package *> *)packages {
	WELF_MACRO
	[self checkPackagesShouldBeLoaded:packages withCompletion:^(NSArray<Package *> *packagesToLoad) {
		if (packagesToLoad && packagesToLoad > 0) {
			DownloadTableViewController *downloadTableViewController = [[DownloadTableViewController alloc] initWithStyle:UITableViewStylePlain];
			downloadTableViewController.delegate = (id<DownloadControllerDelegate>)self;
			DialogViewController * dialog = [DialogViewController dialogWithTitle:@"Downloading" containerController:downloadTableViewController];
			
			[appDelegate.downloadStore setupDownloadItems:packagesToLoad];
			
			performBlockOnMainThread(^{
				[welf.navigationController presentViewController:dialog.navigationController animated:YES completion:^{
					[welf reloadTable];
				}];
			});
		}
	}];
}

- (void)reloadTable {
	WELF_MACRO
	performBlockOnMainThread(^{
		welf.dataSource = [NSMutableArray arrayWithArray:[PrefsManager loadedPackages]];
		if (welf.dataSource.count > 0) {
			welf.tableView.hidden = NO;
			welf.dataSource = [NSMutableArray arrayWithArray:[PrefsManager loadedPackages]];
			[welf.tableView reloadData];
			welf.noPackagesHintLabel.hidden = YES;
		} else {
			welf.tableView.hidden = YES;
			welf.noPackagesHintLabel.hidden = NO;
		}
	});
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	if ([segue.identifier isEqualToString:@"OpenPackageSegueIdentifier"]) {
		[PackageSession sharedSession].package = (Package *)sender;
	} else if ([segue.identifier isEqualToString:@"PresentQRCodeScanner"]) {
		((QRCodeReaderViewController *)segue.destinationViewController).delegate = self;
	}
}

#pragma mark - Actions -

- (IBAction)onAddAction:(id)sender {
	
	
	self.addSheet = [[IBActionSheet alloc] initWithTitle:@"Add Package with"
												delegate:(id<IBActionSheetDelegate>)self
									   cancelButtonTitle:@"Cancel"
								  destructiveButtonTitle:nil
									   otherButtonTitles:@"Code", @"Browse", @"QR code",nil];

	[self.addSheet setTitleFont:[UIFont systemFontOfSize:22.f weight:UIFontWeightBold]];
	[self.addSheet setButtonBackgroundColor:[Colors shared].primary];
	[self.addSheet setButtonTextColor:[UIColor whiteColor]];
	self.addSheet.buttonResponse = IBActionSheetButtonResponseFadesOnPress;
	[self.addSheet showInView:self.navigationController.view];
	
}

- (IBAction)onSortButton:(id)sender {
	
	self.sortSheet = [[IBActionSheet alloc] initWithTitle:@"Sort"
												 delegate:(id<IBActionSheetDelegate>)self
										cancelButtonTitle:@"Cancel"
								   destructiveButtonTitle:nil
										otherButtonTitles:@"By package", @"By organization", nil];

	[self.sortSheet setTitleFont:[UIFont systemFontOfSize:20.f weight:UIFontWeightBold]];
	[self.sortSheet setButtonBackgroundColor:[Colors shared].primary];
	[self.sortSheet setButtonTextColor:[UIColor whiteColor]];
	self.sortSheet.buttonResponse = IBActionSheetButtonResponseFadesOnPress;
	[self.sortSheet showInView:self.navigationController.view];
}


#pragma mark - UITableView Protocols -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return self.dataSource ? self.dataSource.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	PackagesTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:packagesCellReuseId
																   forIndexPath:indexPath];
	Package * pack = self.dataSource[indexPath.row];
	cell.delegate = self;
	[cell configureCellWithPackageTitle:pack.packageTitle organizationTitle:pack.organization.title indexPath:indexPath];
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
	[self performSegueWithIdentifier:@"OpenPackageSegueIdentifier" sender:self.dataSource[indexPath.row]];
}

#pragma mark - ChooseAddPackageWayDelegate -

- (void)addViaCode {
	
	AddPackageViaCodeDialogContainer * container = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AddPackageViaCodeDialogContainer"];
	DialogViewController * dialog = [DialogViewController dialogWithTitle:nil containerController:container];
	container.delegate = self;
	[dialog presentWith:self.navigationController];
}

- (void)addViaBrowsing {
	SelectPackageDialogContainerViewController * container = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SelectPackageDialogContainerViewController"];
	DialogViewController * dialog = [DialogViewController dialogWithTitle:nil containerController:container];
	container.delegate = self;
	[dialog presentWith:self.navigationController];
}

#pragma mark - BrowsingPackageDelegate -

- (void)loadPackage:(Package *)package {
	[self tryToLoadPackages:@[package]];
}

#pragma mark - CodeAddingDelegate -

- (void)loadPackageViaCode:(NSString *)packageCode {
	[self requestPackagesViaCode:packageCode];
}

#pragma mark - IB Action Sheet -

- (void)actionSheet:(IBActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	if ([actionSheet isEqual:self.sortSheet]) {
		self.sortSheet = nil;
		if (buttonIndex == 0) {
			[self.dataSource sortUsingComparator:^NSComparisonResult(Package *  _Nonnull obj1, Package *  _Nonnull obj2) {
				return [obj1.packageTitle compare:obj2.packageTitle];
			}];
		} else if (buttonIndex == 1) {
			[self.dataSource sortUsingComparator:^NSComparisonResult(Package *  _Nonnull obj1, Package *  _Nonnull obj2) {
				return [obj1.organization.title compare:obj2.organization.title];
			}];
		}
		
		[self reloadTable];
		
	} else if ([actionSheet isEqual:self.addSheet]) {
		self.addSheet = nil;
		if (buttonIndex == 0) {
			AddPackageViaCodeDialogContainer * container = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AddPackageViaCodeDialogContainer"];
			DialogViewController * dialog = [DialogViewController dialogWithTitle:nil containerController:container];
			container.delegate = self;
			[dialog presentWith:self.navigationController];
			
		} else if (buttonIndex == 1) {
			
			PackagesListAPIService * service = [PackagesListAPIService getService];
			WELF_MACRO
			[service callServiceForController:self withCompletion:^(CompletionStatus status, id  _Nullable resultObj, NSError * _Nullable error) {
				performBlockOnMainThread(^{
					if (status == CompletionStatusError && error.code != 1488) {
						showMainWindowToastMessage([error localizedDescription]);
					} else {
						SelectPackageDialogContainerViewController * container = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SelectPackageDialogContainerViewController"];
						if (status == CompletionStatusError) {
							container.errorText = [error localizedDescription];
						} else {
							container.delegate = welf;
							container.organizationsDataSource = (NSArray <Organization *> *)resultObj;
						}
						
						DialogViewController * dialog = [DialogViewController dialogWithTitle:nil containerController:container];
						[dialog presentWith:self.navigationController];
					}
				});
			}];
		} else if (buttonIndex == 2) {
			if ([MTBBarcodeScanner cameraIsPresent]) {
				WELF_MACRO
				[MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
					if (success) {
						[welf performSegueWithIdentifier:@"PresentQRCodeScanner" sender:nil];
					} else {
						showMainWindowToastMessage(localized(@"CameraBlocked"));
					}
				}];
			} else {
				showMainWindowToastMessage(localized(@"CameraAbsent"));
			}
		}
	}
}

- (void)actionSheet:(IBActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
	if ([actionSheet isEqual:self.sortSheet]) {
		self.sortSheet = nil;
	} else if ([actionSheet isEqual:self.addSheet]) {
		self.addSheet = nil;
	}
}

#pragma mark - PackageTableCellDelegate -

- (void)cell:(PackagesTableViewCell *)cell longPressedAtIndexPath:(NSIndexPath *)indexPath {
	
	[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	DeletePackageDialogContainer * container = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"DeletePackageDialogContainer"];
	DialogViewController * dialog = [DialogViewController dialogWithTitle:nil containerController:container];
	container.delegate = (id <DeletePackageDialogContainerDelegate>)self;
	container.package = self.dataSource[indexPath.row];
	[dialog presentWith:self.navigationController];
	
	[self reloadTable];
}

#pragma mark - Delete Package Delegate -

- (void)deletePackage:(Package *)package {
	NSError * error = nil;
	if (![Utils deleteFolderOfPackage:package error:&error]) {
		showMainWindowToastMessage([error localizedDescription]);
	}
	
	[self reloadTable];
	
}

#pragma mark - QRCodeReaderDelegate -

- (void)didSnapedQRCode:(NSString *)codeString {
	
	showMainWindowToastMessage(localized(@"RequestPackageQRCode"));
	
	[self requestPackagesViaCode:codeString];
	
}


- (void)qrCodeReaderDidCancelled {

}


#pragma mark - DownloadControllerDelegate -

- (void)downloadControllerWillBeDismissed {
	[self reloadTable];
}

- (void)packageLoaded:(Package *)package {
	showMainWindowToastMessage(localizedWithFormat(@"{Package}Loaded", package.packageTitle));
	
	[self reloadTable];
}

@end
