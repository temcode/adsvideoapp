//
//  ChooseAddPackageWayViewController.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/17/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChooseAddPackageWayDelegate <NSObject>

- (void)addViaCode;
- (void)addViaBrowsing;

@end


@interface ChooseAddPackageWayViewController : UIViewController

@property (weak, nonatomic) id<ChooseAddPackageWayDelegate> delegate;

@end
