//
//  DeletePackageDialogContainer.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/17/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "DeletePackageDialogContainer.h"
#import "DialogViewController.h"

@interface DeletePackageDialogContainer () <DialogContainerProtocol>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIView *buttonsView;
@property (weak, nonatomic) IBOutlet UIButton *yesButton;

@property (weak, nonatomic) IBOutlet UIButton *noButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewTrailing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;

@end

@implementation DeletePackageDialogContainer

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	self.package = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.view.layer.cornerRadius = 5.f;
	self.view.clipsToBounds = YES;
	
	
	
	[self.yesButton setBackgroundColor:[Colors shared].primary];
	self.yesButton.layer.cornerRadius = 3.f;

	[self.noButton setBackgroundColor:[Colors shared].primary];
	self.noButton.layer.cornerRadius = 3.f;
	
	self.titleLabel.textColor = [Colors shared].primary;
	self.textView.textColor = [Colors shared].primary;
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	if (self.textView.contentSize.height > self.textView.frame.size.height) {
		[self.textView setScrollEnabled:YES];
	}
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)orientationChanged:(NSNotification *)notificaion {
	if (self.textView.contentSize.height > self.textView.frame.size.height) {
		[self.textView setScrollEnabled:YES];
	}
}

- (IBAction)onYes:(id)sender {
	WELF_MACRO
	[self dismissWithCompletion:^{
		if (welf.delegate && [welf.delegate respondsToSelector:@selector(deletePackage:)]) {
			[welf.delegate deletePackage:welf.package];
		}
	}];
}

- (IBAction)onNo:(id)sender {
	
	[self dismissWithCompletion:^{

	}];
}

- (void)dismissWithCompletion:(void (^)(void))completion {
	[self.parentViewController.navigationController.presentingViewController dismissViewControllerAnimated:YES completion:completion];
}

#pragma mark - DialogContainerProtocol -


- (CGSize)neededCotentSize {
	CGSize s = CGSizeZero;
	
	CGFloat h = self.contentViewTop.constant + self.contentView.frame.size.height + self.contentViewBottom.constant;
	
	CGFloat w = self.parentViewController.view.frame.size.width;
	s = CGSizeMake(w, h);
	
	return s;
}


- (BOOL)shouldDismissPresentedControllerByOutsideTap {
	return YES;
}

- (void)presentedControllerWillBeDismissed {}


- (BOOL)shouldHideNavBar {
	return YES;
}


- (NSArray <DialogAction *> *)dialogActionsForDialog:(DialogViewController *)dialog {
	return nil;
}

@end
