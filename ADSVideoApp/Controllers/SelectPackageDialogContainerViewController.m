//
//  SelectPackageDialogContainerViewController.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/15/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "SelectPackageDialogContainerViewController.h"
#import "SelectPackageTableCell.h"
#import "Organization.h"
#import "Package.h"

#import "DialogViewController.h"

@interface SelectPackageDialogContainerViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, DialogContainerProtocol>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewTrailing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewLeading;

@property (weak, nonatomic) IBOutlet UIView *orgsTextFieldBgView;
@property (weak, nonatomic) IBOutlet UIView *packagesTextFieldBgView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *orgTableTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint * packagesTextFieldTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *packagesTableTop
;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *requestButtonTop;
@property (weak, nonatomic) IBOutlet UITableView *orgTable;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *orgTableHeight;
@property (weak, nonatomic) IBOutlet UITextField *orgTextField;

@property (weak, nonatomic) IBOutlet UITableView *packagesTable;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *packagesTableHeight;
@property (weak, nonatomic) IBOutlet UITextField *packagesTextField;
@property (weak, nonatomic) IBOutlet UIButton *requestButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectOrgTextFieldTop;


@property (strong, nonatomic) NSArray <Package *> * packagesDataSource;

@property (strong, nonatomic) Package * selectedPackage;

@property (assign, nonatomic) BOOL isFirstTime;
@property (weak, nonatomic) IBOutlet UITextView *errorTextView;

@end

@implementation SelectPackageDialogContainerViewController

- (void)dealloc {
	self.orgTableTop = nil;
	self.packagesTextField = nil;
	self.packagesTableTop = nil;
	self.organizationsDataSource = nil;
	self.packagesDataSource = nil;
}


- (void)viewDidLoad {
	[super viewDidLoad];
	self.isFirstTime = YES;
	
	self.view.layer.cornerRadius = 5.f;
	self.view.clipsToBounds = YES;
	
	if (!self.errorText) {
		self.orgTable.estimatedRowHeight = 40.f;
		self.orgTable.rowHeight = UITableViewAutomaticDimension;
		self.orgTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
		[self.orgTable registerNib:[UINib nibWithNibName:NSStringFromClass([SelectPackageTableCell class]) bundle:nil]
			forCellReuseIdentifier:selectPackageTableCellReuseId];
		self.orgTable.layer.borderColor = [[Colors shared].primary CGColor];
		self.orgTable.layer.borderWidth = 1.f;
		self.orgTable.layer.cornerRadius = 5.f;
		
		self.packagesTable.estimatedRowHeight = 40.f;
		self.packagesTable.rowHeight = UITableViewAutomaticDimension;
		self.packagesTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
		[self.packagesTable registerNib:[UINib nibWithNibName:NSStringFromClass([SelectPackageTableCell class]) bundle:nil]
				 forCellReuseIdentifier:selectPackageTableCellReuseId];
		self.packagesTable.layer.borderColor = [[Colors shared].primary CGColor];
		self.packagesTable.layer.borderWidth = 1.f;
		self.packagesTable.layer.cornerRadius = 5.f;
		

		[self.requestButtonTop setActive:NO];
		self.requestButton.alpha = 0;
		self.requestButton.hidden = YES;
		
		[self.packagesTableTop setActive:NO];
		self.packagesTable.alpha = 0;
		self.packagesTable.hidden = YES;
		
		[self.packagesTextFieldTop setActive:NO];
		self.packagesTextFieldBgView.alpha = 0;
		self.packagesTextField.hidden = YES;
		self.packagesTextFieldBgView.hidden = YES;
		
		[self.orgTableTop setActive:NO];
		self.orgTable.alpha = 0;
		self.orgTable.hidden = YES;
		
		[self.view layoutIfNeeded];
		
		self.requestButton.layer.cornerRadius = 3.f;
		self.packagesTextField.layer.cornerRadius = 3.f;
		self.orgTextField.layer.cornerRadius = 3.f;
		
		self.orgsTextFieldBgView.backgroundColor = [Colors shared].primary;
		self.packagesTextFieldBgView.backgroundColor = [Colors shared].primary;
		
		[self.requestButton setBackgroundColor:[Colors shared].primary];
		
		self.titleLabel.textColor = [Colors shared].primary;
		
	} else {
		[self.contentViewBottom setActive:NO];
		self.contentView.hidden = YES;
		self.errorTextView.hidden = NO;
		self.errorTextView.text = self.errorText;
	}
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	self.isFirstTime = NO;
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	
}


- (IBAction)onAddContent:(id)sender {
	WELF_MACRO
	[self dismissWithCompletion:^{
		if (welf.delegate && [self.delegate respondsToSelector:@selector(loadPackage:)]) {
			[welf.delegate loadPackage:welf.selectedPackage];
		}
	}];
}

- (void)dismissWithCompletion:(void (^)(void))completion {
	[self.parentViewController.navigationController.presentingViewController dismissViewControllerAnimated:YES completion:completion];
}

- (void)resize {
	CGRect r = [(DialogViewController *)self.parentViewController frameOfPresentedViewInContainerView];
	
	if (!self.errorText) {
		if (self.packagesTable.alpha == 1.f) {
			CGFloat freeSize = r.size.height - self.contentViewBottom.constant - self.contentViewTop.constant - self.titleLabel.frame.size.height - self.selectOrgTextFieldTop.constant - self.orgTextField.frame.size.height - self.packagesTextFieldTop.constant - self.packagesTextField.frame.size.height - self.packagesTableTop.constant;
			
			if (freeSize < self.packagesTableHeight.constant) {
				self.packagesTableHeight.constant = freeSize;
			}
		} else if (self.orgTable.alpha == 1.f) {
			CGFloat freeSize = r.size.height - self.contentViewBottom.constant - self.contentViewTop.constant - self.titleLabel.frame.size.height - self.selectOrgTextFieldTop.constant - self.orgTextField.frame.size.height - self.orgTableTop.constant;
			if (freeSize < self.orgTableHeight.constant) {
				self.orgTableHeight.constant = freeSize;
			}
		}

	} else {
		
	}
	
	[(DialogViewController *)self.parentViewController resize];
}

- (CGSize)neededSizeForTable:(UITableView *)table numberOfRows:(NSInteger)count{
	CGSize s = table.contentSize;
	CGFloat h = 0;
	
	for (int i = 0; i < count; i++) {
		h += [table rectForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]].size.height;
	}
	
	s.height = h;
	return s;
}

#pragma mark - UITable View Protocols -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	NSInteger count = 0;
	if (!self.errorText) {
		if (self.orgTable == tableView) {
			count = self.organizationsDataSource ? self.organizationsDataSource.count : 0;
		} else if (self.packagesTable == tableView) {
			count = self.packagesDataSource ? self.packagesDataSource.count : 0;
		}
		
		if (count == 0) {
			count = 1;
		}
	}
	return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	SelectPackageTableCell * cell = [tableView dequeueReusableCellWithIdentifier:selectPackageTableCellReuseId
																	forIndexPath:indexPath];
	
	
	if (self.orgTable == tableView) {
		
		if (!self.organizationsDataSource || self.organizationsDataSource.count == 0) {
			[cell configureCell:localized(@"NoData") indexPath:indexPath];
		} else {
			[cell configureCell:self.organizationsDataSource[indexPath.row].title indexPath:indexPath];
		}
		
	} else if (self.packagesTable == tableView) {
		if (!self.packagesDataSource || self.packagesDataSource.count == 0) {
			[cell configureCell:localized(@"NoData") indexPath:indexPath];
		} else {
			[cell configureCell:self.packagesDataSource[indexPath.row].packageTitle indexPath:indexPath];
		}
	}
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	__weak __block typeof(self) welf = self;
	if (self.orgTable == tableView) {
		
		if (welf.organizationsDataSource && welf.organizationsDataSource.count > 0) {
			self.orgTextField.text = self.organizationsDataSource[indexPath.row].title;
			self.orgTextField.textAlignment = NSTextAlignmentLeft;
			
			self.packagesDataSource = self.organizationsDataSource[indexPath.row].packages;
		}
		self.packagesTextField.text = localized(@"SelectPackage");
		self.packagesTextField.textAlignment = NSTextAlignmentCenter;
		
		[UIView animateWithDuration:0.25f animations:^{
			[welf.orgTableTop setActive:NO];
			welf.orgTable.alpha = 0.f;
			welf.orgTable.hidden = YES;
			
			if (welf.organizationsDataSource && welf.organizationsDataSource.count > 0) {
				[welf.packagesTextFieldTop setActive:YES];
				welf.packagesTextFieldBgView.hidden = NO;
				welf.packagesTextField.hidden = NO;
				welf.packagesTextFieldBgView.alpha = 1;
			}
			
			[welf.parentViewController.navigationController.view layoutIfNeeded];
			[welf resize];
		}];
		
	} else if (self.packagesTable == tableView) {
		
		if (self.packagesDataSource && self.packagesDataSource.count > 0) {
			self.packagesTextField.text = self.packagesDataSource[indexPath.row].packageTitle;
			self.packagesTextField.textAlignment = NSTextAlignmentLeft;
			
			self.selectedPackage = self.packagesDataSource[indexPath.row];
		}
		
		[UIView animateWithDuration:0.25f animations:^{
			[welf.packagesTableTop setActive:NO];
			welf.packagesTable.alpha = 0.f;
			welf.packagesTable.hidden = YES;
			
			if (self.packagesDataSource && self.packagesDataSource.count > 0) {
				[welf.requestButtonTop setActive:YES];
				welf.requestButton.hidden = NO;
				welf.requestButton.alpha = 1.f;
			}
			
			[welf.parentViewController.navigationController.view layoutIfNeeded];
			[welf resize];
		}];
	}
}

#pragma mark - UITextField Delegate -

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
	__weak __block typeof(self) welf = self;
	if (self.packagesTextField == textField) {
		
		[self.packagesTable reloadData];
		CGSize s = [self neededSizeForTable:self.packagesTable numberOfRows:[self tableView:self.packagesTable numberOfRowsInSection:0]];
		[UIView animateWithDuration:0.25f animations:^{
			
			[welf.requestButtonTop setActive:NO];
			welf.requestButton.alpha = 0;
			welf.requestButton.hidden = YES;
			
			[welf.packagesTableTop setActive:YES];
			welf.packagesTable.hidden = NO;
			welf.packagesTable.alpha = 1.f;
			welf.packagesTableHeight.constant = s.height;
			[welf.parentViewController.navigationController.view layoutIfNeeded];
			[welf resize];
		}];
		
	} else if (self.orgTextField == textField) {
		
		[self.orgTable reloadData];
		CGSize s = [self neededSizeForTable:self.orgTable numberOfRows:[self tableView:self.orgTable numberOfRowsInSection:0]];
		[UIView animateWithDuration:0.25f animations:^{
			[welf.orgTableTop setActive:YES];
			welf.orgTable.hidden = NO;
			welf.orgTableHeight.constant = s.height;
			welf.orgTable.alpha = 1.f;
			
			[welf.requestButtonTop setActive:NO];
			welf.requestButton.alpha = 0;
			welf.requestButton.hidden = YES;
			
			[welf.packagesTableTop setActive:NO];
			welf.packagesTable.alpha = 0;
			welf.packagesTable.hidden = YES;
			
			[welf.packagesTextFieldTop setActive:NO];
			welf.packagesTextFieldBgView.alpha = 0;
			welf.packagesTextFieldBgView.hidden = YES;
			welf.packagesTextField.hidden = YES;
			
			[welf.parentViewController.navigationController.view layoutIfNeeded];
			[welf resize];
		}];
	}
	
	return NO;
}

#pragma mark - DialogContainerProtocol -


- (CGSize)neededCotentSize {
	if (self.errorText) {

		CGRect rect = [self.errorTextView.text boundingRectWithSize:CGSizeMake(self.parentViewController.view.frame.size.width - 20.f, CGFLOAT_MAX)
										 options:NSStringDrawingUsesLineFragmentOrigin
									  attributes:@{NSFontAttributeName : self.errorTextView.font}
										 context:nil];
		CGFloat h = 10.f + rect.size.height + 10.f + 40.f;
		CGFloat w = self.parentViewController.view.frame.size.width;
		
		return CGSizeMake(w, h);
	} else {
		
		CGFloat h = self.contentViewTop.constant + self.contentView.frame.size.height + self.contentViewBottom.constant;
		CGFloat w = self.parentViewController.view.frame.size.width;
		CGSize s = CGSizeMake(w, h);
		
		return s;
	}
	return CGSizeZero;
}


- (BOOL)shouldDismissPresentedControllerByOutsideTap {
	return YES;
}

- (void)presentedControllerWillBeDismissed {}


- (BOOL)shouldHideNavBar {
	return YES;
}


- (NSArray <DialogAction *> *)dialogActionsForDialog:(DialogViewController *)dialog {
	return nil;
}


@end
