//
//  MainNavigationController.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/6/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "MainNavigationController.h"

@interface MainNavigationController ()

@end

@implementation MainNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.presentDocumentPreview = NO;

}

- (UIStatusBarStyle)preferredStatusBarStyle {
	
	if (self.presentDocumentPreview) {
		return UIStatusBarStyleDefault;
	} else {
		return UIStatusBarStyleLightContent;
	}
	
	return UIStatusBarStyleLightContent;
}

- (void)setPresentDocumentPreview:(BOOL)presentDocumentPreview {
	_presentDocumentPreview = presentDocumentPreview;
	[self setNeedsStatusBarAppearanceUpdate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
