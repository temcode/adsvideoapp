//
//  LinksViewController.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 3/3/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "LinksViewController.h"
#import "LinkTableViewCell.h"
#import "Link.h"

#import <MessageUI/MessageUI.h>

@interface LinksViewController () <UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (strong, nonatomic) NSMutableArray <Link *> * dataSource;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation LinksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.tableView.rowHeight = UITableViewAutomaticDimension;
	self.tableView.estimatedRowHeight = 50.f;
	self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

	self.dataSource = [NSMutableArray array];

	if ([PackageSession sharedSession].linkActionTitle) {
		[self.backButton setTitle:[PackageSession sharedSession].linkActionTitle forState:UIControlStateNormal];
	}
	
	if ([PackageSession sharedSession].links && [PackageSession sharedSession].links.count > 0) {
		self.dataSource = [NSMutableArray arrayWithArray:[PackageSession sharedSession].links];
	}
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	
}

- (IBAction)onBack:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table View Protocols -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return self.dataSource ? self.dataSource.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	LinkTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:linkTableViewCellReuseId
															   forIndexPath:indexPath];
	[cell configureCellWithTitle:self.dataSource[indexPath.row].title];
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	Link * link = self.dataSource[indexPath.row];
	
	switch (link.type) {
		case LinkTypePhone: {
			if (![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]]) {
				showMainWindowToastMessage(localized(@"NotAbleMakeCalls"));
				break;
			}
		case LinkTypeWeb:
			[[UIApplication sharedApplication] openURL:link.URL];
			break;
		}
		case LinkTypeEmail: {
			if ([MFMailComposeViewController canSendMail])
			{
				MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
				mail.mailComposeDelegate = (id <MFMailComposeViewControllerDelegate>)self;
				[mail setToRecipients:@[link.linkString]];
				[self.navigationController presentViewController:mail animated:YES completion:NULL];
			}
			else
			{
				showMainWindowToastMessage(localized(@"CantSendMailsFromThisDevice"));
				NSLog(@"This device cannot send email");
			}

			break;
		}
		default:
			break;
	}
}

#pragma mark - MFMailComposeViewControllerDelegate -

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
//	NSString * message = nil;
//	switch (result) {
//		case MFMailComposeResultSent: {
//			message = localized(@"MessageSent");
//			break;
//		}
//		case MFMailComposeResultSaved: {
//			message = localized(@"MessageSaved");
//			break;
//		}
//		case MFMailComposeResultFailed: {
//			message = localized(@"MessageFailed");
//			break;
//		}
//		case MFMailComposeResultCancelled: {
//			message = localized(@"MessageCancelled");
//			break;
//		}
//		default:
//			break;
//	}
	
	if (error) {
		NSLog(@"%@", [error localizedDescription]);
	}
	
	[controller.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
