//
//  ChooseViewController.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/4/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "ChooseViewController.h"
#import "ChooseTableViewCell.h"
#import "VideoViewController.h"

#import "MainNavigationController.h"

#import "Video.h"
#import "VideoPoint.h"
#import "Document.h"

#define showDocumentSegue @"ShowDocumentSegueId"
#define showVideoSegue @"ShowVideoSegueId"

@interface ChooseViewController () <UITableViewDelegate, UITableViewDataSource, UIDocumentInteractionControllerDelegate, ChooseCellDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (strong, nonatomic) NSArray <id> * dataSource;

@property (strong, nonatomic) NSIndexPath * selectedIndexPath;

@property (strong, nonatomic) UIDocumentInteractionController * documentInteractionController;

@property (strong, nonatomic) Video * selectedVideo;
@property (strong, nonatomic) VideoPoint * selectedVideoPoint;

@property (assign, nonatomic) BOOL isFirstCellSelection;

@end

@implementation ChooseViewController

- (void)dealloc {
	self.dataSource = nil;
	self.selectedIndexPath = nil;
	self.selectedVideo = nil;
	self.selectedVideoPoint = nil;
	
	self.documentInteractionController = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.isFirstCellSelection = YES;
	
	self.selectedIndexPath = nil;
	self.selectedVideo = nil;
	self.selectedVideoPoint = nil;
	
	self.tableView.rowHeight = UITableViewAutomaticDimension;
	self.tableView.estimatedRowHeight = 75.f;
	self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0,
																			 0,
																			 self.tableView.frame.size.width,
																			  10.f)];
	self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0,
																			  0,
																			  self.tableView.frame.size.width,
																			  10.f)];

	PackageSession * session = [PackageSession sharedSession];
	
	if (self.controllerType == ChooseViewControllerVideos) {
		self.dataSource = session.videos;
		[self.backButton setTitle:session.videoActionTitle forState:UIControlStateNormal];
	} else {
		self.dataSource = session.documents;
		[self.backButton setTitle:session.documentActionTitle forState:UIControlStateNormal];
	}
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];

}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
}

- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
	self.selectedVideo = nil;
	self.selectedVideoPoint = nil;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
	[super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
	
	[self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	if ([segue.identifier isEqualToString:@"ShowVideoSegueId"]) {
		((VideoViewController *)segue.destinationViewController).video = self.selectedVideo;
		((VideoViewController *)segue.destinationViewController).point = self.selectedVideoPoint;
	} 
}



- (IBAction)onBack:(id)sender {
	
	if (self.navigationController.presentingViewController) {
		[self.navigationController dismissViewControllerAnimated:YES completion:nil];
	} else {
		[self.navigationController popViewControllerAnimated:YES];
	}
}

#pragma mark - TableView Protocols - 

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return self.dataSource ? self.dataSource.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	ChooseTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:chooseCellReuseId forIndexPath:indexPath];
	BOOL selected = [self.selectedIndexPath isEqual:indexPath];
	BOOL clipped = self.selectedIndexPath ? self.selectedIndexPath.row + 1 == indexPath.row : NO;
	BOOL showDownSeparator = selected ? self.dataSource.count - 1 == indexPath.row : NO;
	[cell configureCellWith:self.dataSource[indexPath.row]
				   selected:selected
					clipped:clipped
		  showDownSeparator:showDownSeparator
				atIndexPath:indexPath];
	cell.delegate = self;
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if (self.controllerType == ChooseViewControllerVideos) {

	} else {
		Document * doc = self.dataSource[indexPath.row];
		NSURL * documentURL =  [doc URL];
		
		if (documentURL) {
			// Initialize Document Interaction Controller
			self.documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:documentURL];
			
			// Configure Document Interaction Controller
			[self.documentInteractionController setDelegate:self];
			
			// Preview PDF
			[self.documentInteractionController presentPreviewAnimated:YES];
		}
	}
}

#pragma mark - Cell Utils -

- (void)animateCellsUpdate:(NSArray *)reloadCells {
	if (self.dataSource.count == reloadCells.count) {
		[self.tableView beginUpdates];
		[self.tableView deleteRowsAtIndexPaths:reloadCells withRowAnimation:UITableViewRowAnimationFade];
		[self.tableView insertRowsAtIndexPaths:reloadCells withRowAnimation:UITableViewRowAnimationFade];
		[self.tableView endUpdates];
	} else {
//		[self.tableView beginUpdates];
		[self.tableView reloadRowsAtIndexPaths:reloadCells withRowAnimation:UITableViewRowAnimationAutomatic];
//		[self.tableView endUpdates];
	}
}

- (NSMutableArray <NSIndexPath *> *)reloadCellsCurrentIndex:(NSIndexPath *)indexPath previous:(NSIndexPath *)previousIndexPath {

	NSMutableArray <NSIndexPath *> * reloadCells = [NSMutableArray array];

		[reloadCells addObject:indexPath]; // current cell
		NSIndexPath * previousNextIndexPath = nil;
		
		if ((previousIndexPath != nil) && ![indexPath isEqual:previousIndexPath]) {
			[reloadCells addObject:previousIndexPath]; // previous cell
			
			previousNextIndexPath = [NSIndexPath indexPathForRow:previousIndexPath.row + 1 inSection:0]; // next of previous
			if (self.dataSource.count > previousNextIndexPath.row) {
				[reloadCells addObject:previousNextIndexPath];
			}
		}
		
		NSIndexPath * currentNextIndexPath = [NSIndexPath indexPathForRow:indexPath.row + 1 inSection:0];
		if (self.dataSource.count > currentNextIndexPath.row
			&& !(previousNextIndexPath
				 && [currentNextIndexPath isEqual:previousNextIndexPath])) {
				[reloadCells addObject:currentNextIndexPath]; // next of current
			}
		
	return reloadCells;
}

#pragma mark - Cell Delegate -

- (void)point:(VideoPoint *)point wasSelectedForCell:(ChooseTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
	self.selectedVideo = self.dataSource[indexPath.row];
	self.selectedVideoPoint = point;
	[self performSegueWithIdentifier:showVideoSegue sender:point];
	
}

- (void)scaleCell:(ChooseTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
	
	NSIndexPath * previousIndexPath = nil;
	if (self.selectedIndexPath) {
		previousIndexPath = self.selectedIndexPath;
		if([self.selectedIndexPath isEqual:indexPath]) {
			self.selectedIndexPath = nil;
		} else {
			self.selectedIndexPath = indexPath;
		}
	} else {
		self.selectedIndexPath = indexPath;
	}

	NSMutableArray <NSIndexPath *> * reloadCells = [self reloadCellsCurrentIndex:indexPath previous:previousIndexPath];
	
	
	
	[self animateCellsUpdate:reloadCells];
	

	
	if (self.isFirstCellSelection) {
		self.isFirstCellSelection = NO;
		//	[self.tableView reloadData];
	}

}

- (void)openVideoForCell:(ChooseTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
						self.selectedVideo = self.dataSource[indexPath.row];
						[self performSegueWithIdentifier:showVideoSegue sender:nil];

}

#pragma mark - UIDocumentInteractionControllerDelegate -

- (UIViewController *)documentInteractionControllerViewControllerForPreview: (UIDocumentInteractionController *) controller {
	return self;
}

- (void)documentInteractionControllerWillBeginPreview:(UIDocumentInteractionController *)controller {
	((MainNavigationController *)self.navigationController).presentDocumentPreview = YES;
}

-(void)documentInteractionControllerDidEndPreview:(UIDocumentInteractionController *)controller {
	((MainNavigationController *)self.navigationController).presentDocumentPreview = NO;
}

@end
