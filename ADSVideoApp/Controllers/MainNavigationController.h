//
//  MainNavigationController.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/6/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainNavigationController : UINavigationController

@property (assign, nonatomic) BOOL presentDocumentPreview;

@end
