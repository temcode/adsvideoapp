//
//  AlreadyUpdatePackagesContainer.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 3/2/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "AlreadyUpdatePackagesContainer.h"
#import "DialogViewController.h"
#import "AlreadyUpdateCell.h"

@interface AlreadyUpdatePackagesContainer () <DialogContainerProtocol, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextView *infoTextView;

@property (strong, nonatomic) DialogAction * selectionAction;

@property (strong, nonatomic) NSMutableSet <Package *> * selectedPackages;

@end

@implementation AlreadyUpdatePackagesContainer

- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.selectedPackages = [NSMutableSet set];
	
	self.infoTextView.text = localized(@"AskToReplaceFollowingPackages");
	
	self.infoTextView.textColor = [Colors shared].primary;
	
	self.tableView.rowHeight = UITableViewAutomaticDimension;
	self.tableView.estimatedRowHeight = 50.f;
	self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
	
	[DialogAction setDefaultButtonBackgroundColor:[Colors shared].primary];
	[DialogAction setDefaultButtonTitleColor:[UIColor whiteColor]];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	[self resize];
	
	if (self.tableView.contentSize.height > self.tableView.frame.size.height) {
		[self.tableView setScrollEnabled:YES];
		[self.tableView flashScrollIndicators];
	}
	
	if (self.packages.count == 1) {
		[self.tableView setAllowsSelection:NO];
	}
}

- (void)resize {
	[(DialogViewController *)self.parentViewController resize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dismissWithCompletion:(void (^)(void))completion {
	[self.parentViewController.navigationController.presentingViewController dismissViewControllerAnimated:YES completion:completion];
}


#pragma mark - UITable View Protocols -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return self.packages ? self.packages.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	AlreadyUpdateCell * cell = [tableView dequeueReusableCellWithIdentifier:alreadyUpdateCellReuseId
															   forIndexPath:indexPath];
	
	[cell configureCellWith:self.packages[indexPath.row].packageTitle orgTitle:self.packages[indexPath.row].organization.title];
	
	if (self.packages.count == 1) {
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
	} else {
		if ([self.selectedPackages containsObject:self.packages[indexPath.row]]) {
			[tableView selectRowAtIndexPath:indexPath
								   animated:YES
							 scrollPosition:UITableViewScrollPositionNone];
		}

	}
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	Package * selected = self.packages[indexPath.row];
	if (![self.selectedPackages containsObject:selected]) {
		[self.selectedPackages addObject:selected];
	}
	
	if (self.selectedPackages.count > 0 && self.selectionAction) {
		self.selectionAction.titleText = localized(@"DeselectAll");
	}
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
	Package * deselected = self.packages[indexPath.row];
	
	if ([self.selectedPackages containsObject:deselected]) {
		[self.selectedPackages removeObject:deselected];
	}
	
	if (self.selectedPackages.count == 0 && self.selectionAction) {
		self.selectionAction.titleText = localized(@"SelectAll");
	}
}

#pragma mark - DialogContainerProtocol -

- (CGSize)neededSizeForTable:(UITableView *)table numberOfRows:(NSInteger)count{
	CGSize s = table.contentSize;
	CGFloat h = 0;
	
	for (int i = 0; i < count; i++) {
		h += [table rectForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]].size.height;
	}
	
	s.height = h;
	return s;
}



- (CGSize)neededCotentSize {
	CGSize s = [self neededSizeForTable:self.tableView numberOfRows:[self tableView:self.tableView numberOfRowsInSection:0]];
	return CGSizeMake(s.width, s.height + self.infoTextView.frame.size.height) ;
}


- (BOOL)shouldDismissPresentedControllerByOutsideTap {
	return YES;
}

- (void)presentedControllerWillBeDismissed {}


- (BOOL)shouldHideNavBar {
	return YES;
}


- (NSArray <DialogAction *> *)dialogActionsForDialog:(DialogViewController *)dialog {
	NSArray <DialogAction *> * dialogActions = nil;
	
	WELF_MACRO
	DialogAction * noAction = [DialogAction dialogActionWithTitle:localized(@"No")
															style:UIAlertActionStyleDefault
										dismissDialogAfterHandler:NO
														  handler:^(DialogAction * _Nonnull action) {
															  [welf dismissWithCompletion:^{
																  if (welf.updatePackagesBlock) {
																	  welf.updatePackagesBlock(nil);
																  }
															  }];
														  }];
	if (self.packages.count > 1) {
		
		DialogAction * updateSelectedAction = [DialogAction dialogActionWithTitle:localized(@"ReplaceSelected")
																			style:UIAlertActionStyleDefault
														dismissDialogAfterHandler:NO
																		  handler:^(DialogAction * _Nonnull action) {
																			  [welf dismissWithCompletion:^{
																				  if (welf.updatePackagesBlock) {
																					  welf.updatePackagesBlock([welf.selectedPackages allObjects]);
																				  }
																			  }];
																		  }];
		
		self.selectionAction = [DialogAction dialogActionWithTitle:localized(@"SelectAll")
															 style:UIAlertActionStyleDefault
										 dismissDialogAfterHandler:NO
														   handler:^(DialogAction * _Nonnull action) {
															   if ([self.selectionAction.titleText isEqualToString:localized(@"SelectAll")]) {
																   welf.selectionAction.titleText = localized(@"DeselectAll");
																   for (Package * pack in welf.packages) {
																	   [welf.selectedPackages addObject:pack];
																   }
																   [welf.tableView reloadData];
															   } else {
																   welf.selectionAction.titleText = localized(@"SelectAll");
																   for (Package * pack in welf.packages) {
																	   if ([welf.selectedPackages containsObject:pack]) {
																		   [welf.selectedPackages removeObject:pack];
																	   }
																   }
																   [welf.tableView reloadData];
															   }
														   }];
		
		DialogAction * updateAllAction = [DialogAction dialogActionWithTitle:localized(@"ReplaceAll")
																	   style:UIAlertActionStyleDefault
												   dismissDialogAfterHandler:NO
																	 handler:^(DialogAction * _Nonnull action) {
																		 [welf dismissWithCompletion:^{
																			 if (welf.updatePackagesBlock) {
																				 welf.updatePackagesBlock(welf.packages);
																			 }
																		 }];
																	 }];
		
		dialogActions = @[self.selectionAction, updateSelectedAction, updateAllAction, noAction];
	} else if (self.packages.count == 1) {
	
		DialogAction * yesAction = [DialogAction dialogActionWithTitle:localized(@"Yes")
																	   style:UIAlertActionStyleDefault
												   dismissDialogAfterHandler:NO
																	 handler:^(DialogAction * _Nonnull action) {
																		 [welf dismissWithCompletion:^{
																			 if (welf.updatePackagesBlock) {
																				 welf.updatePackagesBlock(welf.packages);
																			 }
																		 }];
																	 }];
		
		dialogActions = @[yesAction, noAction];

	}
	

	return dialogActions;
}

@end
