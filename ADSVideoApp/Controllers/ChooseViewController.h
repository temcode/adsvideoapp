//
//  ChooseViewController.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/4/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ChooseViewController : UIViewController

@property (assign, nonatomic) ChooseViewControllerType controllerType;

@end
