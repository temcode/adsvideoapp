//
//  AlreadyUpdatePackagesContainer.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 3/2/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Package.h"

typedef void (^UpdatePackagesBlock)(NSArray <Package *> * packagesToUpdate);


@interface AlreadyUpdatePackagesContainer : UIViewController

@property (copy, nonatomic) UpdatePackagesBlock updatePackagesBlock;
@property (strong, nonatomic) NSMutableArray <Package *> * packages;

@end
