//
//  QRCodeReaderViewController.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/20/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QRCodeReaderDelegate <NSObject>

- (void)didSnapedQRCode:(NSString *)codeString;
- (void)qrCodeReaderDidCancelled;

@end

@interface QRCodeReaderViewController : UIViewController

@property (weak, nonatomic) id<QRCodeReaderDelegate> delegate;

@end

