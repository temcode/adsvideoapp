//
//  DeletePackageDialogContainer.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/17/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol DeletePackageDialogContainerDelegate <NSObject>

- (void)deletePackage:(Package *)package;

@end

@interface DeletePackageDialogContainer : UIViewController

@property (weak, nonatomic) id<DeletePackageDialogContainerDelegate> delegate;

@property (strong, nonatomic) Package * package;


@end
