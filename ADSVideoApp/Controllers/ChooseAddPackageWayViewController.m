//
//  ChooseAddPackageWayViewController.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/17/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "ChooseAddPackageWayViewController.h"
#import "DialogViewController.h"


@interface ChooseAddPackageWayViewController () <DialogContainerProtocol>
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *codeButton;
@property (weak, nonatomic) IBOutlet UIButton *browseButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewTrailing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBot;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonsViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonsViewHeight;

@end

@implementation ChooseAddPackageWayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	self.view.layer.cornerRadius = 5.f;
	self.view.clipsToBounds = YES;

	[self.codeButton.layer setCornerRadius:3.f];
	self.browseButton.layer.cornerRadius = 3.f;
	
	[self.codeButton setBackgroundColor:[Colors shared].primary];
	[self.browseButton setBackgroundColor:[Colors shared].primary];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBrowse:(id)sender {
	WELF_MACRO
	[self dismissWithCompletion:^{
		if (welf.delegate && [welf.delegate respondsToSelector:@selector(addViaBrowsing)]) {
			[welf.delegate addViaBrowsing];
		}
	}];
}

- (IBAction)onCode:(id)sender {
	WELF_MACRO
	[self dismissWithCompletion:^{
		if (welf.delegate && [welf.delegate respondsToSelector:@selector(addViaCode)]) {
			[welf.delegate addViaCode];
		}
	}];
}

- (void)dismissWithCompletion:(void (^)(void))completion {
	[self.parentViewController.navigationController.presentingViewController dismissViewControllerAnimated:YES completion:completion];
}

#pragma mark - DialogContainerProtocol -


- (CGSize)neededCotentSize {
	CGSize s = CGSizeZero;
	
	CGFloat h = self.titleTop.constant + self.titleLabel.frame.size.height + self.buttonsViewTop.constant + self.buttonsViewHeight.constant + self.contentViewBot.constant;
	
	CGFloat w = self.parentViewController.view.frame.size.width;
	s = CGSizeMake(w, h);
	
	return s;
}


- (BOOL)shouldDismissPresentedControllerByOutsideTap {
	return YES;
}

- (void)presentedControllerWillBeDismissed {}


- (BOOL)shouldHideNavBar {
	return YES;
}


- (NSArray <DialogAction *> *)dialogActionsForDialog:(DialogViewController *)dialog {
	return nil;
}

@end
