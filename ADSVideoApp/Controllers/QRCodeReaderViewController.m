//
//  QRCodeReaderViewController.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/20/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "QRCodeReaderViewController.h"
#import <MTBBarcodeScanner/MTBBarcodeScanner.h>


@interface QRCodeReaderViewController ()

@property (strong, nonatomic) MTBBarcodeScanner * scanner;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@end

@implementation QRCodeReaderViewController

- (void)dealloc
{
	[self.scanner stopScanning];
	
	self.scanner = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.backButton.layer.cornerRadius = self.backButton.frame.size.height / 2.f;
	
	self.scanner = [[MTBBarcodeScanner alloc] initWithMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]
												   previewView:self.view];
	
	self.scanner.didStartScanningBlock = ^{
		NSLog(@"The scanner started scanning! We can now hide any activity spinners.");
	};
	
	WELF_MACRO
	self.scanner.resultBlock = ^(NSArray *codes){
		NSLog(@"Found these codes: %@", codes);

		[welf.scanner stopScanning];

		NSString * codeString = [(AVMetadataMachineReadableCodeObject *)codes[0] stringValue];
		if (welf.delegate && [welf.delegate respondsToSelector:@selector(didSnapedQRCode:)]) {
			[welf.delegate didSnapedQRCode:codeString];
		}
		
		[welf.presentingViewController dismissViewControllerAnimated:YES completion:nil];
	};
	
	self.scanner.didTapToFocusBlock = ^(CGPoint point){
		NSLog(@"The user tapped the screen to focus. \
			  Here we could present a view at %@", NSStringFromCGPoint(point));
	};
	
	NSError * error = nil;
	if (![self.scanner startScanningWithError:&error]) {
		if (error) {
			showMainWindowToastMessage([error localizedDescription]);
		}
	}
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBack:(id)sender {
	WELF_MACRO
	[self.presentingViewController dismissViewControllerAnimated:YES completion:^{
		if (welf.delegate && [welf.delegate respondsToSelector:@selector(qrCodeReaderDidCancelled)]) {
			[welf.delegate qrCodeReaderDidCancelled];
		}
	}];
}


@end
