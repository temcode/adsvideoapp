//
//  AddPackageViaCodeDialogContainer.m
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/17/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import "AddPackageViaCodeDialogContainer.h"
#import "DialogViewController.h"

@interface AddPackageViaCodeDialogContainer () <DialogContainerProtocol, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *tilteLabel;
@property (weak, nonatomic) IBOutlet UITextField *packageCodeTextField;
@property (weak, nonatomic) IBOutlet UIButton *requestButton;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBot;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewTrailing;

@end

@implementation AddPackageViaCodeDialogContainer

- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.view.layer.cornerRadius = 5.f;
	self.view.clipsToBounds = YES;

	self.tilteLabel.textColor = [Colors shared].primary;
	self.packageCodeTextField.textColor = [Colors shared].primary;
	
	[self.requestButton setBackgroundColor:[Colors shared].primary];
	self.requestButton.layer.cornerRadius = 3.f;
	
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	[self.packageCodeTextField becomeFirstResponder];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onRequest:(id)sender {
	if (self.packageCodeTextField.text.length > 0) {
		WELF_MACRO
		[self dismissWithCompletion:^{
			if (welf.delegate && [welf.delegate respondsToSelector:@selector(loadPackageViaCode:)]) {
				[welf.delegate loadPackageViaCode:self.packageCodeTextField.text];
			}
		}];
	} else {
		showMainWindowToastMessage(localized(@"PleaseEnterCode"));
	}
}

- (void)dismissWithCompletion:(void (^)(void))completion {
	[self.parentViewController.navigationController.presentingViewController dismissViewControllerAnimated:YES completion:completion];
}

#pragma mark - DialogContainerProtocol -


- (CGSize)neededCotentSize {
	CGSize s = CGSizeZero;
	
	CGFloat h = self.contentViewTop.constant + self.contentView.frame.size.height + self.contentViewBot.constant;
	
	CGFloat w = self.parentViewController.view.frame.size.width;
	s = CGSizeMake(w, h);
	
	return s;
}


- (BOOL)shouldDismissPresentedControllerByOutsideTap {
	return YES;
}

- (void)presentedControllerWillBeDismissed {}


- (BOOL)shouldHideNavBar {
	return YES;
}


- (NSArray <DialogAction *> *)dialogActionsForDialog:(DialogViewController *)dialog {
	return nil;
}

#pragma mark - UITextField Delegate -

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	return YES;
}

@end
