//
//  SelectPackageDialogContainerViewController.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/15/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Organization;
@class Package;

@protocol BrowsingPackageDelegate <NSObject>

- (void)loadPackage:(Package *)package;

@end

@interface SelectPackageDialogContainerViewController : UIViewController

@property (weak, nonatomic) id<BrowsingPackageDelegate> delegate;

@property (strong, nonatomic) NSArray <Organization *> * organizationsDataSource;

@property (strong, nonatomic) NSString * errorText;

@end
