//
//  AddPackageViaCodeDialogContainer.h
//  ADSVideoApp
//
//  Created by Artem Selivanov on 2/17/17.
//  Copyright © 2017 ArtSelDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CodeAddingDelegate <NSObject>

- (void)loadPackageViaCode:(NSString *)packageCode;

@end


@interface AddPackageViaCodeDialogContainer : UIViewController

@property (weak, nonatomic) id<CodeAddingDelegate> delegate;

@end
